var chrome = chrome||browser;
var lastClick = null;
document.addEventListener("contextmenu", ev => {
	lastClick = ev.target;
});
var directTextContent = el => [...el.childNodes].filter(v => v.nodeType == Node.TEXT_NODE).map(v => v.textContent).join(" ");
var walkUp=(el,filter)=>{
	for(;el;el=el.parentElement){
		if(filter(el)) return el;
	}
};
var htmlEscape = v => v.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/"/g, "&quot;").replace(/'/g, "&#039;");
var link=(href,name)=>`<a href="${htmlEscape(href)}">${htmlEscape(name||href)}</a>`;
chrome.runtime.onMessage.addListener((req, sender, res) => {
	console.log(req);
	if(req.req=="alert"){
		alert(req.alert);
		res();
	} else if(req.req=="makeLink") {
		if(window.location.hostname.endsWith("reddit.com")){
			var thing = walkUp(lastClick,el=>el.classList.contains("thing"));
			if(thing){
				var comments = thing.querySelector(".comments");
				var title = thing.querySelector(".title");
				if(comments&&title){
					return res(link(comments.href,title.textContent));
				}
			}
		}else if(window.location.hostname=="tweetdeck.twitter.com"){
			var thing = walkUp(lastClick,el=>el.tagName=="ARTICLE"&&el.dataset.tweetId!="");
			console.log(thing);
			if(thing){
				var accName = thing.querySelector("a.account-link");
				console.log(accName);
				if(accName){
					return res(link(`${accName.href}/status/${thing.dataset.tweetId}`));
				}
			}
		}
		var a = walkUp(lastClick, el=>el.href);
		if(a){
			return res(link(a.href, a.textContent));
		}
		if(lastClick.src) return res(link(lastClick.src,lastClick.title));
		return res(link(window.location.href,window.title));
	}
});
window.addEventListener(`yumeko_settoken`, e=>{
	var url="";
	if(e.detail){
		if(!/[0-9a-z]{9,}/i.exec(e.detail)) return;
		url = `${window.location.origin}/api/chat?key=${e.detail}`;
	}
	chrome.storage.sync.set({"url":url});
});