var chrome = chrome||browser;
chrome.contextMenus.create({
	id: "yumeko-send",
	title: "Send to Yumeko",
	contexts: ["selection", "link", "image", "video", "audio"],
	onclick: (info, tab) => {
		chrome.tabs.sendMessage(tab.id, {req:"makeLink"}, text => {
			console.log(text);
			if(!text) return;
			chrome.storage.sync.get({
				url:"",
			}, opts=>{
				console.log(opts);
				if(!opts.url){
					chrome.tabs.sendMessage(tab.id, {req:"alert", alert:"Please connect to yumeko in this browser."})
					return;
				}
				var xhr = new XMLHttpRequest();
				xhr.open("POST", opts.url, true);
				xhr.send(text);
			});
		});
	},
});
