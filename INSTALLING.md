INSTALLING
==========

You will need
- [Go 1.4](https://golang.org/doc/install)
- libopus (Use your package manager)
- ffmpeg

Windows
------
See [Opus on windows](opus/windows.md)
TODO: Better guild for this + other deps.

Installing
----------
- Retrieve and install dependancies
  - `libopus`
  - `libjpeg-turbo`
  - `ffmpeg`
- Download and install
  - `go get bitbucket.org/abex/yumeko && go install bitbucket.org/abex/yumeko`
- Copy it out of your GOBIN and into `/usr/bin`
  - `sudo cp $GOPATH/bin/yumeko /usr/bin/yumeko`
- Copy the etc folder into etc
  - `sudo cp -r $GOPATH/src/bitbucket.org/abex/yumeko/etc/yumeko /etc/yumeko.d`
- Install it to systemd
  - `sudo cp $GOPATH/src/bitbucket.org/abex/yumeko/yumeko.service /usr/lib/systemd/system/yumeko.service`
  - `sudo systemctl daemon-reload`
- Configure
  - `comment.txt`
  - `config.json`
  - HTTPS & Mumble certificates and keys
- Start it
  - `sudo systemctl enable yumeko`
  - `sudo systemctl start yumeko`
