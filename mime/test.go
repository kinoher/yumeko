// +build ignore

package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/signal"
	"runtime/pprof"

	"bitbucket.org/abex/yumeko/mime"
)

var dir = "test/"

func main() {
	if len(os.Args) <= 1 {
		panic("Not enough args")
	}

	sigint := make(chan os.Signal, 1)
	signal.Notify(sigint, os.Interrupt)
	go func() {
		signal := <-sigint
		pprof.Lookup("goroutine").WriteTo(os.Stdout, 1)
		panic("Caught signal " + signal.String())
	}()

	db := mime.NewDatabase()
	for _, finame := range os.Args[1:] {
		fi, err := os.Open(finame)
		if err != nil {
			panic(err)
		}
		err = db.ReadFreeDesktop(fi)
		fi.Close()
		if err != nil {
			panic(err)
		}
	}
	fi, err := os.OpenFile("test.json", os.O_TRUNC|os.O_CREATE|os.O_WRONLY, 0777)
	if err != nil {
		panic(err)
	}
	b, err := db.Debug()
	if err != nil {
		panic(err)
	}
	_, err = fi.Write(b)
	if err != nil {
		panic(err)
	}
	fis, err := ioutil.ReadDir(dir)
	if err != nil {
		panic(err)
	}
	header := make([]byte, db.RecommendedLength())
	for _, fiinfo := range fis {
		fi, err := os.Open(dir + fiinfo.Name())
		if err != nil {
			panic(err)
		}
		ext := db.GuessMIMEByName(fiinfo.Name())
		read, err := fi.Read(header)
		if err != nil {
			panic(err)
		}
		head := db.GuessMIMEByHeader(header[:read])
		ok := "OK"
		if head != ext {
			ok = "Error"
		}
		fmt.Printf("%v: %v (header) and %v (extension) for %v\n", ok, head, ext, fiinfo.Name())
		fi.Close()
	}
}
