package mime

import "bytes"

//GuessMIMEByHeader attemtps to deterimine the MIME type by the file's header. It returns "" on failure
func (d *Database) GuessMIMEByHeader(header []byte) string {
	n := d.db.Magic
	for n != nil {
		for _, magic := range n.Magic {
			if isMatch(magic.Matches, header) {
				return magic.Mime
			}
		}
		n = n.Next
	}
	return ""
}

func isMatch(magics []magic, header []byte) bool {
	for _, magic := range magics {
		end := len(magic.Value) + magic.Offset
		if len(header) >= end {
			if bytes.Equal(header[magic.Offset:end], magic.Value) {
				if len(magic.Sub) > 0 {
					if isMatch(magic.Sub, header) {
						return true
					}
				} else {
					return true
				}
			}
		}
	}
	return false
}

//RecommendedLength returns the length required by GuessMIMEByHeader for all matches
func (d *Database) RecommendedLength() int {
	return d.db.MaxLen
}
