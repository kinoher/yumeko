Mime is a relativly fast mime detection library using data from
[FreeDesktop's Shared mime info spec](https://specifications.freedesktop.org/shared-mime-info-spec/latest/ar01s02.html).

It is not a 100% implementation of the specification designed for webservers. 
It skips non-extension globs such as `AUTHORS` because most people dont want a 
file called `AUTHORS` to have the mime type `text/x-authors`