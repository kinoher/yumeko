package mime

import "strings"

//GuessMIMEByName guesses a mime type by filename. This is faster than guessing by magic, but very easy to fool
//returns a empty string on failure
func (d *Database) GuessMIMEByName(name string) string {
	for {
		index := strings.IndexByte(name, '.')
		if index == -1 {
			return ""
		}
		name = name[index:]
		mime, ok := d.db.Ext2mime[name]
		if ok {
			return mime
		}
		name = name[1:]
	}
}

//GuessExtByMIME attempts to get a file extension (including the dot) by a mime type.
func (d *Database) GuessExtByMIME(mime string) string {
	return d.db.Mime2ext[mime]
}
