//package mime implements part of the shared-mime-info spec for guessing by extension and partial matching by header
package mime

import (
	"encoding/binary"
	"encoding/xml"
	"fmt"
	"io"
	"strconv"
	"strings"
)

var big = binary.BigEndian
var little = binary.LittleEndian
var host = binary.BigEndian

//Database holds the LUTs for glob matching and magic matching
type Database struct {
	db database
}
type database struct {
	Ext2mime map[string]string
	Mime2ext map[string]string
	Magic    *magicLL
	MaxLen   int
}

type magicLL struct {
	Magic    []tlMagic
	Priority int
	Next     *magicLL
}

type tlMagic struct {
	Mime    string
	Matches []magic
}

type magic struct {
	Value  []byte
	Offset int
	Sub    []magic
}

//NewDatabase Creates a new empty database
func NewDatabase() *Database {
	return &Database{database{
		make(map[string]string),
		make(map[string]string),
		nil,
		0,
	}}
}

type xmlDB struct {
	MimeTypes []struct {
		Type string `xml:"type,attr"`
		Glob []struct {
			Pattern string `xml:"pattern,attr"`
		} `xml:"glob"`
		Magic []struct {
			Priority *int       `xml:"priority,attr"`
			Match    []xmlMatch `xml:"match"`
		} `xml:"magic"`
	} `xml:"mime-type"`
}

type xmlMatch struct {
	Match  []xmlMatch `xml:"match"`
	Value  string     `xml:"value,attr"`
	Type   string     `xml:"type,attr"`
	Offset string     `xml:"offset,attr"`
}

func pint(bits int, val string) (uint64, error) {
	if strings.HasPrefix(val, "0x") {
		return strconv.ParseUint(val[2:], 16, bits)
	}
	return strconv.ParseUint(val, 8, bits)
}

//ReadFreeDesktop reads into a existing database from a XML document stream describing mime matching
func (db *Database) ReadFreeDesktop(database io.Reader) error {
	xmldb := xmlDB{}
	err := xml.NewDecoder(database).Decode(&xmldb)
	if err != nil {
		return err
	}
	for _, v := range xmldb.MimeTypes {
		mime := v.Type
		for i, glob := range v.Glob {
			ext := glob.Pattern
			if !strings.HasPrefix(ext, "*.") {
				continue
			}
			ext = ext[1:]
			db.db.Ext2mime[ext] = mime
			if i == 0 {
				db.db.Mime2ext[mime] = ext
			}
		}
		for _, imagic := range v.Magic {
			matches, ml, err := match2match(imagic.Match)
			if err != nil {
				return err
			}
			if len(matches) == 0 {
				continue
			}
			if db.db.MaxLen < ml {
				db.db.MaxLen = ml
			}
			omagic := tlMagic{
				Mime:    mime,
				Matches: matches,
			}
			var lm *magicLL
			mm := db.db.Magic
			for {
				priority := 50
				if imagic.Priority != nil {
					priority = *imagic.Priority
				}
				if mm == nil {
					nm := &magicLL{
						Magic:    []tlMagic{omagic},
						Priority: priority,
						Next:     nil,
					}
					if lm == nil {
						db.db.Magic = nm
					} else {
						lm.Next = nm
					}
					break
				}
				if mm.Priority == priority {
					mm.Magic = append(mm.Magic, omagic)
					break
				} else if mm.Priority < priority {
					nm := &magicLL{
						Magic:    []tlMagic{omagic},
						Priority: priority,
						Next:     mm,
					}
					if lm == nil {
						db.db.Magic = nm
					} else {
						lm.Next = nm
					}
					break
				}
				lm = mm
				mm = mm.Next
			}
		}
	}
	//fmt.Printf("%+v\n", xmldb)
	return nil
}

func match2match(in []xmlMatch) ([]magic, int, error) {
	if len(in) == 0 {
		return nil, 0, nil
	}
	out := make([]magic, 0, len(in))
	ml := 0
	for _, in := range in {
		var value []byte
		offset, err := strconv.Atoi(in.Offset)
		if err != nil {
			continue
		}
		switch in.Type {
		case "string":
			v, err := unescape(in.Value)
			if err != nil {
				return nil, 0, err
			}
			value = v
		case "big16":
			value = []byte{0, 0}
			v, err := pint(16, in.Value)
			if err != nil {
				return nil, 0, err
			}
			big.PutUint16(value, uint16(v))
		case "big32":
			value = []byte{0, 0, 0, 0}
			v, err := pint(32, in.Value)
			if err != nil {
				return nil, 0, err
			}
			big.PutUint32(value, uint32(v))
		case "little16":
			value = []byte{0, 0}
			v, err := pint(16, in.Value)
			if err != nil {
				return nil, 0, err
			}
			big.PutUint16(value, uint16(v))
		case "little32":
			value = []byte{0, 0, 0, 0}
			v, err := pint(32, in.Value)
			if err != nil {
				return nil, 0, err
			}
			little.PutUint32(value, uint32(v))
		case "host16":
			value = []byte{0, 0}
			v, err := pint(16, in.Value)
			if err != nil {
				return nil, 0, err
			}
			host.PutUint16(value, uint16(v))
		case "host32":
			value = []byte{0, 0, 0, 0}
			v, err := pint(32, in.Value)
			if err != nil {
				return nil, 0, err
			}
			host.PutUint32(value, uint32(v))
		case "byte":
			v, err := pint(8, in.Value)
			if err != nil {
				return nil, 0, err
			}
			value = []byte{uint8(v)}
		default:
			panic(fmt.Errorf("Type %v not supported", in.Type))
		}

		sub, nml, err := match2match(in.Match)
		if err != nil {
			return nil, 0, err
		}
		if ml < nml {
			ml = nml
		}
		nml = offset + len(value)
		if ml < nml {
			ml = nml
		}
		out = append(out, magic{
			Value:  value,
			Offset: offset,
			Sub:    sub,
		})
	}
	if len(out) == 0 {
		return nil, 0, nil
	}
	return out, ml, nil
}

func unescape(in string) ([]byte, error) {
	out := make([]byte, 0, len(in))
	for i := 0; i < len(in); i++ {
		b := in[i]
		if b == '\\' {
			i++
			b = in[i]
			switch b {
			case 'a':
				b = '\a'
			case 'b':
				b = '\b'
			case 'f':
				b = '\f'
			case 'n':
				b = '\n'
			case 'r':
				b = '\r'
			case 't':
				b = '\t'
			case 'v':
				b = '\v'
			case '\\':
				b = '\\'
			case '\'':
				b = '\''
			case '"':
				b = '"'
			case '?':
				b = '?'
			case 'x':
				acc := byte(0)
				i++
				for {
					if i >= len(in) {
						break
					}
					b = in[i]
					lb := b | 0x20
					if (b >= '0' && b <= '9') || (lb >= 'a' && lb <= 'f') {
						i++
						if lb >= 'a' {
							b = lb - 'a' - 10
						} else {
							b -= '0'
						}
						acc = acc<<4 | b
					} else {
						break
					}
				}
				i--
				b = acc
			default:
				if b >= '0' && b <= '9' { //even though 8 and 9 make no sense I have to not crash because freedesktop allows them for some reason, only in proprietary tga headers
					acc := byte(0)
					for l := 0; l < 3; l++ {
						if i < len(in) && in[i] >= '0' && in[i] <= '9' {
							acc = (acc << 3) | (in[i] - '0')
							i++
						} else {
							break
						}
					}
					i--
					b = acc
				} else {
					return nil, fmt.Errorf("unescape:\\%c is not a valid escape", b)
				}
			}
		}
		out = append(out, b)
	}
	return out, nil
}
