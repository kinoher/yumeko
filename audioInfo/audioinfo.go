// Copyright 2017 Yumeko Contributors (as named in CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the LICENSE file

package audioInfo

import (
	"bytes"
	"errors"
	"os/exec"
	"regexp"
	"strconv"
	"strings"
)

var matchMaxVolume = regexp.MustCompile("\\[.*volumedetect.*\\] max_volume: ([0-9.-]+) dB")
var matchFileExt = regexp.MustCompile("Input #0.*, ?([^,]+), from")

//GetExtension extracts extension from ffmpeg
func GetExtension(name string) (string, error) {
	ffvoldet := exec.Command("ffmpeg", append([]string{"-i", name}, strings.Split("-hide_banner -f null -", " ")...)...)
	pipe, err := ffvoldet.StderrPipe()
	if err != nil {
		return "", err
	}
	ffvoldet.Start()

	buf := &bytes.Buffer{}
	buf.ReadFrom(pipe)
	str := buf.String()
	matches := matchFileExt.FindStringSubmatch(str)
	if len(matches) < 1 {
		return "", errors.New("Cannot find extension")
	}
	info := matches[len(matches)-1]

	ffvoldet.Wait()
	return info, nil
}

//GetReplayGain extracts volume normalization info with ffmpeg -af volumedetect
func GetReplayGain(name string) (float64, error) {
	ffvoldet := exec.Command("ffmpeg", append([]string{"-i", name}, strings.Split("-hide_banner -af volumedetect -f null -", " ")...)...)
	pipe, err := ffvoldet.StderrPipe()
	if err != nil {
		return 0, err
	}
	ffvoldet.Start()
	go ffvoldet.Wait()

	buf := &bytes.Buffer{}
	buf.ReadFrom(pipe)
	str := buf.String()
	matches := matchMaxVolume.FindStringSubmatch(str)
	if len(matches) < 1 {
		return 0, errors.New("Cannot find volume")
	}
	info, err := strconv.ParseFloat(matches[len(matches)-1], 64)
	if err != nil {
		return 0, err
	}
	return info, nil
}

//Info holds data from GetInfo
type Info struct {
	//Duration is in seconds
	Duration int
	Artist   string
	Title    string
}

var matchDuration = regexp.MustCompile("(?i)duration=([^\n\r]+)")
var matchArtist = regexp.MustCompile("(?i)TAG:ARTIST=([^\n\r]+)")
var matchTitle = regexp.MustCompile("(?i)TAG:TITLE=([^\n\r]+)")

//GetInfo extracts some data from ffprobe
func GetInfo(name string) (*Info, error) {
	cmd := exec.Command("ffprobe", append([]string{"-i", name}, strings.Split("-hide_banner -show_format", " ")...)...)
	byt, err := cmd.Output()
	str := string(byt)
	if err != nil {
		return nil, err
	}
	info := &Info{}
	{
		matches := matchDuration.FindStringSubmatch(str)
		if len(matches) > 0 {
			durF, _ := strconv.ParseFloat(matches[len(matches)-1], 64)
			info.Duration = int(durF)
		}
	}
	{
		matches := matchArtist.FindStringSubmatch(str)
		if len(matches) > 0 {
			info.Artist = matches[len(matches)-1]
		}
	}
	{
		matches := matchTitle.FindStringSubmatch(str)
		if len(matches) > 0 {
			info.Title = matches[len(matches)-1]
		}
	}
	return info, nil
}
