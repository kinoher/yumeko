// Copyright 2017 Yumeko Contributors (as named in CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the LICENSE file

package main

import (
	"fmt"

	"bitbucket.org/abex/yumeko/audioInfo"
)

func main() {
	file := "C:\\yumekoetc\\clips\\123412344"
	fmt.Println(audioInfo.GetExtension(file))
	fmt.Println(audioInfo.GetReplayGain(file))
	fmt.Println(audioInfo.GetInfo(file))
}
