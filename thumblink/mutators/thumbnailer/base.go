// Copyright 2017 Yumeko Contributors (as named in CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the LICENSE file

package thumbnailer

import (
	"fmt"
	"html"
	"image"
	"image/png"
	"io"
	"math"
	"net/http"
	"path"
	"regexp"
	"strings"

	errpkg "errors"

	"bitbucket.org/abex/yumeko/conf"
	"bitbucket.org/abex/yumeko/ffmpeg"
	"bitbucket.org/abex/yumeko/glug"
	"bitbucket.org/abex/yumeko/libjpeg"
	"bitbucket.org/abex/yumeko/mime"
	"bitbucket.org/abex/yumeko/mime/minimal"
	"bitbucket.org/abex/yumeko/thumblink"
	"bitbucket.org/abex/yumeko/util"
	"bitbucket.org/abex/yumeko/versioning"
)

const (
	emptyPixel = `data:image/jpeg;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAACklEQVR4nGMAAQAABQABDQottAAAAABJRU5ErkJggg==`
	pngHeader  = `data:image/png;base64,`
	jpegHeader = `data:image/jpeg;base64,`
)

var log = glug.NewLogger("thumbnailer")

var maxRemoteRead int
var maxOutputSize int
var MaxImageWidth int
var MaxImageHeight int
var videoOverlay ffmpeg.Frame
var audioOverlay ffmpeg.Frame

func init() {
	conf.Config(&maxRemoteRead, "inliner", `
# How many bytes can
max-remote-read = 10_000_000
`)
	conf.Config(&maxOutputSize, "inliner", `
# How many bytes can a message in mumble be
max-output-size = 131_000
`)
	conf.Config(&MaxImageWidth, "inliner", `
# How wide can a image be 
max-width = 500
`)
	conf.Config(&MaxImageHeight, "inliner", `
# How tall can a image be
max-height = 800
`)
}

//Mutator implements thumblink.Mutator.
type Mutator struct {
}

var pngEnc = &png.Encoder{
	CompressionLevel: png.DefaultCompression,
}

var contentDispositionMatcher = regexp.MustCompile(`filename[^;=]*=(?:"([^"]*)"|'([^']*)'|([^"' ]*))`)
var matchdb *mime.Database

func init() {
	matchdb = mime.NewDatabase()
	minimal.Include(matchdb)
}

type AnimMode int

const (
	AnimModeGuess AnimMode = iota
	AnimModeNormal
	AnimModeVideo
	AnimModeAudio
)

var errIsHTML = errpkg.New("text/html is not supported.")

// This function does almost all of the /work/ of inlining media.
// It retrieves it, guesses animType if needed, demuxes, decodes,
// scales, draws, rescales and encodes the final image.
// It has serveral anonymous functions to contain scope, and more importantly
// allow `defer`s to be ran earlier than they would otherwise, as
// `ffmpeg` uses manual memory management. This limits scope to prevent
// leaks and ensures resources are freed once they are unused
// They also serve to segment the code, without loosing context.
func work(url string, animMode AnimMode, maxRead, maxLen int, mime string, maxWidth, maxHeight int) (*Base64Data, error) {
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}
	versioning.SetUserAgent(req)
	res, err := util.Client.Do(req)
	if err != nil {
		return nil, err
	}
	body := thumblink.NewPeekable(&io.LimitedReader{R: res.Body, N: int64(maxRead)}, 0)

	// +DetectMime
	err = func() error {
		if mime == "" { // Try the header
			mime = res.Header.Get("content-type")
		}
		if mime == "" || strings.HasPrefix(mime, "application/") { //sniff
			cd := res.Header.Get("Content-Disposition")
			name := ""
			if len(cd) > len("inline;filename='.'") { //Guess the filename by Content-Disposition
				cds := contentDispositionMatcher.FindStringSubmatch(cd)
				for i := 0; i < len(cds); i++ {
					if len(cds[i]) > 0 {
						name = cds[i]
						break
					}
				}
			}
			if name == "" { //Guess name by path
				name = path.Base(res.Request.URL.Path)
			}
			if name != "" {
				mime = matchdb.GuessMIMEByName(name)
			}
		}
		if mime == "" { //Try to guess the MIME by contents
			header := make([]byte, matchdb.RecommendedLength())
			read, err := body.Peeker().Read(header)
			if err != nil && err != io.EOF {
				return err
			}
			mime = matchdb.GuessMIMEByHeader(header[:read])
		}
		if mime == "" {
			return fmt.Errorf("%v: Cannot guess MIME type.", url)
		}
		if !(strings.HasPrefix(mime, "image/") || strings.HasPrefix(mime, "video/") || strings.HasPrefix(mime, "audio/")) {
			if mime == "text/html" {
				return errIsHTML
			}
			return fmt.Errorf("%v: '%v' is not a supported MIME type.", url, mime)
		}
		return nil
	}() // -DetectMime
	if err != nil {
		res.Body.Close()
		return nil, err
	}

	//Acutally read the image
	var frm ffmpeg.Frame
	var orient int
	if mime == "image/jpeg" {
		frm, orient, animMode, err = ReadJPEG(body, animMode, mime, maxWidth, maxHeight)
		if err != jpeg.ErrBadColorspace {
			res.Body.Close()
			if err != nil {
				return nil, fmt.Errorf("%v: %v", url, err)
			}
		}
	}
	if frm == 0 {
		frm, orient, animMode, err = ReadFFmpeg(body, animMode, mime, maxWidth, maxHeight)
		res.Body.Close()
		if err != nil {
			return nil, fmt.Errorf("%v: %v", url, err)
		}
	}
	defer func() { frm.Free() }()

	// +PreliminaryResize
	err = func() error {
		//If the image is huge, or it is in a /weird/ format, use libswscale to resize/sample it
		width, height := frm.Size()
		do := width > maxWidth*2 || height > maxHeight*2
		if !do {
			gimg := frm.GoImg()
			if gimg == nil {
				do = true
			} else {
				_, ok := gimg.(*image.NRGBA)
				do = !ok
			}
		}
		if do {
			if width > maxWidth*2 || height > maxHeight*2 {
				scale := math.Min(math.Min(float64(maxWidth)/float64(width), float64(maxHeight)/float64(height)), 1)
				width = int(float64(width) * scale)
				height = int(float64(height) * scale)
			}
			//Alloc a frame
			nf, err := ffmpeg.MakeFrame()
			if err != nil {
				return err
			}
			defer func() { nf.Free() }() //pass by reference
			if frm.HasAlpha() {
				err = nf.FillRGBA(width, height)
			} else {
				err = nf.FillYCbCr420(frm.Size())
			}
			if err != nil {
				return err
			}
			//Scale
			err = frm.Scale(nf, ffmpeg.SWSBilinear)
			if err != nil {
				return err
			}
			frm, nf = nf, frm
		}
		return nil
	}() // -PreliminaryResize
	if err != nil {
		return nil, fmt.Errorf("%v: %v", url, err)
	}

	maxLen = (maxLen / 4) * 3
	write := newWriteCounter(maxLen)

	fiw, fih := frm.Size()
	fw, fh := float64(fiw), float64(fih)
	scale := math.Min(math.Min(float64(maxWidth)/fw, float64(maxHeight)/fh), 1)
	maxSize := float64(maxLen)

	pngFactor := 1.0
	tryPng := func() error {
		min, err := ffmpeg.MakeFrame()
		if err != nil {
			return err
		}
		defer min.Free()
		min.FillRGBA(int(fw*scale*pngFactor), int(fh*scale*pngFactor))
		err = frm.Scale(min, ffmpeg.SWSBilinear)
		if err != nil {
			return err
		}
		err = pngEnc.Encode(write, min.GoImg())
		if err != nil {
			return err
		}
		pngFactor *= math.Min(.9, maxSize/float64(write.Len()))
		return nil
	}

	jpegFactor := 1.0
	tryJpeg := func() error {
		min, err := ffmpeg.MakeFrame()
		if err != nil {
			return err
		}
		defer min.Free()
		min.FillYCbCr420(int(fw*scale*jpegFactor), int(fh*scale*jpegFactor))
		err = frm.Scale(min, ffmpeg.SWSBilinear)
		if err != nil {
			return err
		}
		err = jpeg.Encode(write, min.GoImg(), &jpeg.EncoderOptions{
			Quality:        int(85.0 * jpegFactor),
			OptimizeCoding: false,
			DCTMethod:      jpeg.DCTFloat,
		})
		if err != nil {
			return err
		}
		jpegFactor *= math.Min(.9, maxSize/float64(write.Len()))
		return nil
	}

	pngerr := tryPng()
	pngSize := write.Len()
	write.Reset()
	jpegerr := tryJpeg()
	jpegSize := write.Len()
	write.Reset()

	if pngerr != nil && jpegerr != nil {
		return nil, glug.Wrapf(glug.Combine([]error{pngerr, jpegerr}), "%v: Unable to encode! ", url)
	}
	usePNG := (pngerr == nil && pngSize < jpegSize) || jpegerr != nil
	// +Filter
	err = func() error {
		var overlay ffmpeg.Frame
		if animMode == AnimModeAudio {
			overlay = audioOverlay
		} else if animMode == AnimModeVideo {
			overlay = videoOverlay
		}
		needsOverlay := overlay != 0
		needsRotate := orient > 1
		needsBackground := !usePNG && frm.HasAlpha()

		if !(needsOverlay || needsRotate || needsBackground) {
			return nil
		}

		cfg := ""
		if needsBackground {
			cfg += "[under][in] overlay"
		}
		if needsRotate {
			if orient > 4 {
				fiw, fih = fih, fiw
				fw, fh = float64(fiw), float64(fih)
				scale = math.Min(math.Min(float64(maxWidth)/fw, float64(maxHeight)/fh), 1)
			}
			if len(cfg) > 0 {
				cfg += ","
			} else {
				cfg = "[in] "
			}
			cfg += []string{
				"",
				"",
				"hflip",
				"hflip,vflip",
				"vflip",
				"transpose=cclock_flip",
				"transpose=clock",
				"transpose=clock_flip",
				"transpose=cclock",
			}[orient] //cclock_flip clock_flip clock cclock
		}
		if needsOverlay {
			name := "in"
			if len(cfg) > 0 {
				cfg += "[imed] ;"
				name = "imed"
			}

			oiw, oih := overlay.Size()
			ow, oh := float64(oiw), float64(oih)
			scale := math.Min(math.Min(fw/ow, fh/oh), 1)
			oow, ooh := int(ow*scale), int(oh*scale)
			ox, oy := (fiw-oow)/2, (fih-ooh)/2
			cfg += fmt.Sprintf("[over] scale=w=%v:h=%v [over]; [%v] [over] overlay=x=%v:y=%v", oow, ooh, name, ox, oy)
		}

		flt, err := ffmpeg.MakeFilter()
		defer flt.Free()
		if err != nil {
			return err
		}
		in, err := flt.AddInput(frm, "in")
		if err != nil {
			return err
		}
		defer in.Free()
		var ulframe ffmpeg.Frame
		var bg ffmpeg.FilterInput
		if needsBackground {
			ulframe, err = ffmpeg.MakeFrame()
			if err != nil {
				return err
			}
			defer ulframe.Free()
			err = ulframe.FillWhite(frm.Size())
			if err != nil {
				return err
			}
			bg, err = flt.AddInput(ulframe, "under")
			if err != nil {
				return err
			}
			defer bg.Free()
		}
		var ovl ffmpeg.FilterInput
		if needsOverlay {
			ovl, err = flt.AddInput(overlay, "over")
			if err != nil {
				return err
			}
			defer ovl.Free()
		}

		err = flt.Configure(cfg, !usePNG)
		if err != nil {
			return err
		}

		frm.ClearTimestamp()
		err = in.Add(frm)
		if err != nil {
			return err
		}
		if needsBackground {
			err = bg.Add(ulframe)
			if err != nil {
				return err
			}
		}
		if needsOverlay {
			err = ovl.Add(overlay)
			if err != nil {
				return err
			}
		}
		ofrm, err := ffmpeg.MakeFrame()
		defer func() { ofrm.Free() }()
		if err != nil {
			return err
		}
		err = flt.Get(ofrm)
		if err != nil {
			return err
		}
		ofrm, frm = frm, ofrm

		return nil
	}() // -Filter
	if err != nil {
		return nil, fmt.Errorf("%v: %v", url, err)
	}

	tryMethod := tryJpeg
	header := jpegHeader
	if usePNG {
		tryMethod = tryPng
		header = pngHeader
	}

	for i := 0; i < 20; i++ {
		err = tryMethod()
		if err != nil {
			return nil, err
		}
		size := write.Len()
		if size < maxLen {
			return write.Base64(header), nil
		}
		write.Reset()
	}

	return nil, fmt.Errorf("%v: Too many attemts to encode", url)
}

func Startup() {
	conf.OpenFile("video.png", func(_ struct{}, err error) {
		if err != nil {
			log.Errorf("Error opening video.png: %v", err)
		}
		if videoOverlay != 0 {
			videoOverlay.Free()
			videoOverlay = 0
		}
		videoOverlay, err = ffReadFrame(conf.Path("video.png"))
		if err != nil {
			log.Errorf("Error opening video.png: %v", err)
			panic(err)
		}
		videoOverlay.ClearTimestamp()
	})
	conf.OpenFile("audio.png", func(_ struct{}, err error) {
		if err != nil {
			log.Errorf("Error opening audio.png: %v", err)
		}
		if audioOverlay != 0 {
			audioOverlay.Free()
			audioOverlay = 0
		}
		audioOverlay, err = ffReadFrame(conf.Path("audio.png"))
		if err != nil {
			log.Errorf("Error opening audio.png: %v", err)
			panic(err)
		}
		audioOverlay.ClearTimestamp()
	})
}

//Mutate impl thumblink.Mutator
func (m Mutator) Mutate(in []thumblink.HTMLAble) (out []thumblink.HTMLAble, errors []error) {
	count := 0
	maxLen := maxOutputSize
	for _, iin := range in {
		switch iin := iin.(type) {
		case *thumblink.Link:
			count++
			maxLen -= 35 + len(iin.String())
		case Image:
			count++
			maxLen -= 35 + len(iin.URL)
		default:
			maxLen -= iin.Len()
		}
	}
	if count <= 0 {
		return in, nil
	}
	maxRead := maxRemoteRead / count
	maxLen /= count
	maxLen = 3 * (maxLen / 4)
	grid := int(math.Ceil(math.Sqrt(float64(count))))
	maxHeight := MaxImageHeight / grid
	maxWidth := MaxImageWidth / grid
	if maxLen < 0 {
		return in, []error{errpkg.New("Too much data!")}
	}
	out = make([]thumblink.HTMLAble, 0, len(in))
	for _, item := range in {
		switch item := item.(type) {
		case *thumblink.Link:
			data, err := work(item.String(), AnimModeGuess, maxRead, maxLen, "", maxWidth, maxHeight)
			if err != nil {
				if err != errIsHTML {
					errors = append(errors, err)
				}
				out = append(out, item)
			} else {
				out = append(out, thumblink.HTML{`<a href="`, html.EscapeString(item.String()), `"><img src="`}, data, thumblink.HTML{`"/></a>`})
			}
		case Image:
			var err error
			var data thumblink.HTMLAble
			nmaxWidth := maxWidth
			nmaxHeight := maxHeight
			if item.MaxSize != 0 {
				nmaxWidth = int(float64(maxWidth) / item.MaxSize)
				nmaxHeight = int(float64(maxHeight) / item.MaxSize)
			}
			iurl := item.ImageURL
			if iurl == "" {
				iurl = item.URL
			}
			durl := item.URL
			if durl == "" {
				durl = item.ImageURL
			}
			data, err = work(iurl, item.Animated, maxRead, maxLen, item.MIMEOverride, nmaxWidth, nmaxHeight)
			hideError := item.Flags&ImageFlagHideError != 0
			skipImg := item.Flags&ImageFlagSkipImg != 0
			skipLink := item.Flags&ImageFlagSkipLink != 0
			silent := item.Flags&ImageFlagSilent != 0
			if err != nil {
				if silent {
					if skipImg {
						data = thumblink.HTML{emptyPixel}
					} else {
						data = nil
					}
				} else {
					ll := html.EscapeString(durl)
					data = thumblink.HTML{`<a href="`, ll, `">`, ll, `</a>`}
					skipImg = true
				}
				if !hideError && err != errIsHTML {
					errors = append(errors, err)
				}
			}
			if data != nil {
				if skipImg {
					out = append(out, data)
				} else if skipLink {
					out = append(out, thumblink.HTML{`<img src="`}, data, thumblink.HTML{`"/>`})
				} else {
					out = append(out, thumblink.HTML{`<a href="`, html.EscapeString(durl), `"><img src="`}, data, thumblink.HTML{`"/></a>`})
				}
			}
		default:
			out = append(out, item)
		}
	}
	return out, errors
}

func (_ Mutator) Help() string {
	return "images, videos and audio."
}
