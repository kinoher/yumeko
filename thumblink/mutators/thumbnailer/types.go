// Copyright 2017 Yumeko Contributors (as named in CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the LICENSE file

package thumbnailer

import (
	"encoding/base64"
	"html"

	"bitbucket.org/abex/yumeko/thumblink"
)

type ImageFlags uint8

const (
	//Do not report a error if failure occures
	ImageFlagHideError ImageFlags = 1 << iota
	//Do not wrap with <img> tags
	ImageFlagSkipImg
	//Do not wrap with <a> tags
	ImageFlagSkipLink
	//Do not output anything on failure
	//If skipImg is set it will output a 1px x 1px transparent png
	ImageFlagSilent
)

//Pass this HTMLAble image down for more control inlining
type Image struct {
	//URL to show to user
	URL string
	//Actual url to download
	ImageURL string
	//Show a 'animated' icon
	Animated AnimMode
	//Bitfield of ImageFlag*
	Flags ImageFlags
	//Override the server's mime type
	MIMEOverride string
	// Max(Width|Height) = thumbnailer.Mutator.Max(Width|Height)/MaxSize
	MaxSize float64
}

//Len impl HTMLAble
func (b Image) Len() int {
	return thumblink.JoinLen(b.HTML())
}

func (b Image) HTML() []string {
	skipImg := b.Flags&ImageFlagSkipImg != 0
	skipLink := b.Flags&ImageFlagSkipLink != 0
	silent := b.Flags&ImageFlagSilent != 0
	if skipImg || silent {
		return []string{}
	}
	ll := html.EscapeString(b.URL)
	if skipLink {
		return []string{ll}
	}
	return []string{"<a href=\"", ll, "\">", ll, "</a>"}
}

//Base64Data represents a data URI in base64 format
type Base64Data struct {
	Header string
	Data   []byte
}

//Len impl HTMLAble
func (b *Base64Data) Len() int {
	return len(b.Header) + (4 * (len(b.Data) / 3))
}

//HTML impl HTMLAble
func (b *Base64Data) HTML() []string {
	return []string{
		b.Header,
		base64.StdEncoding.EncodeToString(b.Data),
	}
}

type writeCounter struct {
	data []byte
	cur  int
}

func newWriteCounter(length int) *writeCounter {
	return &writeCounter{
		data: make([]byte, length),
		cur:  0,
	}
}

func (w *writeCounter) Base64(header string) *Base64Data {
	return &Base64Data{
		Header: header,
		Data:   w.Data(),
	}
}

func (w *writeCounter) Reset() {
	w.cur = 0
}

func (w *writeCounter) Len() int {
	return w.cur
}

func (w *writeCounter) Data() []byte {
	nc := w.cur
	if nc > len(w.data) {
		nc = len(w.data)
	}
	return w.data[:nc]
}

func (w *writeCounter) Write(p []byte) (n int, err error) {
	if w.cur < len(w.data) {
		copy(w.data[w.cur:], p)
	}
	w.cur += len(p)
	return len(p), nil
}
