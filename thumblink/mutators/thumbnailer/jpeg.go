// Copyright 2017 Yumeko Contributors (as named in CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the LICENSE file

package thumbnailer

import (
	"image"
	"math"

	"bitbucket.org/abex/yumeko/ffmpeg"
	jpeg "bitbucket.org/abex/yumeko/libjpeg"
	"bitbucket.org/abex/yumeko/thumblink"
)

func ReadJPEG(body *thumblink.Peekable, animMode AnimMode, mime string, maxWidth, maxHeight int) (ffmpeg.Frame, int, AnimMode, error) {
	if animMode == AnimModeGuess {
		animMode = AnimModeNormal
	}
	config, err := jpeg.DecodeConfig(body.Peeker())
	if err != nil {
		return 0, 0, animMode, err
	}
	orient, err := findOrientation(body.Peeker())
	if err != nil {
		return 0, 0, animMode, err
	}
	cw, ch := config.Width, config.Height
	if orient > 4 {
		cw, ch = ch, cw
	}
	scale := 1 / math.Min(math.Min(float64(maxWidth)/float64(config.Width), float64(maxHeight)/float64(config.Height)), 1)
	if scale >= 2 {
		scale = math.Floor(scale)
		if scale > 8 {
			scale = 8
		}
	} else {
		scale = 1
	}
	iscale := int(scale)
	target := image.Rect(0, 0, config.Width/iscale, config.Height/iscale)
	opts := &jpeg.DecoderOptions{
		ScaleTarget:            target,
		DCTMethod:              jpeg.DCTFloat,
		DisableFancyUpsampling: true,
		DisableBlockSmoothing:  false,
	}
	frm, err := jpeg.Decode(body, opts)
	if err != nil {
		if frm != 0 {
			frm.Free()
		}
		return 0, 0, animMode, err
	}
	return frm, orient, animMode, nil
}
