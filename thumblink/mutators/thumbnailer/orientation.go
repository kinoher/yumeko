// Copyright 2017 Yumeko Contributors (as named in CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the LICENSE file

package thumbnailer

import (
	"bytes"
	"encoding/binary"
	"io"
)

func b16(en binary.ByteOrder, reader io.Reader) (uint16, error) {
	b := []byte{0, 0}
	_, err := io.ReadFull(reader, b)
	return en.Uint16(b), err
}
func b32(en binary.ByteOrder, reader io.Reader) (uint32, error) {
	b := []byte{0, 0, 0, 0}
	_, err := io.ReadFull(reader, b)
	return en.Uint32(b), err
}

func findOrientation(r io.ReadSeeker) (orient int, err error) {
	var en binary.ByteOrder = binary.BigEndian
	soi, err := b16(en, r)
	if err != nil {
		return 0, err
	}
	if soi != 0xFFD8 { //SOI
		return 0, nil
	}
	for { // each JFIF segment
		mark, err := b16(en, r)
		if err != nil {
			return 0, err
		}
		len, err := b16(en, r)
		if err != nil {
			return 0, err
		}
		if mark == 0xFFDA { //SOS
			return 0, nil
		}
		if mark == 0xFFE1 { // APP1
			eh := make([]byte, 6)
			_, err := io.ReadFull(r, eh)
			if err != nil {
				return 0, err
			}
			if !bytes.Equal([]byte{0x45, 0x78, 0x69, 0x66, 0x00, 0x00}, eh) {
				return 0, nil
			}
			iend, err := b16(en, r)
			if err != nil {
				return 0, err
			}
			if iend == 0x4949 { //I
				en = binary.LittleEndian
			}
			sig, err := b16(en, r)
			if err != nil {
				return 0, err
			}
			if sig != 42 {
				return 0, nil
			}
			ofs, err := b32(en, r)
			if err != nil {
				return 0, err
			}
			_, err = r.Seek(int64(ofs)-8, io.SeekCurrent)
			if err != nil {
				return 0, err
			}
			//IFD0
			count, err := b16(en, r)
			if err != nil {
				return 0, err
			}
			//112
			for i := 0; i < int(count); i++ { //each ifd
				tag, err := b16(en, r)
				if tag != 0x112 { //orientation
					_, err = r.Seek(10, io.SeekCurrent)
					if err != nil {
						return 0, err
					}
					continue
				}
				_, err = b16(en, r)
				if err != nil {
					return 0, err
				}
				_, err = b32(en, r)
				if err != nil {
					return 0, err
				}
				rot, err := b16(en, r) //rotation
				return int(rot), err
			}
			return 0, nil
		}
		if mark&0xFF00 != 0xFF00 {
			return 0, nil
		}
		_, err = r.Seek(int64(len)-2, io.SeekCurrent)
		if err != nil {
			return 0, err
		}
	}
	return 0, nil
}
