// Copyright 2016 Yumeko Contributors (as named in CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the LICENSE file

//package google transforms google tracking urls so the content that is pointed to is inlined
package google

import (
	"net/url"
	"strings"

	"bitbucket.org/abex/yumeko/thumblink"
)

//Mutator mutates google links so they are thumbnailed properly
type Mutator struct{}

//Mutate impl thumblink.Mutator
func (_ Mutator) Mutate(in []thumblink.HTMLAble) ([]thumblink.HTMLAble, []error) {
outer:
	for i, mut := range in {
		switch link := mut.(type) {
		case *thumblink.Link:
			uurl := (*url.URL)(link)
			zones := strings.Split(uurl.Hostname(), ".")
			match := true
			if len(zones) < 2 {
				continue outer
			}
			for i := len(zones) - 2; i >= 0; i-- {
				zone := zones[i]
				if len(zone) == 6 && strings.ToLower(zone) == "google" {
					break
				}
				if len(zone) > 3 {
					match = false
					break
				}
			}
			if match {
				key := ""
				switch strings.ToLower(uurl.Path) {
				case "/url":
					key = "url"
				case "/imgres":
					key = "imgurl"
				default:
					continue outer
				}
				query := uurl.Query()
				surl := query.Get(key)
				if surl == "" {
					continue outer
				}
				nsurl, err := url.QueryUnescape(surl)
				if err == nil {
					surl = nsurl
				}
				nurl, err := url.Parse(surl)
				if err != nil {
					continue outer
				}
				in[i] = (*thumblink.Link)(nurl)
			}
		}
	}
	return in, nil
}

func (_ Mutator) Help() string {
	return "Strip google redirector links."
}
