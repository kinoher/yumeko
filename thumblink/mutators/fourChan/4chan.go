// Copyright 2017 Yumeko Contributors (as named in CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the LICENSE file

//package fourChan transforms fourChan so they are inlined
package fourChan

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/url"
	"regexp"
	"strconv"
	"strings"

	"golang.org/x/net/html"

	"net/http"

	"bitbucket.org/abex/yumeko/thumblink"
	"bitbucket.org/abex/yumeko/thumblink/mutators/thumbnailer"
	"bitbucket.org/abex/yumeko/util"
	"bitbucket.org/abex/yumeko/versioning"
)

type thread struct {
	Posts []struct {
		No             int    `json:"no"`
		Name           string `json:"name"`
		Trip           string `json:"trip"`
		Subject        string `json:"sub"`
		Comment        string `json:"com"`
		Filename       string `json:"filename"`
		Time           string `json:"now"`
		ImageID        int64  `json:"tim"`
		ImageExtension string `json:"ext"`
		ImageSize      int64  `json:"fsize"`
		Country        string `json:"country"`
		CountryName    string `json:"country_name"`
		Spoiler        int    `json:"spoiler"`
		CustomSpoiler  int    `json:"custom_spoiler"`
		Replies        int    `json:"replies"`
		Images         int    `json:"images"`
		UniqueIPs      int    `json:"unique_ips"`
		BumpLimit      int    `json:"bumplimit"`
		ImageLimit     int    `json:"imagelimit"`
	} `json:"posts"`
}

func inlinePost(url, board, tid, postStr string) ([]thumblink.HTMLAble, error) {
	uri := fmt.Sprintf("https://a.4cdn.org/%v/thread/%v.json", board, tid)
	req, err := http.NewRequest("GET", uri, nil)
	if err != nil {
		panic(err)
	}
	versioning.SetUserAgent(req)
	res, err := util.Client.Do(req)
	if err != nil {
		return nil, err
	}
	t := thread{}
	defer res.Body.Close()
	err = json.NewDecoder(res.Body).Decode(&t)
	if err != nil {
		return nil, err
	}

	post := t.Posts[0]
	if postStr != "" {
		postNum, _ := strconv.Atoi(postStr)
		for _, p := range t.Posts {
			if p.No == postNum {
				post = p
				break
			}
		}
	}

	out := []thumblink.HTMLAble{}
	lh := thumblink.HTML{
		`<table><tr><td colwidth="2"><span style="font-weight:bold">`,
		post.Name, post.Trip,
		`</span> `,
		post.Subject,
		` `,
	}
	if post.Country != "" {
		country := post.Country
		country = strings.ToLower(country)
		out = append(out, lh, thumbnailer.Image{
			URL:      url,
			ImageURL: `https://s.4cdn.org/image/country/` + country + `.gif`,
			Animated: thumbnailer.AnimModeNormal,
			Flags:    thumbnailer.ImageFlagSkipLink | thumbnailer.ImageFlagSilent,
		})
		lh = thumblink.HTML{}
	}
	lh = append(lh,
		post.Time,
		` <a href="`, html.EscapeString(url), `">No.`,
		fmt.Sprint(post.No),
		`</a>`,
	)
	if post.UniqueIPs > 0 {
		lh = append(lh, " ")
		if post.BumpLimit != 0 {
			lh = append(lh, `<span style="color:#F00;">`)
		}
		lh = append(lh, fmt.Sprint(post.Replies))
		if post.BumpLimit != 0 {
			lh = append(lh, `</span>`)
		}
		lh = append(lh, "/")
		if post.ImageLimit != 0 {
			lh = append(lh, `<span style="color:#F00;">`)
		}
		lh = append(lh, fmt.Sprint(post.Images))
		if post.ImageLimit != 0 {
			lh = append(lh, `</span>`)
		}
		lh = append(lh, "/")
		lh = append(lh, fmt.Sprint(post.UniqueIPs))
	}

	thumbnail := ""
	imageURL := ""
	if post.ImageID != 0 {
		thumbnail = fmt.Sprintf("https://t.4cdn.org/%v/%vs.jpg", board, post.ImageID)
		imageName := fmt.Sprintf("%v%v", post.ImageID, post.ImageExtension)
		imageURL = fmt.Sprintf("https://t.4cdn.org/%v/%v", board, imageName)
		lh = append(lh, ` <a href="`, imageURL, `">`,
			imageName,
			`</a> (`, util.SizeToISO(post.ImageSize), `)`)
	}

	lh = append(lh, `</td></tr><td>`)
	if post.ImageID != 0 {
		out = append(out, lh, thumbnailer.Image{
			URL:      imageURL,
			ImageURL: thumbnail,
		})
		lh = thumblink.HTML{"</td><td>"}
	}

	tokStream := html.NewTokenizer(bytes.NewBufferString(post.Comment))
tokloop:
	for {
		tokType := tokStream.Next()
		switch tokType {
		case html.ErrorToken:
			err := tokStream.Err()
			if err != io.EOF {
				return nil, err
			}
			break tokloop
		case html.StartTagToken:
			tagNameB, more := tokStream.TagName()
			tagName := string(tagNameB)
			if tagName == "span" && more {
				var k []byte
				var v []byte
				for more {
					k, v, more = tokStream.TagAttr()
					if string(k) == "class" && string(v) == "quote" {
						lh = append(lh, `<span style="color:#b5bd68;">`)
						continue tokloop
					}
				}
			}
			if tagName != "br" {
				lh = append(lh, string(tokStream.Raw()))
				break
			}
			fallthrough
		case html.SelfClosingTagToken:
			str := tokStream.Raw()
			carrot := 0
			for i := len(str) - 1; i >= 0; i-- {
				if str[i] == ' ' || str[i] == '\t' || str[i] == '\n' || str[i] == '\r' {
					continue
				}
				if str[i] == '>' && carrot == 0 {
					carrot = i
					continue
				}
				if str[i] == '/' && carrot != 0 {
					carrot = 1
				}
				break
			}
			if carrot != 0 {
				sstr := string(str)
				if carrot != 1 {
					lh = append(lh, sstr[:carrot], "/>")
				} else {
					lh = append(lh, sstr)
				}
			}
		default:
			lh = append(lh, string(tokStream.Raw()))
		}
	}

	lh = append(lh, `</td></tr></table>`)
	out = append(out, lh)

	return out, nil
}

//Mutator mutates fourChan links so they are thumbnailed properly
type Mutator struct{}

var matcher = regexp.MustCompile(`\/([0-9a-z]+)\/thread\/([0-9]+)(?:#p([0-9]+))?`)

//Mutate impl thumblink.Mutator
func (_ Mutator) Mutate(in []thumblink.HTMLAble) ([]thumblink.HTMLAble, []error) {
	out := make([]thumblink.HTMLAble, 0, len(in))
	var errors []error
	for _, mut := range in {
		switch link := mut.(type) {
		case *thumblink.Link:
			url := (*url.URL)(link)
			if link.HostMatch("boards.4chan.org") {
				match := matcher.FindStringSubmatch(url.Path + "#" + url.Fragment)
				if len(match) == 4 {
					nout, nerr := inlinePost(url.String(), match[1], match[2], match[3])
					if nout != nil {
						out = append(out, nout...)
					} else {
						out = append(out, link)
					}
					if nerr != nil {
						errors = append(errors, nerr)
					}
					continue
				}
			}
			out = append(out, link)
		default:
			out = append(out, link)
		}
	}
	return out, errors
}

func (_ Mutator) Help() string {
	return "4chan.org threads and posts."
}
