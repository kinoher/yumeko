// Copyright 2017 Yumeko Contributors (as named in CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the LICENSE file

//package youtube transforms youtube so they are inlined
package youtube

import (
	"encoding/json"
	"errors"
	"html"
	"net/url"
	"strings"

	"net/http"

	"bitbucket.org/abex/yumeko/conf"
	"bitbucket.org/abex/yumeko/glug"
	"bitbucket.org/abex/yumeko/thumblink"
	"bitbucket.org/abex/yumeko/thumblink/mutators/thumbnailer"
	"bitbucket.org/abex/yumeko/util"
	"bitbucket.org/abex/yumeko/versioning"
)

//Mutator mutates youtube links so they are thumbnailed properly
type Mutator struct {
}

var apiKey string

func init() {
	conf.Config(&apiKey, "keys.youtube", `
# Youtube API Key
# https://console.developers.google.com/
key = ""
`)
}

type youtubeVideosAPI struct {
	Items []struct {
		Snippet struct {
			Title      string `json:"title"`
			Channel    string `json:"channelTitle"`
			Thumbnails struct {
				Thumbnail struct {
					URL string `json:"url"`
				} `json:"high"`
			} `json:"thumbnails"`
		} `json:"snippet"`
	} `json:"items"`
}

func (m Mutator) getThumbURL(url string, id string) ([]thumblink.HTMLAble, error) {
	req, err := http.NewRequest("GET", "https://www.googleapis.com/youtube/v3/videos?part=snippet&maxResults=1&key="+apiKey+"&id="+id, nil)
	if err != nil {
		return nil, err
	}
	versioning.SetUserAgent(req)
	res, err := util.Client.Do(req)
	if err != nil {
		return nil, err
	}
	api := youtubeVideosAPI{}
	err = json.NewDecoder(res.Body).Decode(&api)
	res.Body.Close()
	if err != nil {
		return nil, err
	}
	if len(api.Items) < 1 {
		return nil, errors.New("Invalid id.")
	}
	s := api.Items[0].Snippet
	if len(s.Thumbnails.Thumbnail.URL) < 10 {
		return nil, errors.New("Bad API Response")
	}
	return []thumblink.HTMLAble{
		thumblink.HTML{
			html.EscapeString(s.Title),
			" by ",
			html.EscapeString(s.Channel),
			"<br/>",
		},
		thumbnailer.Image{
			URL:      url,
			ImageURL: s.Thumbnails.Thumbnail.URL,
			Animated: thumbnailer.AnimModeVideo,
		},
	}, nil
}

//Mutate impl thumblink.Mutator
func (m Mutator) Mutate(in []thumblink.HTMLAble) ([]thumblink.HTMLAble, []error) {
	out := make([]thumblink.HTMLAble, 0, len(in))
	var errors []error
	for _, mut := range in {
		switch link := mut.(type) {
		case *thumblink.Link:
			url := (*url.URL)(link)
			if link.HostMatch("youtube.com") && strings.HasPrefix(url.Path, "/watch") {
				l, err := m.getThumbURL(url.String(), url.Query().Get("v"))
				if err != nil {
					errors = append(errors, glug.Wrapf(err, "%v: Unable to get metadata", url.String()))
					out = append(out, link)
				} else {
					out = append(out, l...)
				}
			} else if link.HostMatch("youtu.be") && len(url.Path) > 1 {
				l, err := m.getThumbURL(url.String(), url.Path[1:])
				if err != nil {
					errors = append(errors, glug.Wrapf(err, "%v: Unable to get metadata", url.String()))
					out = append(out, link)
				} else {
					out = append(out, l...)
				}
			} else {
				out = append(out, link)
			}
		default:
			out = append(out, link)
		}
	}
	return out, errors
}

func (_ Mutator) Help() string {
	return "youtube.com videos."
}
