// Copyright 2016 Yumeko Contributors (as named in CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the LICENSE file

//package vine transforms vine so they are inlined
package vine

import (
	"encoding/json"
	"strings"

	"html"
	"net/http"
	"net/url"

	"bitbucket.org/abex/yumeko/thumblink"
	"bitbucket.org/abex/yumeko/thumblink/mutators/thumbnailer"
	"bitbucket.org/abex/yumeko/util"
	"bitbucket.org/abex/yumeko/versioning"
)

type api struct {
	ThumbnailURL string `json:"thumbnailUrl"`
	Username     string `json:"username"`
	Description  string `json:"description"`
}

func inline(id, ou string) ([]thumblink.HTMLAble, error) {
	req, err := http.NewRequest("GET", "https://archive.vine.co/posts/"+id+".json", nil)
	versioning.SetUserAgent(req)
	res, err := util.Client.Do(req)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	api := api{}
	err = json.NewDecoder(res.Body).Decode(&api)
	if err != nil {
		return nil, err
	}
	return []thumblink.HTMLAble{
		thumblink.HTML{
			"<p>",
			html.EscapeString(api.Username),
			": ",
			html.EscapeString(api.Description),
			"</p>",
		},
		thumbnailer.Image{
			URL:      ou,
			ImageURL: api.ThumbnailURL,
		},
	}, nil
}

//Mutator mutates vine links so they are thumbnailed properly
type Mutator struct{}

//Mutate impl thumblink.Mutator
func (_ Mutator) Mutate(in []thumblink.HTMLAble) ([]thumblink.HTMLAble, []error) {
	var errors []error
	out := make([]thumblink.HTMLAble, 0, len(in))
	for _, mut := range in {
		switch link := mut.(type) {
		case *thumblink.Link:
			url := (*url.URL)(link)
			if link.HostMatch("vine.co") && strings.HasPrefix(url.Path, "/v/") && len(url.Path) > 5 {
				it, err := inline(url.Path[3:], link.String())
				if err != nil {
					errors = append(errors, err)
					out = append(out, link)
				} else {
					out = append(out, it...)
				}
			} else {
				out = append(out, link)
			}
		default:
			out = append(out, link)
		}
	}
	return out, errors
}

func (_ Mutator) Help() string {
	return "vine.co vines."
}
