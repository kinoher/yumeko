// Copyright 2016 Yumeko Contributors (as named in CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the LICENSE file

//package wiki transforms wiki so they are inlined
package wiki

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"regexp"
	"strings"

	"golang.org/x/net/html"

	"bitbucket.org/abex/yumeko/glug"
	"bitbucket.org/abex/yumeko/thumblink"
	"bitbucket.org/abex/yumeko/thumblink/mutators/thumbnailer"
	"bitbucket.org/abex/yumeko/util"
	"bitbucket.org/abex/yumeko/versioning"
)

//Mutator mutates wiki links so they are thumbnailed properly
type Mutator struct{}

//Mutate impl thumblink.Mutator
func (_ Mutator) Mutate(in []thumblink.HTMLAble) ([]thumblink.HTMLAble, []error) {
	var errors []error
	out := make([]thumblink.HTMLAble, 0, len(in))
	for _, mut := range in {
		switch link := mut.(type) {
		case *thumblink.Link:
			bimg, err := inlineImage((*url.URL)(link))
			if err != nil {
				errors = append(errors, glug.Wrapf(err, "Wiki: %v:", link))
			}
			if bimg != nil {
				out = append(out, bimg)
			} else {
				nout, err := inlineArticle(link)
				if err != nil {
					errors = append(errors, glug.Wrapf(err, "Wiki: %v:", link))
				}
				if nout != nil {
					out = append(out, nout...)
				} else {
					out = append(out, link)
				}
			}
		default:
			out = append(out, link)
		}
	}
	return out, errors
}

type iinfo struct {
	Query struct {
		Pages map[string]struct {
			ImageInfo []struct {
				ThumbURL string `json:"thumburl"`
			} `json:"imageinfo"`
		} `json:"pages"`
	} `json:"query"`
}

func inlineImage(url *url.URL) (thumblink.HTMLAble, error) {
	ind := strings.Index(url.Path, "/File:")
	if ind == -1 {
		return nil, nil
	}
	apiroot := *url
	if ind == 0 {
		apiroot.Path = "/api.php"
	} else {
		apiroot.Path = "/w/api.php"
	}
	query := apiroot.Query()
	query.Set("action", "query")
	query.Set("format", "json")
	query.Set("titles", url.Path[ind+1:])
	query.Set("prop", "imageinfo")
	query.Set("iiprop", "url")
	query.Set("iiurlwidth", fmt.Sprint(thumbnailer.MaxImageWidth))
	apiroot.RawQuery = query.Encode()
	req, err := http.NewRequest("GET", apiroot.String(), nil)
	if err != nil {
		return nil, err
	}
	versioning.SetUserAgent(req)
	res, err := util.Client.Do(req)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()
	v := iinfo{}
	err = json.NewDecoder(res.Body).Decode(&v)
	if err != nil {
		return nil, err
	}
	for _, page := range v.Query.Pages {
		if len(page.ImageInfo) < 1 {
			break
		}
		return thumbnailer.Image{
			URL:      url.String(),
			ImageURL: page.ImageInfo[0].ThumbURL,
		}, nil
	}
	return nil, nil
}

var articleMatcher = regexp.MustCompile(`(?i)\/wiki\/([a-z]{2}(?:-[a-z]{2})?\/)?(.*)`)

type extracts struct {
	Query struct {
		Pages map[string]struct {
			Title     string `json:"title"`
			Extract   string `json:"extract"`
			Thumbnail struct {
				Source string `json:"source"`
			} `json:"thumbnail"`
			Links []struct {
				Title string `json:"title"`
			} `json:"links"`
		} `json:"pages"`
	} `json:"query"`
}

type details struct {
	Items map[string]struct {
		Title     string `json:"title"`
		Abstract  string `json:"abstract"`
		Thumbnail string `json:"thumbnail"`
	} `json:"items"`
}

func inlineArticle(link *thumblink.Link) ([]thumblink.HTMLAble, error) {
	uri := (*url.URL)(link)
	isWikiPath := strings.HasPrefix(uri.Path, "/wiki/")
	if !isWikiPath && !strings.HasPrefix(uri.Path, "/w/index.php") {
		return nil, nil
	}
	var cc, pageTitle string
	apiroot := *uri
	if isWikiPath {
		uriparts := articleMatcher.FindStringSubmatch(uri.Path)
		cc = uriparts[1]
		pageTitle = uriparts[2]
	} else {
		pageTitle = apiroot.Query().Get("title")
	}
	wikia := strings.HasSuffix(uri.Host, "wikia.com")
	if wikia {
		apiroot.Path = "/api/v1/Articles/Details/"
	} else if cc != "" {
		apiroot.Path = fmt.Sprintf("/%vapi.php", cc)
	} else {
		apiroot.Path = "/w/api.php"
	}
	query := url.Values{}
	if wikia {
		query.Set("abstract", "500")
	} else {
		query.Set("action", "query")
		query.Set("format", "json")
		query.Set("prop", "extracts|pageimages|links")
		query.Set("exintro", "true")
		query.Set("pithumbsize", "200")
		query.Set("pllimit", "1")
	}
tryAgain:
	for i := 0; i < 5; i++ {
		query.Set("titles", pageTitle)
		apiroot.RawQuery = query.Encode()

		req, err := http.NewRequest("GET", apiroot.String(), nil)
		if err != nil {
			return nil, err
		}
		versioning.SetUserAgent(req)
		res, err := util.Client.Do(req)
		if err != nil {
			return nil, err
		}
		defer res.Body.Close()

		var title, text string
		var image thumblink.HTMLAble
		if wikia {
			v := details{}
			err = json.NewDecoder(res.Body).Decode(&v)
			if err != nil {
				return nil, err
			}
			for _, page := range v.Items {
				newPage := strings.TrimPrefix(page.Abstract, "REDIRECT ")
				if newPage != page.Abstract {
					pageTitle = newPage
					continue tryAgain
				}
				title = page.Title
				text = html.EscapeString(page.Abstract)
				if page.Thumbnail != "" {
					image = thumbnailer.Image{
						URL:      uri.String(),
						MaxSize:  5,
						ImageURL: page.Thumbnail,
						Flags:    thumbnailer.ImageFlagSilent,
					}
				}
			}
		} else {
			v := extracts{}
			err = json.NewDecoder(res.Body).Decode(&v)
			if err != nil {
				return nil, err
			}
			for _, page := range v.Query.Pages {
				if page.Extract == "" && len(page.Links) > 0 {
					pageTitle = page.Links[0].Title
					continue tryAgain
				}
				title = page.Title
				buf := bytes.Buffer{}
				strlen := 0
				depth := 0
				tokStream := html.NewTokenizer(bytes.NewBufferString(page.Extract))
			tokLoop:
				for {
					tokType := tokStream.Next()
					switch tokType {
					case html.ErrorToken:
						err := tokStream.Err()
						if err != io.EOF {
							return nil, err
						}
						break tokLoop
					case html.StartTagToken:
						depth++
						buf.Write(tokStream.Raw())
					case html.EndTagToken:
						depth--
						buf.Write(tokStream.Raw())
						if depth <= 0 && strlen > 250 {
							break tokLoop
						}
					case html.TextToken:
						strlen += len(tokStream.Raw())
						buf.Write(tokStream.Raw())
					default:
						buf.Write(tokStream.Raw())
					}
				}
				text = buf.String()
				if page.Thumbnail.Source != "" {
					image = thumbnailer.Image{
						URL:      uri.String(),
						MaxSize:  5,
						ImageURL: page.Thumbnail.Source,
						Flags:    thumbnailer.ImageFlagSilent,
					}
				}
			}
		}
		var out []thumblink.HTMLAble
		if image != nil {
			out = append(out, thumblink.HTML{`<table><tr><td>`}, image, thumblink.HTML{`</td><td>`})
		}
		out = append(out, thumblink.HTML{
			`<h3><a href="`, html.EscapeString(uri.String()), `">`,
			html.EscapeString(title),
			`</a></h3><p>`,
			text,
			`</p>`,
		})
		if image != nil {
			out = append(out, thumblink.HTML{`</td></table>`})
		}
		return out, nil
	}
	return nil, fmt.Errorf("redirect recursion")
}

func (_ Mutator) Help() string {
	return "mediawiki and wikia.com images and pages."
}
