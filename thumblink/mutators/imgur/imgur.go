// Copyright 2017 Yumeko Contributors (as named in CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the LICENSE file

//package imgur transforms imgur so they are inlined
package imgur

import (
	"encoding/json"
	"errors"
	"fmt"
	"html"
	"net/http"
	"regexp"
	"strings"

	"bitbucket.org/abex/yumeko/conf"
	"bitbucket.org/abex/yumeko/glug"
	"bitbucket.org/abex/yumeko/thumblink"
	"bitbucket.org/abex/yumeko/thumblink/mutators/thumbnailer"
	"bitbucket.org/abex/yumeko/util"
	"bitbucket.org/abex/yumeko/versioning"
)

//Mutator mutates imgur links so they are thumbnailed properly
type Mutator struct{}

var clientID string
var galleryCount int

func init() {
	conf.Config(&clientID, "keys.imgur", `
# Imgur Client ID
# https://api.imgur.com/oauth2/addclient
client-id = ""
`)
	conf.Config(&galleryCount, "inliner.imgur", `
# How many images can a gallery inline
gallery-count = 4
`)
}

func mklink(url string) thumblink.HTML {
	link := html.EscapeString(url)
	return thumblink.HTML{`<a href="`, link, `">`, link, `</a>`}
}

func (m Mutator) image(link, id string) (out []thumblink.HTMLAble, err error) {
	img := apiImage{}
	err = m.api("/3/image/"+id, &img)
	if err != nil {
		out = []thumblink.HTMLAble{mklink(link)}
		return
	}
	out = img.Data.htmlable(link)
	return
}

func (m Mutator) album(link, id string) (out []thumblink.HTMLAble, err error) {
	alb := apiAlbum{}
	err = m.api("/3/album/"+id, &alb)
	if err != nil {
		out = []thumblink.HTMLAble{mklink(link)}
		return
	}
	d := alb.Data
	Html := thumblink.HTML{
		`<div style="font-size:large;font-weight:bold;"><a href="`,
		html.EscapeString(d.Link),
		`">`,
		html.EscapeString(d.Title),
		`</a></div>`,
	}
	if d.Description != "" {
		Html = append(Html, `<p>`, strings.Replace(html.EscapeString(d.Description), "\n", "<br/>", -1), `</p>`)
	}
	end := len(d.Images)
	if end > galleryCount {
		end = galleryCount
	}
	if end == 0 {
		out = append(out, mklink(link))
		err = errors.New("Bad API Response")
		return
	}
	out = append(out, Html)
	for i := 0; i < end; i++ {
		out = append(out, d.Images[i].htmlable(link)...)
	}
	limited := len(d.Images) - end
	if limited > 0 {
		out = append(out, thumblink.HTML{`<br/><a href="`, html.EscapeString(d.Link), `">` + fmt.Sprint(limited) + ` More...</a>`})
	}
	return
}

func (i image) htmlable(link string) (out []thumblink.HTMLAble) {
	Html := thumblink.HTML{}
	if i.Title != "" {
		Html = append(Html, `<div style="font-size:large;font-weight:bold;">`, html.EscapeString(i.Title), `</div>`)
	}
	if i.Description != "" {
		Html = append(Html, `<p>`, strings.Replace(html.EscapeString(i.Description), "\n", "<br/>", -1), `</p>`)
	}
	if len(Html) > 0 {
		out = append(out, Html)
	}
	iimage := i.Link
	if i.Gifv != "" {
		iimage = i.Gifv
	}
	dot := strings.LastIndexByte(iimage, '.')
	if dot < len(iimage)-5 || dot < len("https://i.imgur.com/") {
		out = []thumblink.HTMLAble{
			mklink(link),
		}
		return
	}
	ext := iimage[dot+1:]
	gifv := ext == "gifv"
	animated := thumbnailer.AnimModeNormal
	if gifv || ext == "mp4" {
		animated = thumbnailer.AnimModeVideo
	}
	var ti thumbnailer.Image
	if ext != "png" {
		ti = thumbnailer.Image{
			URL:          link,
			ImageURL:     iimage[:dot] + "h.jpg",
			Animated:     animated,
			MIMEOverride: "image/jpeg",
		}
	} else {
		ti = thumbnailer.Image{
			URL:      link,
			ImageURL: iimage,
			Animated: animated,
		}
	}
	out = append(out, ti)

	return
}

type apiError struct {
	Data struct {
		Error   string `json:"error"`
		Request string `json:"request"`
	} `json:"data"`
	Success bool `json:"success"`
	Status  int  `json:"status"`
}

type image struct {
	Title       string `json:"title"`
	Description string `json:"description"`
	Link        string `json:"link"`
	Gifv        string `json:"gifv"`
}

type apiImage struct {
	Data image `json:"data"`
}

type apiAlbum struct {
	Data struct {
		Title       string  `json:"title"`
		Description string  `json:"description"`
		Link        string  `json:"link"`
		Images      []image `json:"images"`
	} `json:"data"`
}

func (m Mutator) api(uri string, v interface{}) error {
	req, err := http.NewRequest("GET", "https://api.imgur.com"+uri, nil)
	if err != nil {
		return err
	}
	versioning.SetUserAgent(req)
	req.Header.Add("Authorization", "Client-ID "+clientID)
	res, err := util.Client.Do(req)
	if err != nil {
		return err
	}
	defer res.Body.Close()
	if res.StatusCode != 200 {
		e := &apiError{}
		err = json.NewDecoder(res.Body).Decode(e)
		if err != nil {
			return err
		}
		return errors.New(e.Data.Error)
	}
	return json.NewDecoder(res.Body).Decode(v)
}

var urlmatcher = regexp.MustCompile(`https?:\/\/(?:i\.|www\.|)imgur\.com\/(a\/|gallery\/|)([^\/\.]+)(\.[a-zA-Z0-9]{3,4})?$`)

//Mutate impl thumblink.Mutator
func (m Mutator) Mutate(in []thumblink.HTMLAble) ([]thumblink.HTMLAble, []error) {
	out := make([]thumblink.HTMLAble, 0, len(in))
	errors := []error{}
	for _, mut := range in {
		switch link := mut.(type) {
		case *thumblink.Link:
			if link.HostMatch("imgur.com") {
				l := link.String()
				us := urlmatcher.FindStringSubmatch(l)
				if len(us) >= 3 {
					var o []thumblink.HTMLAble
					var e error
					if us[1] == "" {
						o, e = m.image(l, us[2])
					} else {
						o, e = m.album(l, us[2])
					}
					if e != nil {
						errors = append(errors, glug.Wrapf(e, "Imgur: %v: ", l))
					}
					if o != nil {
						out = append(out, o...)
					}
					continue
				}
			}
			out = append(out, link)
		default:
			out = append(out, link)
		}
	}
	return out, errors
}

func (_ Mutator) Help() string {
	return "imgur.com galleries and albums."
}
