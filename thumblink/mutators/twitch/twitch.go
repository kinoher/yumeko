// Copyright 2017 Yumeko Contributors (as named in CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the LICENSE file

//package twitch transforms twitch so they are inlined
package twitch

import (
	"html"
	"net/url"
	"strings"

	"bitbucket.org/abex/yumeko/commonConfig"
	"bitbucket.org/abex/yumeko/conf"
	"bitbucket.org/abex/yumeko/subhub/twitch"
	"bitbucket.org/abex/yumeko/thumblink"
	"bitbucket.org/abex/yumeko/thumblink/mutators/thumbnailer"
)

var clientid string
var livestreamer bool

func init() {
	conf.Config(&clientid, "keys.twitch", `
# Twitch.tv Client-ID
# https://www.twitch.tv/settings/connections > "Developer Applications"
client-id = ""
`)
	conf.Config(&livestreamer, "inliner.twitch", `
# Enable generation of livesteamer: URIs in twitch messages
# For use with https://bitbucket.org/Abex/livestreamer.user.js/overview
livesteamer = false
`)
}

func inlineChannel(url, channel string) ([]thumblink.HTMLAble, error) {
	cd, err := twitch.GetChannel(clientid, channel)
	if err != nil {
		return nil, err
	}
	lh := thumblink.HTML{
		`</td><td><h4>`,
		html.EscapeString(cd.Status),
		`</h4><br/><a href="`,
		html.EscapeString(url),
		`">`,
		html.EscapeString(cd.DisplayName),
		`</a> streaming `,
		html.EscapeString(cd.Game),
	}
	if livestreamer {
		lh = append(
			lh,
			`<a href="`,
			commonConfig.ExternalHostname,
			`scheme.html#livestreamer:https://twitch.tv/`,
			html.EscapeString(channel),
			`"> (livestreamer)</a>`,
		)
	}
	lh = append(lh, `</td></tr></table>`)
	out := []thumblink.HTMLAble{
		thumblink.HTML{
			`<table><tr><td>`,
		},
		thumbnailer.Image{
			URL:      url,
			ImageURL: cd.Logo,
			Animated: thumbnailer.AnimModeNormal,
			MaxSize:  3,
		},
		lh,
	}
	return out, nil
}

func inlineClip(url, slug string) ([]thumblink.HTMLAble, error) {
	clip, err := twitch.GetClip(clientid, slug)
	if err != nil {
		return nil, err
	}
	return []thumblink.HTMLAble{
		thumblink.HTML{
			`<a href="`,
			html.EscapeString(url),
			`">`,
			html.EscapeString(clip.Title),
			`</a><br/>`,
			`<a href="`,
			html.EscapeString(clip.Broadcaster.ChannelURL),
			`">`,
			html.EscapeString(clip.Broadcaster.DisplayName),
			`</a> playing `,
			html.EscapeString(clip.Game),
			` clipped by `,
			html.EscapeString(clip.Curator.DisplayName),
			`<br/>`,
		},
		thumbnailer.Image{
			URL:      url,
			ImageURL: clip.Thumbnails.Medium,
			Animated: thumbnailer.AnimModeVideo,
		},
	}, nil
}

//Mutator mutates twitch links so they are thumbnailed properly
type Mutator struct{}

//Mutate impl thumblink.Mutator
func (_ Mutator) Mutate(in []thumblink.HTMLAble) ([]thumblink.HTMLAble, []error) {
	out := make([]thumblink.HTMLAble, 0, len(in))
	var errors []error
	for _, mut := range in {
		switch link := mut.(type) {
		case *thumblink.Link:
			url := (*url.URL)(link)
			if link.HostMatch("clips.twitch.tv") {
				if len(url.Path) > 1 {
					nout, nerr := inlineClip(url.String(), url.Path[1:])
					if nout != nil {
						out = append(out, nout...)
					} else {
						out = append(out, link)
					}
					if nerr != nil {
						errors = append(errors, nerr)
					}
					continue
				}
			} else if link.HostMatch("twitch.tv") {
				slash := strings.LastIndexByte(url.Path, '/')
				if slash == 0 || slash == len(url.Path)-1 {
					end := len(url.Path)
					if slash != 0 {
						end = slash
					}
					nout, nerr := inlineChannel(url.String(), url.Path[1:end])
					if nout != nil {
						out = append(out, nout...)
					} else {
						out = append(out, link)
					}
					if nerr != nil {
						errors = append(errors, nerr)
					}
					continue
				}
			}
			out = append(out, link)
		default:
			out = append(out, link)
		}
	}
	return out, errors
}

func (_ Mutator) Help() string {
	return "twitch.tv clips and channels."
}
