// Copyright 2017 Yumeko Contributors (as named in CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the LICENSE file

//package gfycat transform gfycat links so they are inlineable
package gfycat

import (
	"net/url"

	"bitbucket.org/abex/yumeko/thumblink"
	"bitbucket.org/abex/yumeko/thumblink/mutators/thumbnailer"
)

//Mutator mutates gfycat links so they are thumbnailed properly
type Mutator struct{}

//Mutate impl thumblink.Mutator
func (_ Mutator) Mutate(in []thumblink.HTMLAble) ([]thumblink.HTMLAble, []error) {
	for i, mut := range in {
		switch link := mut.(type) {
		case *thumblink.Link:
			url := (*url.URL)(link)
			if link.HostMatch("gfycat.com") && len(url.Path) > 6 {
				in[i] = thumbnailer.Image{
					URL:      link.String(),
					ImageURL: "https://thumbs.gfycat.com" + url.Path + "-poster.jpg",
					Animated: thumbnailer.AnimModeVideo,
				}
			}
		}
	}
	return in, nil
}

func (_ Mutator) Help() string {
	return "gfycat.com videos."
}
