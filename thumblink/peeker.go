// Copyright 2017 Yumeko Contributors (as named in CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the LICENSE file

package thumblink

import (
	"errors"
	"fmt"
	"io"
)

// Peekable allows you to read ahead indefinatly
// It will cache any Read()s made on a Peeker.
// Read()int on a Peekable will destroy cache as it reads.
// Peekers are seekable to any point cached.
type Peekable struct {
	Reader   io.Reader
	data     []block
	len      int
	bufEnd   int
	bufStart int
}

type block struct {
	End  int
	Data []byte
}

type Peeker struct {
	a      *Peekable
	offset int
}

var ErrSeekPastEnd = errors.New("seek past possible end of stream")

func NewPeekable(reader io.Reader, offset int) *Peekable {
	a := &Peekable{
		Reader: reader,
		data:   []block{},
		len:    -1,
	}
	if offset > 0 {
		a.data = append(a.data, block{
			End:  offset,
			Data: nil,
		})
		a.bufEnd = offset
	}
	return a
}

func (a *Peekable) findEnd() error {
	for {
		ri := make([]byte, 8192)
		read, err := io.ReadFull(a.Reader, ri)
		if read > 0 {
			a.data = append(a.data, block{
				End:  a.bufEnd + read,
				Data: ri[:read],
			})
			a.bufEnd += read
		}
		if err != nil {
			if err == io.ErrUnexpectedEOF || err == io.EOF {
				return nil
			}
			return err
		}
	}
}

func (a *Peekable) bufferTo(offset int) error {
	amnt := offset - a.bufEnd
	if amnt < 0 {
		panic("bufferTo: invalid offset")
	}
	if len(a.data) > 0 {
		lel := len(a.data) - 1
		ed := a.data[lel].Data
		if len(ed)+amnt < cap(ed) { // fits
			ri := ed[len(ed) : len(ed)+amnt]
			read, err := io.ReadFull(a.Reader, ri)
			a.data[lel].Data = ri[:len(ed)+read]
			a.data[lel].End += read
			a.bufEnd += read
			return err
		}

		if len(ed)+amnt < 4096 { //combine
			ri := make([]byte, len(ed)+amnt)
			copy(ri, ed)
			read, err := io.ReadFull(a.Reader, ri[len(ed):])
			a.data[lel].Data = ri[len(ed)+read:]
			a.data[lel].End += read
			a.bufEnd += read
			return err
		}
	}
	ri := make([]byte, amnt)
	reead, err := io.ReadFull(a.Reader, ri)
	if reead > 0 {
		ri = ri[:reead]
		a.data = append(a.data, block{
			End:  a.bufEnd + reead,
			Data: ri,
		})
		a.bufEnd += reead
	}
	return err
}

func (a *Peekable) Read(data []byte) (read int, err error) {
	read, err = a.read(data, a.bufStart, true)
	return
}

func (a *Peekable) read(data []byte, offset int, consume bool) (read int, err error) {
	if a.bufEnd > offset { //search
		i := 0
		lgi := 0
		for ; i < len(a.data); i++ {
			if a.data[i].End > offset {
				break
			}
		}
		for ; read < len(data) && i < len(a.data); i++ {
			start := a.data[i].End - len(a.data[i].Data)
			ss := offset - start
			if ss < 0 {
				ss = 0
			}
			se := offset + (len(data) - read) + start
			if se > len(a.data[i].Data) {
				se = len(a.data[i].Data)
			}
			read += copy(data[read:], a.data[i].Data[ss:se])
			if consume {
				rem := a.data[i].Data[se:]
				if len(rem) > 0 {
					a.data[i].Data = a.data[i].Data[se:]
				} else {
					lgi = i
				}
			}
		}
		if consume {
			a.bufStart += read
			a.data = a.data[lgi:]
		}
	}
	if read < len(data) {
		sr := read
		var nr int
		nr, err = a.Reader.Read(data[sr:])
		read += nr
		if !consume && nr > 0 {
			ndata := data[sr:read]
			nsdata := make([]byte, len(ndata))
			copy(nsdata, ndata)
			a.data = append(a.data, block{
				End:  a.bufEnd + nr,
				Data: nsdata,
			})
			a.bufEnd += nr
		}
		if consume {
			a.bufStart += nr
		}
	}
	return
}

func (a *Peekable) calcOffset(offset, mode, co int) (int, error) {
	switch mode {
	case io.SeekStart:
	case io.SeekCurrent:
		offset += co
	case io.SeekEnd:
		if a.len == -1 {
			err := a.findEnd()
			if err != nil {
				return 0, fmt.Errorf("unable to find end: %v", err)
			}
		}
		offset = a.len - offset
	default:
		panic("invalid seek mode")
	}
	if offset < 0 {
		return 0, errors.New("seek past begining of string")
	}
	if offset > a.bufEnd {
		err := a.bufferTo(offset)
		if err != nil {
			return a.bufEnd, err
		}
	}
	return offset, nil
}

func (a *Peekable) Peeker() *Peeker {
	e, _ := a.PeekerOffset(0, io.SeekCurrent)
	return e
}

func (a *Peekable) PeekerOffset(offset, mode int) (*Peeker, error) {
	var err error
	offset, err = a.calcOffset(offset, mode, a.bufStart)
	if err != nil {
		return nil, err
	}
	if offset > a.bufEnd {
		return nil, ErrSeekPastEnd
	}
	return &Peeker{
		a:      a,
		offset: offset,
	}, nil
}

func (r *Peeker) Read(data []byte) (read int, err error) {
	read, err = r.a.read(data, r.offset, false)
	r.offset += read
	return
}

func (r *Peeker) Seek(offset int64, mode int) (int64, error) {
	co, err := r.a.calcOffset(int(offset), mode, r.offset)
	r.offset = co
	return int64(co), err
}
