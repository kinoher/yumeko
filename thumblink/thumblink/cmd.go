// Copyright 2017 Yumeko Contributors (as named in CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the LICENSE file

package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"os/signal"
	"runtime"
	"runtime/debug"
	"runtime/pprof"

	"bitbucket.org/abex/yumeko/conf"
	"bitbucket.org/abex/yumeko/glug"
	"bitbucket.org/abex/yumeko/thumblink/thumblinkrpc"
	"bitbucket.org/abex/yumeko/util"
)

var log = glug.NewLogger("thumbexec")

func main() {
	sigint := make(chan os.Signal, 1)
	signal.Notify(sigint, os.Interrupt)
	go func() {
		signal := <-sigint
		pprof.Lookup("goroutine").WriteTo(os.Stdout, 1)
		panic("Caught signal " + signal.String())
	}()

	debug.SetMaxStack(1024 * 1024 * 300)
	debug.SetGCPercent(15)

	def := util.DefaultConfigDir()
	configPtr := flag.String("config", def, def)
	rpc := flag.Bool("rpc", false, "")
	file := flag.String("file", "", "File to inline")

	flag.Parse()

	_, err := os.Stat(*configPtr)
	if os.IsNotExist(err) {
		err = os.MkdirAll(*configPtr, 0770)
		if err != nil {
			panic(err)
		}
	}
	conf.Load(*configPtr)

	if *file == "" && !*rpc {
		fmt.Println("No file specified. Pass --file <filename>")
		os.Exit(1)
	}
	startup()
	if *file != "" {
		bc, err := ioutil.ReadFile(*file)
		if err != nil {
			panic(err)
		}
		new, err := inline(string(bc))
		if err != nil {
			fmt.Printf("Errors: %v\n", err)
		}
		fmt.Println(new)
	} else {
		if runtime.GOOS == "linux" {
			fi, err := os.OpenFile(fmt.Sprintf("/proc/%v/oom_adj", os.Getpid()), os.O_WRONLY|os.O_TRUNC, 0777)
			if err != nil {
				log.Weirdf("Unable to change oom_adj: %v", err)
			} else {
				_, err = fi.WriteString("12")
				if err != nil {
					log.Weirdf("Unable to change oom_adj: %v", err)
				}
			}
		}
		for {
			msg, err := thumblinkrpc.ReadString(os.Stdin)
			if err != nil {
				panic(err)
			}
			new, ierr := inline(msg)
			err = thumblinkrpc.SendString(os.Stdout, new)
			if err != nil {
				panic(err)
			}
			errstr := ""
			if ierr != nil {
				errstr = ierr.Error()
			}
			err = thumblinkrpc.SendString(os.Stdout, errstr)
			if err != nil {
				panic(err)
			}
		}
	}
}
