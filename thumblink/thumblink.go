// Copyright 2017 Yumeko Contributors (as named in CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the LICENSE file

//Package thumblink parses HTML and replaces links with images.
package thumblink

import (
	"html"
	"net/url"
	"strings"
)

//HTMLAble is a item that can be turned into HTML.
type HTMLAble interface {
	HTML() []string
	Len() int
}

//HTML represents raw HTML. no further transforming should be done
type HTML []string

//HTML impl HTMLAble
func (l HTML) HTML() []string {
	return ([]string)(l)
}

//Len impl HTMLAble
func (l HTML) Len() int {
	return JoinLen(l)
}

//Link is the url of a link
type Link url.URL

//HTML impl HTMLAble
func (l *Link) HTML() []string {
	ll := html.EscapeString(l.String())
	return []string{"<a href=\"", ll, "\">", ll, "</a>"}
}

//Len impl HTMLAble
func (l *Link) Len() int {
	return JoinLen(l.HTML())
}

//HostMatch returns true if the url's host looks like *test(:port)?
func (l *Link) HostMatch(test string) bool {
	host := (*url.URL)(l).Host
	end := len(host)
	for end > len(test) {
		end--
		c := host[end]
		if c == ':' {
			break
		}
		if c > '9' || c < '0' {
			end = len(host)
			break
		}
	}
	return strings.HasSuffix(host[:end], test)
}

//String call url.*URL.String()
func (l *Link) String() string {
	return (*url.URL)(l).String()
}

//Tag represents a html tag.
type Tag []string

//HTML impl HTMLAble
func (t Tag) HTML() []string {
	return ([]string)(t)
}

//Len impl HTMLAble
func (l Tag) Len() int {
	return JoinLen(l)
}

//Text represents text inside of a pair of Tags
type Text []string

//HTML impl HTMLAble
func (t Text) HTML() []string {
	return ([]string)(t)
}

//Len impl HTMLAble
func (l Text) Len() int {
	return JoinLen(l)
}

//Mutators mutate HTML data
type Mutator interface {
	Mutate([]HTMLAble) ([]HTMLAble, []error)
	Help() string
}

func Join(htmlable []HTMLAble) string {
	l := 0
	for _, o := range htmlable {
		l += o.Len()
	}
	out := make([]byte, 0, l)
	for _, o := range htmlable {
		for _, i := range o.HTML() {
			b := ([]byte)(i)
			out = append(out, b...)
		}
	}
	return string(out)
}

func JoinLen(a []string) int {
	l := 0
	for _, s := range a {
		l += len(s)
	}
	return l
}
