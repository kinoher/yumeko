//+build none

package main

import (
	"fmt"
	"html"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"sync"

	"bitbucket.org/abex/yumeko/conf"
	"bitbucket.org/abex/yumeko/thumblink/thumblinkrpc"

	"gitlab.com/yumeko/MumbleEmu/tableTester"
)

var links = map[string]string{
	"4chan/op":       "http://boards.4chan.org/g/thread/51971506",
	"4chan/imagless": "http://boards.4chan.org/news/thread/6#q5006",
	"4chan/flagged":  "http://boards.4chan.org/pol/thread/124205675#q124205675",

	"gfycat/video": "https://gfycat.com/PerfumedConstantAzurevase",

	"google/textlink":             "https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=2&cad=rja&uact=8&ved=0ahUKEwih-MLl3I3WAhWs54MKHZRtDC8QFgi_ATAB&url=http%3A%2F%2Fwww.thesaurus.com%2Fbrowse%2Fthing&usg=AFQjCNGC6oOEwgtGBbAOlu7GIwD5dQrgqg",
	"google/imagelink_thumb":      "https://www.google.com/imgres?imgurl=http%3A%2F%2Fvignette4.wikia.nocookie.net%2Fdeathbattlefanon%2Fimages%2Fa%2Fa9%2FThe_Thing.png%2Frevision%2Flatest%3Fcb%3D20150223191205&imgrefurl=http%3A%2F%2Fdeathbattlefanon.wikia.com%2Fwiki%2FThe_Thing&docid=SQa5RLz8D7pCBM&tbnid=-CjVicHvt-9aoM%3A&vet=10ahUKEwjMmZ7x3I3WAhUr_IMKHStQC70QMwg8KAAwAA..i&w=1977&h=1939&bih=1854&biw=1080&q=thing&ved=0ahUKEwjMmZ7x3I3WAhUr_IMKHStQC70QMwg8KAAwAA&iact=mrc&uact=8",
	"google/imagelink_full":       "https://www.google.com/url?sa=i&rct=j&q=&esrc=s&source=images&cd=&cad=rja&uact=8&ved=0ahUKEwjB77v-3I3WAhUB6oMKHTCkDucQjRwIBw&url=http%3A%2F%2Fdeathbattlefanon.wikia.com%2Fwiki%2FThe_Thing&psig=AFQjCNES6KDF_S_UZhk4bPjIZkTeFs6W1Q&ust=1504689917381298",
	"google/imagelink_view_image": "https://www.google.com/url?sa=i&rct=j&q=&esrc=s&source=images&cd=&cad=rja&uact=8&ved=0ahUKEwjB77v-3I3WAhUB6oMKHTCkDucQjBwIBA&url=http%3A%2F%2Fvignette4.wikia.nocookie.net%2Fdeathbattlefanon%2Fimages%2Fa%2Fa9%2FThe_Thing.png%2Frevision%2Flatest%3Fcb%3D20150223191205&psig=AFQjCNES6KDF_S_UZhk4bPjIZkTeFs6W1Q&ust=1504689917381298",

	"imgur/direct":       "https://i.imgur.com/kpxaKko.png",
	"imgur/indirect":     "http://imgur.com/rbU3Miy",
	"imgur/gallery":      "https://imgur.com/a/Z2OM5",
	"imgur/gif":          "https://imgur.com/2BXq1mI",
	"imgur/specialchars": "https://www.reddit.com/r/dankmemes/comments/84qm0d/woah/",

	"reddit/textpost":      "https://www.reddit.com/r/2007scape/comments/6y1huw/daily_reminder_that_there_is_still_no_delay_on/",
	"reddit/i_reddit_post": "https://www.reddit.com/r/2007scape/comments/6y5jfs/osbuddy_got_me_a_cool_slayer_level_screenshot/",
	"reddit/imgur_gallery": "https://www.reddit.com/r/2007scape/comments/6y31xa/im_the_guy_who_originally_posted_the_tutorial/",
	"reddit/reply":         "https://www.reddit.com/r/2007scape/comments/6y1huw/daily_reminder_that_there_is_still_no_delay_on/dmk36gn/",

	"twitch/channel": "https://www.twitch.tv/odysseyeurobeat",
	"twitch/clip":    "https://clips.twitch.tv/FairBlightedCucumberDatSheffy",

	"twitter/youtube":    "https://twitter.com/autumnelegyrs/status/904861825044549633",
	"twitter/image":      "https://t.co/KLjK1z62IH",
	"twitter/text":       "https://twitter.com/Hanyuu_status/status/904903498969559040",
	"twitter/multiimage": "https://t.co/yTkyTqhrrM",
	"twitter/multiline":  "https://twitter.com/taxiderby/status/934496896604807168",
	"twitter/carrots":    "https://twitter.com/Abex_TM/status/935736528508194817",

	"vine/vine": "https://vine.co/v/itdL5nZvHZa",

	"wiki/wikia_image":                  "https://vignette.wikia.nocookie.net/2007scape/images/c/cc/Recipe_for_Disaster.png/revision/latest?cb=20170321234859",
	"wiki/wikia_article_with_image":     "http://oldschoolrunescape.wikia.com/wiki/Corporeal_Beast",
	"wiki/wikia_article":                "http://oldschoolrunescape.wikia.com/wiki/List_of_quest_series",
	"wiki/wikipedia_article_with_image": "https://en.wikipedia.org/wiki/Adolf_Hitler",
	"wiki/wikipedia_article":            "https://en.wikipedia.org/wiki/Video_game_genre",
	"wiki/wikipedia_image":              "https://en.wikipedia.org/wiki/File:Germanische-ratsversammlung_1-1250x715.jpg",
	"wiki/wikipedia_redirect":           "https://en.wikipedia.org/w/index.php?title=Corporeal_Beast",
	"wiki/wikipedia_video":              "https://upload.wikimedia.org/wikipedia/commons/9/96/Curiosity%27s_Seven_Minutes_of_Terror.ogv",

	"youtube/video": "https://www.youtube.com/watch?v=6rZtfgtDT1w",
}

func init() {
	tabletester.Init()
}

func main() {
	cmd := exec.Command("go", "install", "bitbucket.org/abex/yumeko/thumblink/thumblink")
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	err := cmd.Run()
	if err != nil {
		panic(err)
	}
	if len(os.Args) != 2 {
		panic("Usage: go run run_tests.go [configpath]")
	}
	conf.Load(os.Args[1])
	go func() {
		test := tabletester.NewSuite("rendered_tests", 600, 600)
		defer func() {
			test.Finish()
			tabletester.AllDone()
		}()
		test.AddDefaultStyles()
		jobs := make(chan job)
		wg := &sync.WaitGroup{}
		for i := 0; i < 8; i++ {
			wg.Add(1)
			go func() {
				defer wg.Done()
				il, err := thumblinkrpc.MakeInliner()
				if err != nil {
					panic(err)
				}
				for job := range jobs {
					out := <-il.InlineBlock(job.Str)
					msg := ""
					if out.Errors != "" {
						msg += fmt.Sprintf(`[01:23:45] (Channel) <a href='clientid://fake' class='log-user log-source'>夢子</a>: <a href='clientid://fake' class='log-user log-source'>Tester</a>: %v`, out.Errors)
					}
					if out.Msg != "" {
						if len(msg) > 0 {
							msg += "<br/>"
						}
						msg += fmt.Sprintf(`[01:23:45] (Channel) <a href='clientid://fake' class='log-user log-source'>夢子</a>: <a href='clientid://fake' class='log-user log-source'>Tester</a>: %v`, out.Msg)
					}
					bmsg := []byte(msg)
					os.MkdirAll(filepath.Dir(filepath.Join("rendered_tests", job.Name+".html")), 0775)
					ioutil.WriteFile(filepath.Join("rendered_tests", job.Name+".html"), bmsg, 0775)
					test.Test(job.Name, bmsg)
				}
			}()
		}
		for name, link := range links {
			l := html.EscapeString(link)
			jobs <- job{name, fmt.Sprintf(`<a href="%v">%v</a>`, l, l)}
		}
		jobs <- job{"help", "thumblink help"}
		jobs <- job{"version", "thumblink version"}
		close(jobs)
		wg.Wait()
	}()
	tabletester.Main()
}

type job struct {
	Name string
	Str  string
}
