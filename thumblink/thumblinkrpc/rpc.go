// Copyright 2017 Yumeko Contributors (as named in CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the LICENSE file

package thumblinkrpc

import (
	"encoding/binary"
	"errors"
	"fmt"
	"io"
	"os"
	"os/exec"
	"path/filepath"
	"time"

	"bitbucket.org/abex/yumeko/conf"
	"bitbucket.org/abex/yumeko/glug"
	"github.com/kardianos/osext"
)

var log = glug.NewLogger("thumbrpc")

func SendString(w io.Writer, str string) error {
	b := []byte{0, 0, 0, 0}
	binary.LittleEndian.PutUint32(b, uint32(len(str)))
	_, err := w.Write(b)
	if err != nil {
		return err
	}
	_, err = w.Write(([]byte)(str))
	return err
}

func ReadString(r io.Reader) (string, error) {
	b := []byte{0, 0, 0, 0}
	_, err := io.ReadFull(r, b)
	if err != nil {
		return "", err
	}
	out := make([]byte, binary.LittleEndian.Uint32(b))
	re, err := io.ReadFull(r, out)
	return string(out[:re]), err
}

//Inliner represents a instance of a Inliner
type Inliner struct {
	in chan msg
}

type Outcome struct {
	Msg    string
	Errors string
}

type msg struct {
	in   string
	done chan Outcome
}

func (i *Inliner) Inline(msgs string) (chan Outcome, error) {
	msg := msg{
		in:   msgs,
		done: make(chan Outcome),
	}
	select {
	case i.in <- msg:
		return msg.done, nil
	default:
		return nil, errors.New("Inliner queue filled.")
	}
}

func (i *Inliner) InlineBlock(msgs string) chan Outcome {
	msg := msg{
		in:   msgs,
		done: make(chan Outcome),
	}
	i.in <- msg
	return msg.done
}

func MakeInliner() (*Inliner, error) {
	i := &Inliner{
		in: make(chan msg, 8),
	}
	exited := make(chan error)
	var out io.Reader
	var in io.Writer
	spawn := func() error {
		thumblink := "thumblink"
		path, err := osext.ExecutableFolder()
		if err == nil {
			tp := filepath.Join(path, "thumblink")
			if _, err := os.Stat(tp); err == nil {
				thumblink = tp
			}
		}
		cmd := exec.Command(thumblink, "--config", conf.Path(""), "--rpc")
		errp, err := cmd.StderrPipe()
		if err != nil {
			return err
		}
		in, err = cmd.StdinPipe()
		if err != nil {
			return err
		}
		out, err = cmd.StdoutPipe()
		if err != nil {
			return err
		}
		go io.Copy(os.Stdout, errp)
		err = cmd.Start()
		if err != nil {
			return err
		}
		go func() {
			exited <- cmd.Wait()
		}()
		return nil
	}
	err := spawn()
	if err != nil {
		return nil, err
	}
	go func() {
		failed := false
		for {
			msg := <-i.in
			if !failed {
				select {
				case err := <-exited:
					log.Infof("Inliner exited: %v", err)
					failed = true
				default:
				}
			}
			if failed {
				err = spawn()
				failed = false
				if err != nil {
					failed = true
					errstr := fmt.Sprintf("Failed to restart inliner: %v", err)
					log.Error(errstr)
					msg.done <- Outcome{
						Msg:    msg.in,
						Errors: errstr,
					}
					continue
				}
			}
			herr := func(error) {
				c := time.NewTimer(time.Millisecond * 250)
				select {
				case err = <-exited:
					c.Stop()
					ns := spawn()
					failed = false
					if ns != nil {
						errstr := fmt.Sprintf("Failed to restart inliner: %v", ns)
						log.Error(errstr)
						failed = true
					}
				case <-c.C:
				}
				msg.done <- Outcome{
					Msg:    msg.in,
					Errors: err.Error(),
				}
			}
			err := SendString(in, msg.in)
			if err != nil {
				herr(err)
				continue
			}
			nmsg, err := ReadString(out)
			if err != nil {
				herr(err)
				continue
			}
			nerr, err := ReadString(out)
			if err != nil {
				herr(err)
				continue
			}
			msg.done <- Outcome{
				Msg:    nmsg,
				Errors: nerr,
			}
		}
	}()

	return i, nil
}
