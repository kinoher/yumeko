// Copyright 2017 Yumeko Contributors (as named in CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the LICENSE file

package thumblink

import (
	"strings"
	"testing"
)

func TestFindLinks_NoLinks(t *testing.T) {
	str := `<br /> &nbsp; hello `
	v, err := FindLinks(str)
	if err != nil {
		t.Fatal(err)
	}
	if len(v) != 2 {
		t.Fatal(v)
	}
	if strings.Join(v[0].HTML(), "") != "<br />" && strings.Join(v[1].HTML(), "") != " &nbsp; hello " {
		t.Fatal(v)
	}
}

func TestFindLinks_OnlyLink(t *testing.T) {
	str := `<a href="url">url</a>`
	v, err := FindLinks(str)
	if err != nil {
		t.Fatal(err)
	}
	if len(v) != 1 {
		t.Fatal(v)
	}
	if strings.Join(v[0].HTML(), "") != str {
		t.Fatal(v)
	}
}

func TestFindLinks_OnlyLinks_2(t *testing.T) {
	str := `<a href="url">url</a><a href="url">url</a>`
	v, err := FindLinks(str)
	if err != nil {
		t.Fatal(err)
	}
	if len(v) != 2 {
		t.Fatal(v)
	}
	if v[0].(Link) != "url" || v[1].(Link) != "url" {
		t.Fatal(v)
	}
}

func TestFindLinks_Mixed_3T_2L(t *testing.T) {
	str := `text<a href="url">url</a>text<a href="url">url</a>text`
	v, err := FindLinks(str)
	if err != nil {
		t.Fatal(err)
	}
	if len(v) != 5 {
		t.Fatal(v)
	}
	if v[1].(Link) != "url" || v[3].(Link) != "url" || strings.Join(v[0].HTML(), "") != "text" || strings.Join(v[2].HTML(), "") != "text" || strings.Join(v[4].HTML(), "") != "text" {
		t.Fatal(v)
	}
}

func TestFindLinks_Mixed_2T_3L(t *testing.T) {
	str := `<a href="url">url</a>text<a href="url">url</a>text<a href="url">url</a>`
	v, err := FindLinks(str)
	if err != nil {
		t.Fatal(err)
	}
	if len(v) != 5 {
		t.Fatal(v)
	}
	if v[0].(Link) != "url" || v[2].(Link) != "url" || v[4].(Link) != "url" || strings.Join(v[1].HTML(), "") != "text" || strings.Join(v[3].HTML(), "") != "text" {
		t.Fatal(v)
	}
}
