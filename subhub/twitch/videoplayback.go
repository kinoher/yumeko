// Copyright 2017 Yumeko Contributors (as named in CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the LICENSE file

package twitch

import (
	"encoding/json"
	"fmt"
)

const videoPlayback = "video-playback."

type vpAPI struct {
	Type    string `json:"type"`
	Viewers int    `json:"viewers"`
}

type PlaybackHandler interface {
	Up()
	Down()
	ViewCount(viewers int)
	Commercial()
	PropertyUpdate()
	Error(error)
}

//ListenPlayback listens for Video playback events if will call cb with(channel, viewers, is-streaming, error). Viewers is 0 for the first is-streaming=true call
func (c *Connection) ListenPlayback(channel string, cb PlaybackHandler) {
	c.Listen <- Listen{
		Topic: videoPlayback + channel,
		Update: func(_, Message string, err error) {
			if err != nil {
				cb.Error(err)
				return
			}
			ap := vpAPI{}
			err = json.Unmarshal(([]byte)(Message), &ap)
			if err != nil {
				cb.Error(err)
				return
			}
			switch ap.Type {
			case "stream-up":
				cb.Up()
			case "stream-down":
				cb.Down()
			case "viewcount":
				cb.ViewCount(ap.Viewers)
			case "commercial":
				cb.Commercial()
			case "property-update":
				cb.PropertyUpdate()
			default:
				cb.Error(fmt.Errorf("ListenPlayback: Unknown type: %v", ap.Type))
			}
		},
	}
}

//CancelListenPlayback calls
func (c *Connection) CancelListenPlayback(channel string) {
	c.Listen <- Listen{
		Topic: videoPlayback + channel,
	}
}

type PlaybackHandlerClosures struct {
	UpHandler             func()
	DownHandler           func()
	ViewCountHandler      func(int)
	CommercialHandler     func()
	PropertyUpdateHandler func()
	ErrorHandler          func(error)
}

func (p PlaybackHandlerClosures) Up() {
	if p.UpHandler != nil {
		p.UpHandler()
	}
}
func (p PlaybackHandlerClosures) Down() {
	if p.DownHandler != nil {
		p.DownHandler()
	}
}
func (p PlaybackHandlerClosures) ViewCount(viewers int) {
	if p.ViewCountHandler != nil {
		p.ViewCountHandler(viewers)
	}
}
func (p PlaybackHandlerClosures) Commercial() {
	if p.CommercialHandler != nil {
		p.CommercialHandler()
	}
}
func (p PlaybackHandlerClosures) PropertyUpdate() {
	if p.PropertyUpdateHandler != nil {
		p.PropertyUpdateHandler()
	}
}
func (p PlaybackHandlerClosures) Error(err error) {
	if p.ErrorHandler != nil {
		p.ErrorHandler(err)
	}
}
