// Copyright 2017 Yumeko Contributors (as named in CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the LICENSE file

package twitch

import (
	"encoding/json"
	"net/http"

	"bitbucket.org/abex/yumeko/util"
	"bitbucket.org/abex/yumeko/versioning"
)

type ClipUser struct {
	ID          string `json:"id"`
	Name        string `json:"name"`
	DisplayName string `json:"display_name"`
	ChannelURL  string `json:"channel_url"`
	Logo        string `json:"logo"`
}
type Clip struct {
	Broadcaster ClipUser `json:"broadcaster"`
	Curator     ClipUser `json:"curator"`
	Game        string   `json:"game"`
	Title       string   `json:"title"`
	Duration    float64  `json:"duration"`
	Thumbnails  struct {
		Medium string `json:"medium"`
		Small  string `json:"small"`
		Tiny   string `json:"tiny"`
	} `json:""`
}

func GetClip(key, slug string) (Clip, error) {
	c := Clip{}
	req, err := http.NewRequest("GET", "https://api.twitch.tv/kraken/clips/"+slug, nil)
	if err != nil {
		return c, err
	}
	versioning.SetUserAgent(req)
	req.Header.Add("Accept", "application/vnd.twitchtv.v5+json")
	req.Header.Add("Client-ID", key)
	res, err := util.Client.Do(req)
	if err != nil {
		return c, err
	}
	defer res.Body.Close()
	err = json.NewDecoder(res.Body).Decode(&c)
	return c, err
}
