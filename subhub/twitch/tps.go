// Copyright 2017 Yumeko Contributors (as named in CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the LICENSE file

package twitch

import (
	"errors"
	"time"

	"bitbucket.org/abex/yumeko/glug"
	"bitbucket.org/abex/yumeko/util"
	"github.com/gorilla/websocket"
)

//Listen causes a LISTEN or UNLISTEN to be sent to TPS
//If Updates is nil UNLISTEN is send, otherwise LISTEN is sent
type Listen struct {
	Topic  string
	Update func(Topic, Message string, err error)
}

//Connection represents a connection to TPS
type Connection struct {
	Listen    chan Listen
	listens   map[string]Listen
	authToken string
}

type apiTX struct {
	Type  string `json:"type",omitempty`
	Nonce string `json:"nonce",omitempty`
	Data  dataTX `json:"data",omitempty`
}
type dataTX struct {
	Topics    []string `json:"topics",omitempty`
	AuthToken string   `json:"auth_token,omitempty"`
}
type apiRX struct {
	Type  string `json:"type",omitempty`
	Error string `json:"error",omitempty`
	Nonce string `json:"nonce",omitempty`
	Data  struct {
		Topic   string `json:"topic",omitempty`
		Message string `json:"message",omitempty`
	} `json:"data",omitempty`
}
type apiStreamRX struct {
	Type string `json:"type",omitempty`
}

const tpsEndpoint = "wss://pubsub-edge.twitch.tv"

var log = glug.NewLogger("subhub/twitch")

//MakeConnection connects to TPS ready for LISTENs
func MakeConnection(AuthToken string) (*Connection, error) {
	c := &Connection{
		Listen:    make(chan Listen, 1),
		listens:   map[string]Listen{},
		authToken: AuthToken,
	}
	err := c.connect()
	if err != nil {
		return nil, err
	}
	return c, nil
}

//Len returns the number of listens on this connection
func (c *Connection) Len() int {
	return len(c.listens)
}

func (c *Connection) reconnect() {
	for len(c.listens) == 0 { //Dont reconnect until we have something to do
		update := <-c.Listen
		if update.Update != nil {
			c.listens[update.Topic] = update
		}
	}
	holdoff := time.Second * 3
	for {
		time.Sleep(util.Jitter(holdoff))
		err := c.connect()
		if err != nil {
			holdoff *= 2
			log.Infof("Error connecting to TPS (will retry in %v): %v", holdoff, err)
			continue
		}
		break
	}
}

func (c *Connection) connect() error {
	conn, _, err := (&websocket.Dialer{}).Dial(tpsEndpoint, nil)
	if err != nil {
		return err
	}
	rx := make(chan apiRX, 1)
	go func() {
		defer func() {
			recover()
		}()
		for {
			val := apiRX{}
			err := conn.ReadJSON(&val)
			if err != nil {
				log.Infof("Error reading TPS: %v", err)
				close(rx)
				return
			}
			rx <- val
		}
	}()
	for k, v := range c.listens {
		tx := apiTX{
			Type:  "LISTEN",
			Nonce: k,
			Data: dataTX{
				AuthToken: c.authToken,
				Topics:    []string{v.Topic},
			},
		}
		log.Trace("Sending", tx)
		err := conn.WriteJSON(tx)
		if err != nil {
			log.Infof("Error writing initial (%v): %v", v.Topic, err)
		}
	}
	go func() {
		kill := time.NewTimer(time.Hour)
		defer kill.Stop()
		kill.Stop()
		pingTicker := time.NewTicker(time.Second * 270)
		defer pingTicker.Stop()
		for {
			select {
			case msg, ok := <-rx:
				if !ok {
					c.reconnect()
					return
				}
				log.Trace("Recieved", msg)
				switch msg.Type {
				case "PONG":
					if kill != nil {
						if !kill.Stop() {
							<-kill.C
						}
					}
				case "RECONNECT":
					conn.Close()
					c.reconnect()
					return
				case "MESSAGE":
					l, ok := c.listens[msg.Data.Topic]
					if !ok {
						log.Weirdf("Recieved message for unknown topic: '%v'", msg.Data.Topic)
						continue
					}
					l.Update(msg.Data.Topic, msg.Data.Message, nil)
				case "RESPONSE":
					if msg.Error != "" {
						l, ok := c.listens[msg.Nonce]
						if !ok {
							log.Weirdf("Recieved responce for unknown topic: '%v'", msg.Nonce)
							continue
						}
						l.Update("", "", errors.New(msg.Error))
					}
				default:
					log.Infof("Unknown type from TPS '%v'", msg.Type)
				}
			case <-kill.C:
				conn.Close()
				c.reconnect()
				return
			case <-pingTicker.C:
				err := conn.WriteJSON(apiTX{
					Type: "PING",
				})
				log.Trace("Sent PING")
				if err != nil {
					log.Infof("Error writing: %v", err)
					conn.Close()
					c.reconnect()
					return
				}
				kill.Reset(time.Second * 10)
			case msg, ok := <-c.Listen:
				if !ok {
					conn.Close()
					log.Debug("Exiting TPS loop due to closed channel")
					return
				}
				if msg.Update == nil {
					key := ""
					for k, v := range c.listens {
						if v.Topic == msg.Topic {
							key = k
						}
					}
					if key == "" {
						log.Weirdf("Tried to unregister unknown '%v'", msg.Topic)
						continue
					}
					delete(c.listens, key)
					tx := apiTX{
						Type:  "UNLISTEN",
						Nonce: msg.Topic,
						Data: dataTX{
							AuthToken: c.authToken,
							Topics:    []string{msg.Topic},
						},
					}
					log.Trace("Sending", tx)
					err := conn.WriteJSON(tx)
					if err != nil {
						log.Infof("Error writing: %v", err)
						conn.Close()
						c.reconnect()
						return
					}
				} else {
					c.listens[msg.Topic] = msg
					tx := apiTX{
						Type:  "LISTEN",
						Nonce: msg.Topic,
						Data: dataTX{
							AuthToken: c.authToken,
							Topics:    []string{msg.Topic},
						},
					}
					log.Trace("Sending", tx)
					err := conn.WriteJSON(tx)
					if err != nil {
						log.Infof("Error writing: %v", err)
						conn.Close()
						c.reconnect()
						return
					}
				}
			}
		}
	}()
	return nil
}
