// Copyright 2017 Yumeko Contributors (as named in CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the LICENSE file

package twitch

import (
	"encoding/json"
	"net/http"

	"bitbucket.org/abex/yumeko/util"
	"bitbucket.org/abex/yumeko/versioning"
)

type Channel struct {
	Status      string `json:"status"`
	DisplayName string `json:"display_name"`
	Game        string `json:"game"`
	Logo        string `json:"logo"`
	URL         string `json:"url"`
}

func GetChannel(key, name string) (Channel, error) {
	c := Channel{}
	req, err := http.NewRequest("GET", "https://api.twitch.tv/kraken/channels/"+name, nil)
	if err != nil {
		return c, err
	}
	versioning.SetUserAgent(req)
	req.Header.Add("Accept", "application/vnd.twitchtv.v3+json")
	req.Header.Add("Client-ID", key)
	res, err := util.Client.Do(req)
	if err != nil {
		return c, err
	}
	defer res.Body.Close()
	err = json.NewDecoder(res.Body).Decode(&c)
	return c, err
}
