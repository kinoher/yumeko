// Copyright 2017 Yumeko Contributors (as named in CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the LICENSE file

package opus

/*#include "opus.h"
void gopEReset(OpusEncoder* e){
	opus_encoder_ctl(e,OPUS_RESET_STATE);
}
opus_uint32 gopEFinalRange(OpusEncoder* e){
	opus_uint32 a;
	opus_encoder_ctl(e,OPUS_GET_FINAL_RANGE(&a));
	return a;
}
opus_int32 gopEBandwidth(OpusEncoder* e){
	opus_int32 a;
	opus_encoder_ctl(e,OPUS_GET_BANDWIDTH(&a));
	return a;
}
void gopESetApplication(OpusEncoder* e,opus_int32 v){
	opus_encoder_ctl(e,OPUS_SET_APPLICATION(v));
}
void gopESetBandwidth(OpusEncoder* e,opus_int32 v){
	opus_encoder_ctl(e,OPUS_SET_BANDWIDTH(v));
}
void gopESetBitrate(OpusEncoder* e,opus_int32 v){
	opus_encoder_ctl(e,OPUS_SET_BITRATE(v));
}
void gopESetComplexity(OpusEncoder* e,opus_int32 v){
	opus_encoder_ctl(e,OPUS_SET_COMPLEXITY(v));
}
void gopESetDTX(OpusEncoder* e, opus_int32 v){
	opus_encoder_ctl(e,OPUS_SET_DTX(v));
}
void gopEForceSetChannels(OpusEncoder* e,opus_int32 v){
	opus_encoder_ctl(e,OPUS_SET_FORCE_CHANNELS(v));
}
void gopESetInbandFEC(OpusEncoder* e,opus_int32 v){
	opus_encoder_ctl(e,OPUS_SET_INBAND_FEC(v));
}
void gopESetLSBDepth(OpusEncoder* e, opus_int32 v){
	opus_encoder_ctl(e,OPUS_SET_LSB_DEPTH(v));
}
void gopESetMaxBandwidth(OpusEncoder* e,opus_int32 v){
	opus_encoder_ctl(e,OPUS_SET_MAX_BANDWIDTH(v));
}
void gopESetPacketLossPerc(OpusEncoder* e,opus_int32 v){
	opus_encoder_ctl(e,OPUS_SET_PACKET_LOSS_PERC(v));
}
void gopESetSignal(OpusEncoder* e,opus_int32 v){
	opus_encoder_ctl(e,OPUS_SET_SIGNAL(v));
}
void gopESetVBR(OpusEncoder* e,opus_int32 v){
	opus_encoder_ctl(e,OPUS_SET_VBR(v));
}
void gopESetVBRConstraint(OpusEncoder* e,opus_int32 v){
	opus_encoder_ctl(e,OPUS_SET_VBR_CONSTRAINT(v));
}
opus_int32 gopEApplication(OpusEncoder* e){
	opus_int32 a;
	opus_encoder_ctl(e,OPUS_GET_APPLICATION(&a));
	return a;
}
opus_int32 gopEBitrate(OpusEncoder* e){
	opus_int32 a;
	opus_encoder_ctl(e,OPUS_GET_BITRATE(&a));
	return a;
}
opus_int32 gopEComplexity(OpusEncoder* e){
	opus_int32 a;
	opus_encoder_ctl(e,OPUS_GET_COMPLEXITY(&a));
	return a;
}
opus_int32 gopEDTX(OpusEncoder* e){
	opus_int32 a;
	opus_encoder_ctl(e,OPUS_GET_DTX(&a));
	return a;
}
opus_int32 gopEForceChannels(OpusEncoder* e){
	opus_int32 a;
	opus_encoder_ctl(e,OPUS_GET_FORCE_CHANNELS(&a));
	return a;
}
opus_int32 gopEInbandFEC(OpusEncoder* e){
	opus_int32 a;
	opus_encoder_ctl(e,OPUS_GET_INBAND_FEC(&a));
	return a;
}
opus_int32 gopELastPacketDuration(OpusEncoder* e){
	opus_int32 a;
	opus_encoder_ctl(e,OPUS_GET_LAST_PACKET_DURATION(&a));
	return a;
}
opus_int32 gopELookahead(OpusEncoder* e){
	opus_int32 a;
	opus_encoder_ctl(e,OPUS_GET_LOOKAHEAD(&a));
	return a;
}
opus_int32 gopELSBDepth(OpusEncoder* e){
	opus_int32 a;
	opus_encoder_ctl(e,OPUS_GET_LSB_DEPTH(&a));
	return a;
}
opus_int32 gopEMaxBandwidth(OpusEncoder* e){
	opus_int32 a;
	opus_encoder_ctl(e,OPUS_GET_MAX_BANDWIDTH(&a));
	return a;
}
opus_int32 gopEPacketLossPerc(OpusEncoder* e){
	opus_int32 a;
	opus_encoder_ctl(e,OPUS_GET_PACKET_LOSS_PERC(&a));
	return a;
}
opus_int32 gopESampleRate(OpusEncoder* e){
	opus_int32 a;
	opus_encoder_ctl(e,OPUS_GET_SAMPLE_RATE(&a));
	return a;
}
opus_int32 gopESignal(OpusEncoder* e){
	opus_int32 a;
	opus_encoder_ctl(e,OPUS_GET_SIGNAL(&a));
	return a;
}
opus_int32 gopEVBR(OpusEncoder* e){
	opus_int32 a;
	opus_encoder_ctl(e,OPUS_GET_VBR(&a));
	return a;
}
opus_int32 gopEVBRConstraint(OpusEncoder* e){
	opus_int32 a;
	opus_encoder_ctl(e,OPUS_GET_VBR_CONSTRAINT(&a));
	return a;
}*/
import "C"
import "unsafe"

//Encoder is a opus encoder
type Encoder C.struct_OpusEncoder

//GetEncoderSize returns the size of a Encoder
func GetEncoderSize(channels int) int {
	return int(C.opus_encoder_get_size(C.int(channels)))
}

//NewEncoder Allocates and initializes an encoder state.
func NewEncoder(sampleRate int, channels int, application Application) (*Encoder, error) {
	enc := (*Encoder)((unsafe.Pointer)(&make([]byte, GetEncoderSize(channels))[0]))
	e := C.opus_encoder_init((*C.struct_OpusEncoder)(enc), C.opus_int32(sampleRate), C.int(channels), C.int(application))
	return enc, getError(e)
}

//Encodes a opus frame from pcm input
func (e *Encoder) EncodeInt16(pcm []int16, frameSize int, maxData int) ([]byte, error) {
	data := make([]byte, maxData)
	ret := C.opus_encode((*C.struct_OpusEncoder)(e), (*C.opus_int16)(&pcm[0]), C.int(frameSize), (*C.uchar)(&data[0]), C.opus_int32(maxData))
	var err error
	if ret < 0 {
		err = getError(C.int(ret))
		data = nil
	} else {
		data = data[:ret]
	}
	return data, err
}

//Encodes an Opus frame from floating point input.
func (e *Encoder) EncodeFloat32(pcm []float32, frameSize int, maxData int) ([]byte, error) {
	data := make([]byte, maxData)
	ret := C.opus_encode_float((*C.struct_OpusEncoder)(e), (*C.float)(&pcm[0]), C.int(frameSize), (*C.uchar)(&data[0]), C.opus_int32(maxData))
	var err error
	if ret < 0 {
		err = getError(C.int(ret))
		data = nil
	} else {
		data = data[:ret]
	}
	return data, err
}

//Resets the codec state to be equivalent to a freshly initialized state.
//This should be called when switching streams in order to prevent the back to back decoding from giving different results from one at a time decoding.
func (e *Encoder) Reset() {
	C.gopEReset((*C.struct_OpusEncoder)(e))
}

//Gets the final state of the codec's entropy coder.
func (e *Encoder) GetFinalRange() uint32 {
	return uint32(C.gopEFinalRange((*C.struct_OpusEncoder)(e)))
}

//Gets the encoder's configured bandpass or the decoder's last bandpass.
func (e *Encoder) Bandwidth() Bandwidth {
	return Bandwidth(C.gopEBandwidth((*C.struct_OpusEncoder)(e)))
}

//Configures the encoder's intended application.
//The initial value is a mandatory argument to the encoder_create function.
func (e *Encoder) SetApplication(v Application) {
	C.gopESetApplication((*C.struct_OpusEncoder)(e), C.opus_int32(v))
}

//Sets the encoder's bandpass to a specific value.
//This prevents the encoder from automatically selecting the bandpass based on the available bitrate. If an application knows the bandpass of the input audio it is providing, it should normally use OPUS_SET_MAX_BANDWIDTH instead, which still gives the encoder the freedom to reduce the bandpass when the bitrate becomes too low, for better overall quality.
func (e *Encoder) SetBandwidth(v Bandwidth) {
	C.gopESetBandwidth((*C.struct_OpusEncoder)(e), C.opus_int32(v))
}

//Configures the bitrate in the encoder.
//Rates from 500 to 512000 bits per second are meaningful, as well as the special values OPUS_AUTO and OPUS_BITRATE_MAX. The value OPUS_BITRATE_MAX can be used to cause the codec to use as much rate as it can, which is useful for controlling the rate by adjusting the output buffer size.
func (e *Encoder) SetBitrate(v int) {
	C.gopESetBitrate((*C.struct_OpusEncoder)(e), C.opus_int32(v))
}

//Configures the encoder's computational complexity.
//The supported range is 0-10 inclusive with 10 representing the highest complexity.
func (e *Encoder) SetComplexity(v int) {
	C.gopESetComplexity((*C.struct_OpusEncoder)(e), C.opus_int32(v))
}

//Configures the encoder's use of discontinuous transmission (DTX).
func (e *Encoder) SetDTX(vb bool) {
	v := 0
	if vb {
		v = 1
	}
	C.gopESetDTX((*C.struct_OpusEncoder)(e), C.opus_int32(v))
}

//Configures mono/stereo forcing in the encoder.
//This can force the encoder to produce packets encoded as either mono or stereo, regardless of the format of the input audio. This is useful when the caller knows that the input signal is currently a mono source embedded in a stereo stream.
//Auto is applicable
func (e *Encoder) SetForceChannels(v int) {
	C.gopEForceSetChannels((*C.struct_OpusEncoder)(e), C.opus_int32(v))
}

//Configures the encoder's use of inband forward error correction (FEC).
//Note: This is only applicable to the LPC layer
func (e *Encoder) SetInbandFEC(vb bool) {
	v := 0
	if vb {
		v = 1
	}
	C.gopESetInbandFEC((*C.struct_OpusEncoder)(e), C.opus_int32(v))
}

//Configures the depth of signal being encoded.
//This is a hint which helps the encoder identify silence and near-silence.
func (e *Encoder) SetLSBDepth(v int) {
	C.gopESetLSBDepth((*C.struct_OpusEncoder)(e), C.opus_int32(v))
}

//Configures the maximum bandpass that the encoder will select automatically.
//Applications should normally use this instead of OPUS_SET_BANDWIDTH (leaving that set to the default, OPUS_AUTO). This allows the application to set an upper bound based on the type of input it is providing, but still gives the encoder the freedom to reduce the bandpass when the bitrate becomes too low, for better overall quality.
func (e *Encoder) SetMaxBandwidth(v Bandwidth) {
	C.gopESetMaxBandwidth((*C.struct_OpusEncoder)(e), C.opus_int32(v))
}

//Configures the encoder's expected packet loss percentage.
//Higher values with trigger progressively more loss resistant behavior in the encoder at the expense of quality at a given bitrate in the lossless case, but greater quality under loss.
func (e *Encoder) SetPacketLossPerc(v int) {
	C.gopESetPacketLossPerc((*C.struct_OpusEncoder)(e), C.opus_int32(v))
}

//Configures the type of signal being encoded.
//This is a hint which helps the encoder's mode selection.
func (e *Encoder) SetSignal(v Signal) {
	C.gopESetSignal((*C.struct_OpusEncoder)(e), C.opus_int32(v))
}

//Enables or disables variable bitrate (VBR) in the encoder.
//The configured bitrate may not be met exactly because frames must be an integer number of bytes in length.
//Warning: Only the MDCT mode of Opus can provide hard CBR behavior.
func (e *Encoder) SetVBR(v int) {
	C.gopESetVBR((*C.struct_OpusEncoder)(e), C.opus_int32(v))
}

//Enables or disables constrained VBR in the encoder.
//This setting is ignored when the encoder is in CBR mode.
//Warning: Only the MDCT mode of Opus currently heeds the constraint. Speech mode ignores it completely, hybrid mode may fail to obey it if the LPC layer uses more bitrate than the constraint would have permitted.
func (e *Encoder) SetVBRConstraint(v int) {
	C.gopESetVBRConstraint((*C.struct_OpusEncoder)(e), C.opus_int32(v))
}

//Gets the encoder's configured application.
func (e *Encoder) Application() Application {
	return Application(C.gopEApplication((*C.struct_OpusEncoder)(e)))
}

//Gets the encoder's bitrate configuration.
func (e *Encoder) Bitrate() int {
	return int(C.gopEBitrate((*C.struct_OpusEncoder)(e)))
}

//Gets the encoder's complexity configuration.
func (e *Encoder) Complexity() int {
	return int(C.gopEComplexity((*C.struct_OpusEncoder)(e)))
}

//Gets encoder's configured use of discontinuous transmission.
func (e *Encoder) DTX() bool {
	return C.gopEDTX((*C.struct_OpusEncoder)(e)) == 1
}

//Gets the encoder's forced channel configuration.
func (e *Encoder) ForceChannels() int {
	return int(C.gopEForceChannels((*C.struct_OpusEncoder)(e)))
}

//Gets encoder's configured use of inband forward error correction.
func (e *Encoder) InbandFEC() bool {
	return C.gopEInbandFEC((*C.struct_OpusEncoder)(e)) == 1
}

//Gets the duration (in samples) of the last packet successfully decoded or concealed.
func (e *Encoder) LastPacketDuration() int {
	return int(C.gopELastPacketDuration((*C.struct_OpusEncoder)(e)))
}

//Gets the total samples of delay added by the entire codec.
//This can be queried by the encoder and then the provided number of samples can be skipped on from the start of the decoder's output to provide time aligned input and output. From the perspective of a decoding application the real data begins this many samples late.
//The decoder contribution to this delay is identical for all decoders, but the encoder portion of the delay may vary from implementation to implementation, version to version, or even depend on the encoder's initial configuration. Applications needing delay compensation should call this CTL rather than hard-coding a value.
func (e *Encoder) Lookahead() int {
	return int(C.gopELookahead((*C.struct_OpusEncoder)(e)))
}

//Gets the encoder's configured signal depth.
func (e *Encoder) LSBDepth() int {
	return int(C.gopELSBDepth((*C.struct_OpusEncoder)(e)))
}

//Gets the encoder's configured maximum allowed bandpass.
func (e *Encoder) MaxBandwidth() Bandwidth {
	return Bandwidth(C.gopEMaxBandwidth((*C.struct_OpusEncoder)(e)))
}

//Gets the encoder's configured packet loss percentage.
func (e *Encoder) PacketLossPerc() int {
	return int(C.gopEPacketLossPerc((*C.struct_OpusEncoder)(e)))
}

//Gets the sampling rate the encoder or decoder was initialized with.
//This simply returns the frameSize value passed to NewEncoder()
func (e *Encoder) SampleRate() int {
	return int(C.gopESampleRate((*C.struct_OpusEncoder)(e)))
}

//Gets the encoder's configured signal type.
func (e *Encoder) Signal() Signal {
	return Signal(C.gopESignal((*C.struct_OpusEncoder)(e)))
}

//Determine if variable bitrate (VBR) is enabled in the encoder.
func (e *Encoder) VBR() int {
	return int(C.gopEVBR((*C.struct_OpusEncoder)(e)))
}

//Configures the encoder's intended application.
//The initial value is a mandatory argument to the encoder_create function.
func (e *Encoder) VBRConstraint() int {
	return int(C.gopEVBRConstraint((*C.struct_OpusEncoder)(e)))
}
