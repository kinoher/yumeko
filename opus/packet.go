// Copyright 2017 Yumeko Contributors (as named in CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the LICENSE file

package opus

//#include "opus.h"
import "C"
import "errors"

//Gets the bandwidth of an Opus packet.
func PacketGetBandwidth(data []byte) (Bandwidth, error) {
	ret := C.opus_packet_get_bandwidth((*C.uchar)(&data[0]))
	if ret == C.OPUS_INVALID_PACKET {
		return Bandwidth(0), errors.New("OPUS_INVALID_PACKET: The compressed data passed is corrupted or of an unsupported type.")
	}
	return Bandwidth(ret), nil
}

//Gets the number of channels from a Opus packet.
func PacketGetNbChannels(data []byte) (int, error) {
	ret := C.opus_packet_get_nb_channels((*C.uchar)(&data[0]))
	if ret == C.OPUS_INVALID_PACKET {
		return 0, errors.New("OPUS_INVALID_PACKET: The compressed data passed is corrupted or of an unsupported type.")
	}
	return int(ret), nil
}

//Gets the number of frames in an Opus packet.
func PacketGetNbFrames(data []byte) (int, error) {
	ret := C.opus_packet_get_nb_frames((*C.uchar)(&data[0]), C.opus_int32(len(data)))
	if ret == C.OPUS_BAD_ARG {
		return 0, errors.New("OPUS_BAD_ARG: Insufficient data was passed to the function.")
	}
	if ret == C.OPUS_INVALID_PACKET {
		return 0, errors.New("OPUS_INVALID_PACKET: The compressed data passed is corrupted or of an unsupported type.")
	}
	return int(ret), nil
}

//Gets the number of samples of an Opus packet
func PacketGetNbSamples(data []byte, sampleRate int) (int, error) {
	ret := C.opus_packet_get_nb_samples((*C.uchar)(&data[0]), C.opus_int32(len(data)), C.opus_int32(sampleRate))
	if ret == C.OPUS_BAD_ARG {
		return 0, errors.New("OPUS_BAD_ARG: Insufficient data was passed to the function.")
	}
	if ret == C.OPUS_INVALID_PACKET {
		return 0, errors.New("OPUS_INVALID_PACKET: The compressed data passed is corrupted or of an unsupported type.")
	}
	return int(ret), nil
}

//Gets the number of samples per frame from an Opus packet
func PacketGetSamplesPerFrame(data []byte, sampleRate int) int {
	return int(C.opus_packet_get_samples_per_frame((*C.uchar)(&data[0]), C.opus_int32(sampleRate)))
}
