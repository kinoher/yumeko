// Copyright 2017 Yumeko Contributors (as named in CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the LICENSE file

package opus

//#include "opus.h"
import "C"
import (
	"errors"
	"unsafe"
)

//The repacketizer can be used to merge multiple Opus packets into a single packet or alternatively to split Opus packets that have previously been merged.
type Repacketizer C.struct_OpusRepacketizer

//Gets the size of an OpusRepacketizer structure.
func GetRepacketizerSize() int {
	return int(C.opus_repacketizer_get_size())
}

//Allocates memory and initializes the new repacketizer with opus_repacketizer_init().
func NewRepacketizer() *Repacketizer {
	en := (*Repacketizer)((unsafe.Pointer)(&make([]byte, GetRepacketizerSize())[0]))
	C.opus_repacketizer_init((*C.struct_OpusRepacketizer)(en))
	return en
}

//Add a packet to the current repacketizer state.
//This packet must match the configuration of any packets already submitted for repacketization since the last call to opus_repacketizer_init(). This means that it must have the same coding mode, audio bandwidth, frame size, and channel count. This can be checked in advance by examining the top 6 bits of the first byte of the packet, and ensuring they match the top 6 bits of the first byte of any previously submitted packet. The total duration of audio in the repacketizer state also must not exceed 120 ms, the maximum duration of a single packet, after adding this packet.
//The contents of the current repacketizer state can be extracted into new packets using opus_repacketizer_out() or opus_repacketizer_out_range().
//In order to add a packet with a different configuration or to add more audio beyond 120 ms, you must clear the repacketizer state by calling opus_repacketizer_init(). If a packet is too large to add to the current repacketizer state, no part of it is added, even if it contains multiple frames, some of which might fit. If you wish to be able to add parts of such packets, you should first use another repacketizer to split the packet into pieces and add them individually.
func (e *Repacketizer) Cat(data []byte) error {
	er := C.opus_repacketizer_cat((*C.struct_OpusRepacketizer)(e), (*C.uchar)(&data[0]), C.opus_int32(len(data)))
	if er == C.OPUS_OK {
		return nil
	}
	return errors.New("OPUS_INVALID_PACKET: The packet did not have a valid TOC sequence, the packet's TOC sequence was not compatible with previously submitted packets (because the coding mode, audio bandwidth, frame size, or channel count did not match), or adding this packet would increase the total amount of audio stored in the repacketizer state to more than 120 ms.")
}

//Return the total number of frames contained in packet data submitted to the repacketizer state so far via opus_repacketizer_cat() since the last call to opus_repacketizer_init() or opus_repacketizer_create().
//This defines the valid range of packets that can be extracted with opus_repacketizer_out_range() or opus_repacketizer_out().
func (e *Repacketizer) GetNBFrames() int {
	return int(C.opus_repacketizer_get_nb_frames((*C.struct_OpusRepacketizer)(e)))
}

//Construct a new packet from data previously submitted to the repacketizer state via opus_repacketizer_cat().
//This is a convenience routine that returns all the data submitted so far in a single packet. It is equivalent to calling
// OutRange(0,GetNBFrames(),maxlen)
func (e *Repacketizer) Out(maxlen int) ([]byte, error) {
	data := make([]byte, maxlen)
	ret := C.opus_repacketizer_out((*C.struct_OpusRepacketizer)(e), (*C.uchar)(&data[0]), C.opus_int32(len(data)))
	if ret == C.OPUS_BUFFER_TOO_SMALL {
		return nil, errors.New("OPUS_BUFFER_TOO_SMALL:	maxlen was insufficient to contain the complete output packet.")
	}
	return data[:ret], nil
}

//Construct a new packet from data previously submitted to the repacketizer state via opus_repacketizer_cat().
func (e *Repacketizer) OutRange(begin, end, maxlen int) ([]byte, error) {
	data := make([]byte, maxlen)
	ret := C.opus_repacketizer_out_range((*C.struct_OpusRepacketizer)(e), C.int(begin), C.int(end), (*C.uchar)(&data[0]), C.opus_int32(len(data)))
	if ret == C.OPUS_BAD_ARG {
		return nil, errors.New("OPUS_BAD_ARG: [begin,end) was an invalid range of frames (begin < 0, begin >= end, or end > opus_repacketizer_get_nb_frames()).")
	}
	if ret == C.OPUS_BUFFER_TOO_SMALL {
		return nil, errors.New("OPUS_BUFFER_TOO_SMALL: maxlen was insufficient to contain the complete output packet.")
	}
	return data[:ret], nil
}
