// Copyright 2017 Yumeko Contributors (as named in CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the LICENSE file

//Opus gives bindings to libopus
//https://www.opus-codec.org/
package opus

//#include "opus.h"
import "C"
import "errors"

func getError(e C.int) error {
	if e == C.OPUS_OK {
		return nil
	} else {
		return errors.New(C.GoString(C.opus_strerror(e)))
	}
}

//GetError Converts an opus error code into a human readable string.
func GetError(error int) error {
	return getError(C.int(error))
}

//GetVersionString Gets the libopus version string.
func GetVersionString() string {
	return C.GoString(C.opus_get_version_string())
}
