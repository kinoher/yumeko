// Copyright 2017 Yumeko Contributors (as named in CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the LICENSE file

package opus

//#include "opus.h"
import "C"

//Error represents a Opus Error
type Error int

const (
	//No Error
	OK Error = C.OPUS_OK
	//One or more invalid/out of range arguments
	BadArg Error = C.OPUS_BAD_ARG
	// The mode struct passed is invalid
	BufferToSmall Error = C.OPUS_BUFFER_TOO_SMALL
	// An internal error was detected
	InternalError Error = C.OPUS_INTERNAL_ERROR
	// The compressed data passed is corrupted
	InvalidPacket Error = C.OPUS_INVALID_PACKET
	// Invalid/unsupported request number
	Unimplemented Error = C.OPUS_UNIMPLEMENTED
	// An encoder or decoder structure is invalid or already freed
	InvalidState Error = C.OPUS_INVALID_STATE
	// Memory allocation has failed
	AllocFail Error = C.OPUS_ALLOC_FAIL
)

const (
	//Auto/default setting
	Auto int = C.OPUS_AUTO
	//Maximum bitrate
	BitrateMax int = C.OPUS_BITRATE_MAX
)

type Application int

const (
	// Best for most VoIP/videoconference applications where listening quality and intelligibility matter most
	ApplicationVoip Application = C.OPUS_APPLICATION_VOIP
	// Best for broadcast/high-fidelity application where the decoded audio should be as close as possible to the input
	ApplicationAudio Application = C.OPUS_APPLICATION_AUDIO
	// Only use when lowest-achievable latency is what matters most. Voice-optimized modes cannot be used.
	ApplicationRestrictedLowdelay Application = C.OPUS_APPLICATION_RESTRICTED_LOWDELAY
)

type Signal int

const (
	//Signal being encoded is voice
	SignalVoice Signal = C.OPUS_SIGNAL_VOICE
	//Signal being encoded is music
	SignalMusic Signal = C.OPUS_SIGNAL_MUSIC
)

type Bandwidth int

const (
	BandwidthAutoband Bandwidth = C.OPUS_AUTO
	//4kHz bandpass
	BandwidthNarrowband Bandwidth = C.OPUS_BANDWIDTH_NARROWBAND
	//6kHz bandpass
	BandwidthMediumband Bandwidth = C.OPUS_BANDWIDTH_MEDIUMBAND
	//8kHz bandpass
	BandwidthWideband Bandwidth = C.OPUS_BANDWIDTH_WIDEBAND
	//12kHz bandpass
	BandwidthSuperWideband Bandwidth = C.OPUS_BANDWIDTH_SUPERWIDEBAND
	//20kHz bandpass
	BandwidthFullband Bandwidth = C.OPUS_BANDWIDTH_FULLBAND
)
