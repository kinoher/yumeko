// Copyright 2017 Yumeko Contributors (as named in CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the LICENSE file

// +build none

package main

import (
	"fmt"
	"image/png"
	"os"

	"bitbucket.org/abex/yumeko/ffmpeg"
)

func main() {
	fc, err := ffmpeg.OpenFormatFile(os.Args[1])
	defer fc.Free()
	if err != nil {
		panic(err)
	}
	fc.Dump()
	videoStream, err := fc.FindStream(ffmpeg.MediaTypeVideo)
	if err != nil {
		panic(err)
	}
	fmt.Println(videoStream)
	audioStream, _ := fc.FindStream(ffmpeg.MediaTypeAudio)
	fmt.Println(audioStream)
	if audioStream != -1 && audioStream < videoStream {
		fmt.Println("Flag Audio")
	}
	fc.SetDiscard()
	cc, err := fc.GetCodec(videoStream)
	defer cc.Free()
	if err != nil {
		panic(err)
	}
	width, height, maxLowres := cc.GetSizeInfo()
	targetWidth := 500
	targetHeight := 800
	lr := 0
	for ; lr < maxLowres; lr++ {
		div := 1 << uint(lr)
		if width/div < targetWidth || height/div < targetHeight {
			lr--
			break
		}
	}
	if lr > 0 {
		cc.SetLowres(lr)
		div := 1 << uint(lr)
		width /= div
		height /= div
	}
	err = cc.Open()
	if err != nil {
		panic(err)
	}
	fmt.Println(width, height)
	frm, err := ffmpeg.MakeFrame()
	if err != nil {
		panic(err)
	}
	defer frm.Free()
	err = cc.ReadFrame(frm)
	if err != nil {
		panic(err)
	}
	fmt.Println(frm.Size())
	goimg := frm.GoImg()
	if goimg == nil {
		nf, err := ffmpeg.MakeFrame()
		if err != nil {
			panic(err)
		}
		defer nf.Free()
		if frm.HasAlpha() {
			nf.FillRGBA(frm.Size())
		} else {
			nf.FillYCbCr420(frm.Size())
		}
		err = frm.Scale(nf, ffmpeg.SWSBilinear)
		if err != nil {
			panic(err)
		}
		frm, nf = nf, frm
		goimg = frm.GoImg()
	}
	ofi, err := os.OpenFile("out.png", os.O_CREATE|os.O_TRUNC|os.O_WRONLY, 0777)
	if err != nil {
		panic(err)
	}
	err = png.Encode(ofi, goimg)
	if err != nil {
		panic(err)
	}
}
