// Copyright 2016 Yumeko Contributors (as named in CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the LICENSE file
 
#include "libavformat/avformat.h"
#include "libavcodec/avcodec.h"
#include "libavformat/avio.h"
#include "libswscale/swscale.h"
#include "libavfilter/avfilter.h"
#include "libavutil/opt.h"
#include "libavutil/pixdesc.h"
#include "libavfilter/buffersrc.h"
#include "libavfilter/buffersink.h"
#include "goio.h"

void init(){
	av_log_set_level(AV_LOG_ERROR);
	av_register_all();//avformat
	avcodec_register_all();
	avfilter_register_all();
}

#define MEMOK(var) if(!(var)){return AVERROR(ENOMEM);}
#define BUFSIZE 4096

typedef struct {
	AVIOContext *avioCtx;
	AVFormatContext *fCtx;
} format_ctx;

int _avformat_alloc_context(void *sctx){
	format_ctx *ctx = av_mallocz(sizeof(format_ctx));
	*(size_t*)(sctx)=(size_t)ctx;
	MEMOK(ctx);
	ctx->fCtx=avformat_alloc_context();
	MEMOK(ctx->fCtx);
	return 0;
}

int _avformat_open_input(size_t sctx, char *name) {
	format_ctx *ctx = (format_ctx*)sctx;
	int err = avformat_open_input(&ctx->fCtx,name,NULL,NULL);
	if(err<0) return err;
	return avformat_find_stream_info(ctx->fCtx,NULL);
}

int _avformat_open_goio(size_t sctx, int doseek) {
	format_ctx *ctx = (format_ctx*)sctx;
	void *buf = av_malloc(BUFSIZE);
	MEMOK(buf);
	int64_t(*seek)(void*,int64_t,int)=NULL;
	if(doseek){
		seek=&goseek;
	}
	ctx->avioCtx=avio_alloc_context(buf,BUFSIZE,0,ctx,&goread,NULL,seek);
	MEMOK(ctx->avioCtx);
	ctx->fCtx->pb=ctx->avioCtx;
	return 0;
}

void _free_format(size_t sctx){
	format_ctx *ctx = (format_ctx*)sctx;
	if(ctx){
		if(ctx->fCtx){
			if(ctx->avioCtx){
				av_freep(&ctx->avioCtx->buffer);
				avio_context_free(ctx->avioCtx);
			}
			avformat_close_input(&ctx->fCtx);	
			avformat_free_context(ctx->fCtx);
		}
		av_free((void *)ctx);
	}
}

void _av_dump_format(size_t sctx){
	format_ctx *ctx = (format_ctx*)sctx;
	av_dump_format(ctx->fCtx,0,"",0);
}

int _find_stream(size_t sctx, enum AVMediaType type){
	format_ctx *ctx = (format_ctx*)sctx;/*
	for(int i=0;i<ctx->fCtx->nb_streams;i++){
		if(ctx->fCtx->streams[i]->codec->codec_type==type){
			return i;
		}
	}
	return -1;*/
	return av_find_best_stream(ctx->fCtx,type,-1,-1,NULL,0);
}

void _set_discard(size_t sctx){
	format_ctx *ctx = (format_ctx*)sctx;
	for(int i=0;i<ctx->fCtx->nb_streams;i++){
		ctx->fCtx->streams[i]->discard=AVDISCARD_ALL;
	}
}

typedef struct {
	AVStream *stream;
	AVCodec *codec;
	AVCodecContext *cCtx;
	format_ctx *fmt;
	int id;
} codec_ctx;

int _get_codec(size_t sctx,void *scctx,int stream){
	format_ctx *fctx = (format_ctx*)sctx;
	codec_ctx *ctx = av_mallocz(sizeof(codec_ctx));
	MEMOK(ctx);
	*(codec_ctx**)scctx=ctx;

	ctx->stream=fctx->fCtx->streams[stream];
	ctx->id=stream;
	ctx->fmt=fctx;
	ctx->stream->discard=AVDISCARD_DEFAULT;

	ctx->codec=avcodec_find_decoder(ctx->stream->codecpar->codec_id);
	if(!ctx->codec) return AVERROR(EINVAL);

	ctx->cCtx = avcodec_alloc_context3(ctx->codec);
	MEMOK(ctx->cCtx);

	return avcodec_parameters_to_context(ctx->cCtx,ctx->stream->codecpar);
}

int _open_codec(size_t sctx){
	codec_ctx *ctx = (codec_ctx*)sctx;
	AVDictionary *opts=NULL;
	av_dict_set(&opts,"threads","1",0);
	return avcodec_open2(ctx->cCtx,ctx->codec,&opts);
}

void _free_codec(size_t sctx){
	codec_ctx *ctx = (codec_ctx*)sctx;
	if(ctx){
		if(ctx->cCtx)avcodec_free_context(&ctx->cCtx);
		av_free(ctx);
	}
}

void _get_size_info(size_t sctx,void *width, void *height, void *maxLowres){
	codec_ctx *ctx = (codec_ctx*)sctx;
	*(int*)width=ctx->cCtx->width;
	*(int*)height=ctx->cCtx->height;
	*(int*)maxLowres=av_codec_get_max_lowres(ctx->codec);
}

void _av_codec_set_lowres(size_t sctx, int lowres){
	av_codec_set_lowres(((codec_ctx*)sctx)->cCtx,lowres);
}

int _av_frame_alloc(void *sframe){
	AVFrame *frame = av_frame_alloc();
	MEMOK(frame);
	*(AVFrame**)(sframe)=frame;
	return 0;
}

void _av_frame_free(size_t sframe){
	AVFrame *frame = (AVFrame*)sframe;
	av_frame_free(&frame);
}

int _avcodec_receive_frame(size_t sctx, size_t sframe) {
	codec_ctx *ctx = (codec_ctx*)sctx;
	AVFrame *frame = (AVFrame*)sframe;
	AVPacket pkt;
	int err, rferr=0;
	int got_frame=0;
	for(;;){
		// Read frame from buffer
		err = avcodec_receive_frame(ctx->cCtx,frame);
		if(err!=AVERROR(EAGAIN)){
			// Got a frame or a fatal error
			return err;
		}
		//EAGAIN = needs more data
		if(rferr<0){
			// There will be no more data, and no more frames can be decoded, exit
			return rferr;
		}
		for(;;){
			// Stack-alloc a packet
			av_init_packet(&pkt);
			pkt.data=NULL;
			pkt.size=0;
			rferr=av_read_frame(ctx->fmt->fCtx,&pkt);
			if(rferr<0){
				// Enter EOF mode, flush the decoder and try to get any last frames in the pipeline
				pkt.data=NULL;
				pkt.size=0;
				break;
			}

			if(ctx->id==pkt.stream_index){
				// Right stream, decode now
				break;
			}
			// Not the stream we want, discard
			av_packet_unref(&pkt);
		}
		// Update decoder
		err=avcodec_send_packet(ctx->cCtx,&pkt);
		av_packet_unref(&pkt);
		if(err<0){
			return err;
		}
	}
	return 0;
}

void _avframe_size(size_t sframe, void* width, void *height) {
	AVFrame *frame = (AVFrame*)sframe;
	*(int*)width=frame->width;
	*(int*)height=frame->height;
}

void _avframe_set_size(size_t sframe, int width, int height) {
	AVFrame *frame = (AVFrame*)sframe;
	frame->width=width;
	frame->height=height;
}

int _avframe_format(size_t sframe) {
	AVFrame *frame = (AVFrame*)sframe;
	return frame->format;
}

int _avframe_stride(size_t sframe, int i) {
	AVFrame *frame = (AVFrame*)sframe;
	return frame->linesize[i];
}	

void _avframe_fill_white(size_t sframe){
	AVFrame *frame = (AVFrame*)sframe;
	memset(frame->data[0],255,frame->linesize[0]*frame->height);
}

size_t _avframe_data(size_t sframe, int i) {
	AVFrame *frame = (AVFrame*)sframe;
	return (size_t)(frame->data[i]);
}

int _av_frame_get_buffer(size_t sframe, int w, int h, int fmt, int range){
	AVFrame *frame = (AVFrame*)sframe;
	frame->format=fmt;
	frame->width=w;
	frame->height=h;
	if (range!=AVCOL_RANGE_UNSPECIFIED){
		av_frame_set_color_range(frame,range);
	}
	return av_frame_get_buffer(frame,32);
}

int _avframe_has_alpha(size_t sframe){
	AVFrame *frame = (AVFrame*)sframe;
	int fmt=frame->format;
	#define H(T) (fmt == AV_PIX_FMT_ ## T )||
	return 
		H(ARGB)
		H(RGBA)
		H(ABGR)
		H(BGRA)
		H(YUVA420P)
		H(YA8)
		H(RGBA64BE)
		H(RGBA64LE)
		H(BGRA64BE)
		H(BGRA64LE)
		H(YA16BE)
		H(YA16LE)
		H(GBRAP)
		H(GBRAP16BE)
		H(GBRAP16LE)
		H(AYUV64LE)
		H(AYUV64BE)
		0;
}

int _avframe_has_ycbcr(size_t sframe){
	AVFrame *frame = (AVFrame*)sframe;
  const AVPixFmtDescriptor *desc = av_pix_fmt_desc_get(frame->format);
  if(desc==NULL){
  	return 0;
  }
	return !(desc->flags & AV_PIX_FMT_FLAG_RGB) && desc->nb_components >= 2;
}

int _sws_scale(size_t ssframe, size_t dsframe, int flags) {
	AVFrame *s = (AVFrame*)ssframe;
	AVFrame *d = (AVFrame*)dsframe;
	struct SwsContext *ctx = sws_getContext(
		s->width,s->height,s->format,
		d->width,d->height,d->format,
		flags,NULL,NULL,NULL);
	MEMOK(ctx);

	int err = sws_scale(
		ctx,
		(const uint8_t*const*)s->data,s->linesize,0,s->height,
		d->data,d->linesize);
	sws_freeContext(ctx);
	return err;
}


typedef struct {
	AVFilterInOut *inputs;
	AVFilterGraph *filter_graph;
	AVFilterContext *buffersink_ctx;
} filter_ctx;


int _avfilter_alloc(void *sctx){
	filter_ctx *ctx = av_mallocz(sizeof(filter_ctx));
	MEMOK(ctx);
	*(size_t*)(sctx)=(size_t)ctx;
  
  ctx->filter_graph = avfilter_graph_alloc();
  MEMOK(ctx->filter_graph)

  ctx->inputs = avfilter_inout_alloc();
  MEMOK(ctx->inputs)
  return 0;
}

int _avfilter_add_input(void *sictx, size_t sctx, char *name, size_t sframe) {
	AVFrame *frame = (AVFrame*)(sframe);
	AVFilterContext **fctx = (AVFilterContext**)sictx;
	filter_ctx *ctx = (filter_ctx*)(sctx);

	AVFilter *buffersrc  = avfilter_get_by_name("buffer");
	if(buffersrc==NULL) return AVERROR(EINVAL);

	char args[128];
  snprintf(args, sizeof(args),"video_size=%dx%d:pix_fmt=%d:time_base=25:pixel_aspect=%d/%d",
  	frame->width, frame->height, frame->format,frame->sample_aspect_ratio.num, frame->sample_aspect_ratio.den);
	int err = avfilter_graph_create_filter(fctx, buffersrc, name, args, NULL, ctx->filter_graph);
	if(err<0)return err;

	AVFilterInOut *nin = avfilter_inout_alloc();
	nin->name=av_strdup(name);
	nin->filter_ctx=*fctx;
	nin->pad_idx=0;
	nin->next=ctx->inputs;
	ctx->inputs=nin;

	return 0;
}


int _avfilter_add_frame(size_t sctx, size_t sframe) {
	AVFilterContext *filt = (AVFilterContext*)(sctx);
	AVFrame *frame = (AVFrame*)(sframe);
	return av_buffersrc_add_frame_flags(filt,frame,AV_BUFFERSRC_FLAG_KEEP_REF);
}

void _avframe_clear_pts(size_t sframe) {
	AVFrame *frame = (AVFrame*)(sframe);
	frame->pts=AV_NOPTS_VALUE;
}

void _avfilter_free_input(size_t sctx) {
	AVFilterContext *filt = (AVFilterContext*)(sctx);
	avfilter_free(filt);
}

int _avfilter_graph_config(size_t sctx, char* filters_descr, int fmt) {
	filter_ctx *ctx = (filter_ctx*)(sctx);

	AVFilter *buffersink = avfilter_get_by_name("buffersink");
  int err = avfilter_graph_create_filter(&ctx->buffersink_ctx, buffersink, "out", NULL, NULL, ctx->filter_graph);
  if(err<0) return err;
 	enum AVPixelFormat pix_fmts[] = {fmt,AV_PIX_FMT_NONE};
  err = av_opt_set_int_list(ctx->buffersink_ctx, "pix_fmts", pix_fmts, AV_PIX_FMT_NONE, AV_OPT_SEARCH_CHILDREN);
  if(err<0) return err;

  AVFilterInOut *outputs = avfilter_inout_alloc();
  outputs->name       = av_strdup("out");
  outputs->filter_ctx = ctx->buffersink_ctx;
  outputs->pad_idx    = 0;
  outputs->next       = NULL;

  err = avfilter_graph_parse_ptr(ctx->filter_graph, filters_descr,&outputs, &ctx->inputs, NULL);
  if(err<0) goto end;
  err = avfilter_graph_config(ctx->filter_graph, NULL);
	if(err<0) goto end;
end:
  avfilter_inout_free(&outputs);
  return err;
}

void _avfilter_ctx_free(size_t sctx) {
	filter_ctx *ctx = (filter_ctx*)(sctx);

	if(ctx){
	  avfilter_inout_free(&ctx->inputs);
		avfilter_graph_free(&ctx->filter_graph);
		av_free(ctx);
	}
}

int _avfilter_get_frame(size_t sctx, size_t sframe) {
	filter_ctx *ctx = (filter_ctx*)(sctx);
	AVFrame *frame = (AVFrame*)sframe;
	return av_buffersink_get_frame(ctx->buffersink_ctx,frame);
}