// Copyright 2017 Yumeko Contributors (as named in CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the LICENSE file

package ffmpeg

// #include "format.h"
// #include "stdlib.h"
import "C"
import (
	"image"
	"io"
	"reflect"
	"unsafe"
)

func init() {
	C.init()
}

type FormatCtx uintptr

func OpenFormatFile(name string) (f FormatCtx, err error) {
	err = mkerrWrap("FormatAllocContext", C._avformat_alloc_context(unsafe.Pointer(&f)))
	if err != nil {
		return
	}
	cname := C.CString(name)
	err = mkerrWrap("FormatOpenInput", C._avformat_open_input(C.size_t(f), cname))
	C.free(unsafe.Pointer(cname))
	return
}

func OpenFormatStream(stream io.Reader) (f FormatCtx, err error) {
	err = mkerrWrap("FormatAllocContext", C._avformat_alloc_context(unsafe.Pointer(&f)))
	if err != nil {
		return
	}
	seek := mkgoio(stream, uintptr(f))
	iseek := 0
	if seek {
		iseek = 1
	}
	err = mkerrWrap("FormatOpenGOIO", C._avformat_open_goio(C.size_t(f), C.int(iseek)))
	if err != nil {
		return
	}
	err = mkerrWrap("FormatOpenInput", C._avformat_open_input(C.size_t(f), nil))
	return
}

func (f FormatCtx) Free() {
	goread_free(uintptr(f))
	C._free_format(C.size_t(f))
}

func (f FormatCtx) Dump() {
	C._av_dump_format(C.size_t(f))
}

type MediaType C.enum_AVMediaType

const (
	MediaTypeUnkown     MediaType = C.AVMEDIA_TYPE_UNKNOWN
	MediaTypeVideo      MediaType = C.AVMEDIA_TYPE_VIDEO
	MediaTypeAudio      MediaType = C.AVMEDIA_TYPE_AUDIO
	MediaTypeData       MediaType = C.AVMEDIA_TYPE_DATA
	MediaTypeSubtitle   MediaType = C.AVMEDIA_TYPE_SUBTITLE
	MediaTypeAttachment MediaType = C.AVMEDIA_TYPE_ATTACHMENT
	MediaTypeNb         MediaType = C.AVMEDIA_TYPE_NB
)

func (f FormatCtx) FindStream(typ MediaType) (int, error) {
	stream := C._find_stream(C.size_t(f), C.enum_AVMediaType(typ))
	err := mkerrWrap("FindStream", stream)
	if err != nil {
		return -1, err
	}
	return int(stream), nil
}

func (f FormatCtx) SetDiscard() {
	C._set_discard(C.size_t(f))
}

type CodecCtx uintptr

func (f FormatCtx) GetCodec(stream int) (c CodecCtx, err error) {
	err = mkerrWrap("GetCodec", C._get_codec(C.size_t(f), unsafe.Pointer(&c), C.int(stream)))
	return
}

func (c CodecCtx) Open() error {
	return mkerrWrap("OpenCodec", C._open_codec(C.size_t(c)))
}

func (c CodecCtx) Free() {
	C._free_codec(C.size_t(c))
}

func (c CodecCtx) GetSizeInfo() (width, height, maxStride int) {
	C._get_size_info(C.size_t(c), unsafe.Pointer(&width), unsafe.Pointer(&height), unsafe.Pointer(&maxStride))
	return
}

func (c CodecCtx) SetLowres(lowres int) {
	C._av_codec_set_lowres(C.size_t(c), C.int(lowres))
}

type Frame uintptr

func MakeFrame() (f Frame, err error) {
	err = mkerrWrap("MakeFrame", C._av_frame_alloc(unsafe.Pointer(&f)))
	return
}

func (f Frame) FillYCbCr420(w, h int) error {
	return mkerrWrap("FillYCbCr420", C._av_frame_get_buffer(C.size_t(f), C.int(w), C.int(h), C.AV_PIX_FMT_YUVJ420P, C.AVCOL_RANGE_JPEG))
}

var srRatios = map[image.YCbCrSubsampleRatio]C.int{
	image.YCbCrSubsampleRatio444: C.AV_PIX_FMT_YUVJ444P,
	image.YCbCrSubsampleRatio422: C.AV_PIX_FMT_YUVJ422P,
	image.YCbCrSubsampleRatio420: C.AV_PIX_FMT_YUVJ420P,
	image.YCbCrSubsampleRatio440: C.AV_PIX_FMT_YUVJ440P,
	image.YCbCrSubsampleRatio411: C.AV_PIX_FMT_YUVJ411P,
	image.YCbCrSubsampleRatio410: C.AV_PIX_FMT_YUV410P,
}
var rsrRatios = map[C.int]image.YCbCrSubsampleRatio{
	C.AV_PIX_FMT_YUV444P:  image.YCbCrSubsampleRatio444,
	C.AV_PIX_FMT_YUV422P:  image.YCbCrSubsampleRatio422,
	C.AV_PIX_FMT_YUV420P:  image.YCbCrSubsampleRatio420,
	C.AV_PIX_FMT_YUV440P:  image.YCbCrSubsampleRatio440,
	C.AV_PIX_FMT_YUV411P:  image.YCbCrSubsampleRatio411,
	C.AV_PIX_FMT_YUV410P:  image.YCbCrSubsampleRatio410,
	C.AV_PIX_FMT_YUVJ444P: image.YCbCrSubsampleRatio444,
	C.AV_PIX_FMT_YUVJ422P: image.YCbCrSubsampleRatio422,
	C.AV_PIX_FMT_YUVJ420P: image.YCbCrSubsampleRatio420,
	C.AV_PIX_FMT_YUVJ440P: image.YCbCrSubsampleRatio440,
	C.AV_PIX_FMT_YUVJ411P: image.YCbCrSubsampleRatio411,
}

func (f Frame) FillYCbCr(w, h int, ratio image.YCbCrSubsampleRatio) error {
	return mkerrWrap("FillYCbCr", C._av_frame_get_buffer(C.size_t(f), C.int(w), C.int(h), srRatios[ratio], C.AVCOL_RANGE_JPEG))
}

func (f Frame) FillRGBA(w, h int) error {
	return mkerrWrap("FillRGBA", C._av_frame_get_buffer(C.size_t(f), C.int(w), C.int(h), C.AV_PIX_FMT_RGBA, C.AVCOL_RANGE_UNSPECIFIED))
}

func (f Frame) FillWhite(w, h int) error {
	err := mkerrWrap("FillWhite", C._av_frame_get_buffer(C.size_t(f), C.int(w), C.int(h), C.AV_PIX_FMT_GRAY8, C.AVCOL_RANGE_UNSPECIFIED))
	if err == nil {
		C._avframe_fill_white(C.size_t(f))
	}
	return err
}

func (f Frame) Free() {
	C._av_frame_free(C.size_t(f))
}

func (c CodecCtx) ReadFrame(f Frame) error {
	return mkerrWrap("ReadFrame", C._avcodec_receive_frame(C.size_t(c), C.size_t(f)))
}

func (f Frame) Size() (width int, height int) {
	C._avframe_size(C.size_t(f), unsafe.Pointer(&width), unsafe.Pointer(&height))
	return
}

func (f Frame) SetSize(width int, height int) {
	C._avframe_set_size(C.size_t(f), C.int(width), C.int(height))
}

func (f Frame) format() C.int {
	return C._avframe_format(C.size_t(f))
}

func (f Frame) HasAlpha() bool {
	return C._avframe_has_alpha(C.size_t(f)) != C.int(0)
}

type SWSFlag int

const (
	SWSFastBilinear SWSFlag = C.SWS_FAST_BILINEAR
	SWSBilinear     SWSFlag = C.SWS_BILINEAR
	SWSBicubic      SWSFlag = C.SWS_BICUBIC
	SWSX            SWSFlag = C.SWS_X
	SWSPoint        SWSFlag = C.SWS_POINT
	SWSArea         SWSFlag = C.SWS_AREA
	SWSBicublin     SWSFlag = C.SWS_BICUBLIN
	SWSGuass        SWSFlag = C.SWS_GAUSS
	SWSSinc         SWSFlag = C.SWS_SINC
	SWSLanczos      SWSFlag = C.SWS_LANCZOS
	SWSSpline       SWSFlag = C.SWS_SPLINE
)

func (f Frame) Scale(t Frame, flags SWSFlag) error {
	return mkerrWrap("SwsScale", C._sws_scale(C.size_t(f), C.size_t(t), C.int(flags)))
}

func (f Frame) GoImg() image.Image {
	w, h := f.Size()
	bounds := image.Rect(0, 0, w, h)
	format := f.format()
	switch format {
	case C.AV_PIX_FMT_RGBA:
		stride := f.stride(0)
		return &image.NRGBA{
			Pix:    u8slice(f.data(0), stride*h),
			Stride: stride,
			Rect:   bounds,
		}
	case
		C.AV_PIX_FMT_YUV444P,
		C.AV_PIX_FMT_YUV422P,
		C.AV_PIX_FMT_YUV420P,
		C.AV_PIX_FMT_YUV440P,
		C.AV_PIX_FMT_YUV411P,
		C.AV_PIX_FMT_YUV410P,
		C.AV_PIX_FMT_YUVJ444P,
		C.AV_PIX_FMT_YUVJ422P,
		C.AV_PIX_FMT_YUVJ420P,
		C.AV_PIX_FMT_YUVJ440P,
		C.AV_PIX_FMT_YUVJ411P:
		ys := f.stride(0)
		cs := f.stride(1)
		cs2 := f.stride(2)
		if cs2 < cs {
			cs = cs2
		}
		ch := h
		switch format {
		case
			C.AV_PIX_FMT_YUV420P,
			C.AV_PIX_FMT_YUVJ420P:
			ch /= 2
		}
		return &image.YCbCr{
			Y:              u8slice(f.data(0), ys*h),
			Cb:             u8slice(f.data(1), cs*ch),
			Cr:             u8slice(f.data(2), cs*ch),
			YStride:        ys,
			CStride:        cs,
			SubsampleRatio: rsrRatios[format],
			Rect:           bounds,
		}
	default:
		return nil
	}
}

func (f Frame) stride(i int) int {
	return int(C._avframe_stride(C.size_t(f), C.int(i)))
}
func (f Frame) data(i int) C.size_t {
	return C._avframe_data(C.size_t(f), C.int(i))
}

func (f Frame) ClearTimestamp() {
	C._avframe_clear_pts(C.size_t(f))
}

func u8slice(ptr C.size_t, len int) []uint8 {
	var t []uint8
	sh := (*reflect.SliceHeader)(unsafe.Pointer(&t))
	sh.Len = int(len)
	sh.Cap = int(len)
	sh.Data = uintptr(ptr)
	return t
}

type Filter uintptr

func MakeFilter() (f Filter, err error) {
	err = mkerr(C._avfilter_alloc(unsafe.Pointer(&f)))
	return
}

type FilterInput uintptr

func (f Filter) AddInput(frm Frame, name string) (i FilterInput, err error) {
	cname := C.CString(name)
	defer C.free(unsafe.Pointer(cname))
	err = mkerr(C._avfilter_add_input(unsafe.Pointer(&i), C.size_t(f), cname, C.size_t(frm)))
	return
}

func (f FilterInput) Add(frame Frame) error {
	return mkerr(C._avfilter_add_frame(C.size_t(f), C.size_t(frame)))
}

func (f FilterInput) Free() {
	C._avfilter_free_input(C.size_t(f))
}

func (f Filter) Configure(descr string, ycbcr bool) error {
	cdesc := C.CString(descr)
	defer C.free(unsafe.Pointer(cdesc))

	var fmt = C.int(C.AV_PIX_FMT_RGBA)
	if ycbcr {
		fmt = C.AV_PIX_FMT_YUV420P
	}

	return mkerr(C._avfilter_graph_config(C.size_t(f), cdesc, fmt))
}

func (f Filter) Free() {
	C._avfilter_ctx_free(C.size_t(f))
}

func (f Filter) Get(frame Frame) error {
	return mkerr(C._avfilter_get_frame(C.size_t(f), C.size_t(frame)))
}
