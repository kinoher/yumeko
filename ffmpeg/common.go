// Copyright 2017 Yumeko Contributors (as named in CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the LICENSE file

//package ffmpeg provides partial bindings for libavcodec and libavformat.
package ffmpeg

// #cgo pkg-config: libavcodec
// #cgo pkg-config: libavformat
// #cgo pkg-config: libavutil
// #cgo pkg-config: libswscale
// #cgo pkg-config: libavfilter
// #cgo CFLAGS: -std=c99
//
// #include "libavformat/avformat.h"
// #include "libavcodec/avcodec.h"
import "C"
import (
	"fmt"

	"bitbucket.org/abex/yumeko/glug"
)

func convertVersion(v uint32) (uint16, uint8, uint8) {
	return uint16(v >> 16), uint8(v >> 8), uint8(v)
}

//AVFormatVersion returns the LIBAVFORMAT_VERSION constant
func AVFormatVersion() (uint16, uint8, uint8) {
	return convertVersion(uint32(C.avformat_version()))
}

//AVCodecVersion return the LIBAVCODEC_VERSION constant
func AVCodecVersion() (uint16, uint8, uint8) {
	return convertVersion(uint32(C.avcodec_version()))
}

type AVError C.int

var errnames2 = map[int]string{
	2:  "No such file or directory",
	5:  "I/O Error",
	11: "EAGAIN",
	12: "Out of memory",
	13: "Permission Denied",
	22: "Invalid Argument (EINVAL)",
	24: "Too many open files",
}

var errnames = map[string]string{
	" BSF":          "Bitstream filter not found",
	"BUG ":          "Internal bug in FFmpeg",
	"BUG!":          "Internal bug in FFmpeg",
	"BUFS":          "Buffer to small",
	" DEC":          "Decoder not found",
	" DEM":          "Demuxer not found",
	" ENC":          "Encoder not found",
	"EOF ":          "Unexpected End of File",
	" FIL":          "Filter not found",
	"INDA":          "Invalid input data",
	" MUX":          "Muxer not found",
	" OPT":          "Option not found",
	"PAWE":          "Not implemented in FFmpeg. Patch welcome",
	" PRO":          "Protocol not found",
	" STR":          "Stream not found",
	"+\xb2\xaf\xa8": "Experimental feature only",
}

func (e AVError) Error() string {
	i := -int(e)
	name, nice := errnames2[i]
	if nice {
		return fmt.Sprintf("ffmpeg(%v): %v", i, name)
	}
	b := []byte{uint8(i), uint8(i >> 8), uint8(i >> 16), uint8(i >> 24)}
	for i := range b {
		if b[i] == 0xF8 {
			b[i] = ' '
		}
	}
	name, nice = errnames[string(b)]
	for i := range b {
		if b[i] > 0x7F || b[i] < 0x20 {
			b[i] = '?'
		}
	}
	abbr := string(b)
	if nice {
		return fmt.Sprintf("ffmpeg(%v): %v", abbr, name)
	}
	return fmt.Sprintf("ffmpeg(%v): %v", i, abbr)
}

func mkerr(v C.int) error {
	if v >= 0 {
		return nil
	}
	return AVError(v)
}

func mkerrWrap(name string, v C.int) error {
	if v >= 0 {
		return nil
	}
	return glug.Wrap(AVError(v), name)
}
