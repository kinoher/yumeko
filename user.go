// Copyright 2017 Yumeko Contributors (as named in CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the LICENSE file

package main

import (
	"errors"
	"fmt"
	"hash/crc32"
	"html"
	"math"
	"time"

	"github.com/gorilla/websocket"

	cryptorand "crypto/rand"

	"bitbucket.org/abex/yumeko/conf"
	"bitbucket.org/abex/yumeko/gumble/gumble"
	"bitbucket.org/abex/yumeko/gumble/gumbleutil"
	"gopkg.in/mgo.v2/bson"
)

var userTokenSecret string
var userBanLogChannel string
var userPointMax uint
var userPointRegenRate int

func init() {
	conf.Config(&userTokenSecret, "misc", `
# A secret used to salt login tokens. This should be random and secret.
# Changing this value causes all login tokens to be changed. If a user needs
# to change their token they can message Yumeko 'login reset'
token-secret = "change me"
`)
	conf.Config(&userBanLogChannel, "misc", `
# This channel get a user added to its comment if 
# yumeko bans someone for rate limit abuse
ban-log-channel = "Bad People"
`)
	conf.Config(&userPointMax, "points", `
# The maximum number of points a user han have without filling captchas
max-points = 25
`)
	conf.Config(&userPointRegenRate, "points", `
# How many seconds between yumeko regenerating points
regen-rate = 13
`)
}

//A Permission a User can have
type Permission uint32

//Users holds users in memory temporarily
var Users = map[uint32]*User{}

//ServiceUsers holds service users in memory
var ServiceUsers = map[string]*User{}

const (
	//PermissionSBUpdateName gives the permission to modify a clips name
	PermissionSBUpdateName Permission = 1 << iota
	//PermissionSBUpdateVolume gives the permission to modify a clips volume
	PermissionSBUpdateVolume
	//PermissionSBUpdateCost gives the permission to modify a clips cost
	PermissionSBUpdateCost
	//PermissionSBDelete gives the permission to Remove a clip
	PermissionSBDelete
	//PermissionSBUpload gives the permission to upload a clip
	PermissionSBUpload
	//PermissionSBPlay gives the permission to play a clip
	PermissionSBPlay
	//PermissionUserChangePerms gives the permission to change a user's permissions
	PermissionUserChangePerms
	//PermissionUserMoveAll gives the permission to move everyone
	PermissionUserMoveAll
	//PermissionFileUpload gives the permission to upload a temp file
	PermissionFileUpload
	//PermissionDownloadUserAudio gives the permission to download a user's temp audio
	PermissionDownloadUserAudio
	//PermissionInlineExplicit makes all the links sent only to yumeko inlined
	PermissionInlineExplicit
	//PermissionInlineAlways makes all the links sent inlined
	PermissionInlineAlways
	//PermissionSBChangePlayingVolume gives the permission the change the volume of the currently playing clip
	PermissionSBChangePlayingVolume
	//PermissionFileUploadForever gives the permission to uploda a file for as long as they want
	PermissionFileUploadForever
	//PermissionFileUploadDelete gives the permission to delete a file
	PermissionFileUploadDelete
	//PermissionSBCleanup gives the permission to run the cleanup tool (This can be expensive AND run on all files)
	PermissionSBCleanup
)

//PermissionLookup maps the lowercase permission to its value
var PermissionLookup = map[string]Permission{
	"sbupdatename":          PermissionSBUpdateName,
	"sbupdatevolume":        PermissionSBUpdateVolume,
	"sbupdatecost":          PermissionSBUpdateCost,
	"sbdelete":              PermissionSBDelete,
	"sbupload":              PermissionSBUpload,
	"sbplay":                PermissionSBPlay,
	"userchangeperms":       PermissionUserChangePerms,
	"usermoveall":           PermissionUserMoveAll,
	"fileupload":            PermissionFileUpload,
	"downloaduseraudio":     PermissionDownloadUserAudio,
	"inlineexplicit":        PermissionInlineExplicit,
	"inlinealways":          PermissionInlineAlways,
	"sbchangeplayingvolume": PermissionSBChangePlayingVolume,
	"fileuploadforever":     PermissionFileUploadForever,
	"fileuploaddelete":      PermissionFileUploadDelete,
	"sbcleanup":             PermissionSBCleanup,
}

//User data
type User struct {
	Name        string
	ID          uint32
	Points      uint
	Hash        string
	Service     bool
	Permissions Permission
	Salt        string
	Websockets  map[*websocket.Conn]*WSData
}

//UserDisk stores data as in the database
type UserDisk struct {
	Name        string     `bson:"Name"`
	ID          uint32     `bson:"ID"`
	Hash        string     `bson:"Hash"`
	Permissions Permission `bson:"Permission"`
	Salt        string     `bson:"Salt"`
}

//Deduct a some points from a user.
func (u *User) Deduct(cost uint) error {
	if u.Service {
		return nil
	}
	if u.Points >= cost {
		u.Points -= cost
		u.SendPoints()
		return nil
	}
	return errors.New("Insufficient Funds")
}

//M returns the Mumble version of the user
func (u *User) M() *gumble.User {
	if u.Service {
		return nil
	}
	return FindUser(u.ID)
}

//ChatName returns the user's name for use in chat messgaes.
//If they are connected to mumble their name will be a link and will look 'natural'
func (u *User) ChatName() string {
	mu := u.M()
	if mu != nil {
		return "<a href='clientid://" + mu.Hash + "' class='log-user log-source'>" + html.EscapeString(mu.Name) + "</a>: "
	}
	hash := "service"
	if !u.Service {
		hash = u.Hash
	}
	//Add the href otherwise murmur will get angry + if the user connects the link will work even!
	return "<a href='clientid://" + hash + "' class='log-user log-source'>" + html.EscapeString(u.Name) + "</a>: "
}

// ToDisk gets the storable version of a user
func (u *User) ToDisk() (disk UserDisk) {
	disk.Name = u.Name
	disk.ID = u.ID
	disk.Hash = u.Hash
	disk.Permissions = u.Permissions
	disk.Salt = u.Salt
	return
}

// ToClient returns a JSON representation of a user
func (u UserDisk) ToClient() JSON {
	user := FindUser(u.ID)
	json := JSON{
		"Username":  u.Name,
		"ID":        u.ID,
		"Perms":     u.Permissions,
		"Connected": user != nil,
	}
	if user != nil {
		json["Muted"] = user.SelfMuted || user.Muted || user.Suppressed
		json["Deafened"] = user.SelfDeafened || user.Deafened
	}
	return json
}

//Has returns true if a user has a permission
func (u *User) Has(p Permission) bool {
	if u.ID == 0 {
		return true
	}
	return u.Permissions&p != 0
}

//Token returns the token used to login with, minus user ID
func (u *User) Token() string {
	if u.Service {
		return "Service"
	}
	h := crc32.NewIEEE()
	h.Write([]byte(u.Hash))
	h.Write([]byte(u.Salt))
	h.Write([]byte(userTokenSecret))
	return fmt.Sprintf("%08X", h.Sum32())
}

//GetWebLogin returns the token a user would use to login
func (u *User) GetWebLogin() string {
	if u.Service {
		return u.Hash
	}
	return fmt.Sprintf("%X%s", u.ID, u.Token())
}

//ResetSalt resets a user's salt, effectively changing their login token
func (u *User) ResetSalt() error {
	if u.Service {
		return nil
	}
	s := make([]byte, 16)
	_, err := cryptorand.Read(s)
	str := fmt.Sprintf("%X", s)
	u.Salt = str
	_, err = Db.C("UserData").Upsert(bson.M{
		"ID": u.ID,
	}, u.ToDisk())
	if err != nil {
		return err
	}
	return nil
}

//Limit requests a token from the limiter and applys it to the user
func (u *User) Limit(l *Limiter) error {
	if u.Service {
		return nil
	}
	r := l.Request(u.ID)
	if r == ResponseAllow {
		return nil
	}
	if r == ResponseBan {
		muser := u.M()
		if muser != nil {
			c := FindChannel(userBanLogChannel)
			if c != nil {
				c.SetDescription(c.Description + muser.Name + "<br>")
			}
			muser.Ban("Severe rate limit overage")
		}
	}
	return errors.New("Rate limit exceeded")
}

//Load Ensures a User is in Users
func Load(uid uint32, nocache bool) *User {
	if uid==gumble.UserUnregisteredID || uid == Mumble.Self.UserID {
		return nil
	}
	user, ok := Users[uid]
	if ok && !nocache {
		return user
	}

	var du UserDisk
	var err error
	if uid != 0 {
		err = Db.C("UserData").Find(bson.M{
			"ID": uid,
		}).One(&du)
	}

	mu := FindUser(uid)

	if mu == nil && err != nil { //Not in the db or murmur
		return nil
	}

	user = &User{
		ID:          uid,
		Points:      userPointMax / 2,
		Websockets:  map[*websocket.Conn]*WSData{},
		Permissions: du.Permissions,
		Name:        du.Name,
		Hash:        du.Hash,
		Salt:        du.Salt,
	}
	changed := false
	if err != nil { //not in the db
		changed = true
		s := make([]byte, 16)
		cryptorand.Read(s)
		str := fmt.Sprintf("%X", s)
		user.Salt = str
	}

	if mu != nil {
		changed = changed || (user.Hash != mu.Hash)
		user.Hash = mu.Hash
		changed = changed || (user.Name != mu.Name)
		user.Name = mu.Name
	}

	if uid == 0 { //Superuser
		user.Permissions = math.MaxUint32
	} else if changed {
		_, err = Db.C("UserData").Upsert(bson.M{
			"ID": uid,
		}, user.ToDisk())
		fmt.Println(err)
	}
	Users[uid] = user
	return user
}

//StartUser sets up user stuff. Must have database, config
func StartUser() {
	Mumble.Attach(gumbleutil.Listener{
		ServerConfig: func(e *gumble.ServerConfigEvent) { // connected completely
			for _, v := range Mumble.Users {
				Load(v.UserID, false)
			}
		},
		UserChange: func(e *gumble.UserChangeEvent) {
			if e.Type.Has(gumble.UserChangeConnected) || e.Type.Has(gumble.UserChangeName) {
				Load(e.User.UserID, true)
			}
		},
	})
	go func() {
		for {
			for _, v := range Mumble.Users {
				user := Load(v.UserID, false)
				if user != nil && user.Points < userPointMax && !user.M().SelfMuted {
					user.Points++
					user.SendPoints()
				}
				if user != nil && user.Points > userPointMax {
					user.Points--
					user.SendPoints()
				}
			}
			time.Sleep(time.Second * time.Duration(userPointRegenRate))
		}
	}()
}
