// Copyright 2017 Yumeko Contributors (as named in CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the LICENSE file

package main

import (
	"flag"
	"fmt"
	"os"
	"os/signal"
	"runtime/debug"
	"runtime/pprof"

	"bitbucket.org/abex/yumeko/conf"
	"bitbucket.org/abex/yumeko/util"
)

var keepAlive chan bool

func main() {
	keepAlive = make(chan bool)
	sigint := make(chan os.Signal, 1)
	signal.Notify(sigint, os.Interrupt)
	go func() {
		signal := <-sigint
		pprof.Lookup("goroutine").WriteTo(os.Stdout, 1)
		panic("Caught signal " + signal.String())
	}()

	debug.SetMaxStack(1024 * 1024 * 300)
	debug.SetGCPercent(15)

	fmt.Println("Reading Configs")
	def := util.DefaultConfigDir()
	configPtr := flag.String("config", def, def)
	flag.Parse()

	_, err := os.Stat(*configPtr)
	if os.IsNotExist(err) {
		err = os.MkdirAll(*configPtr, 0770)
		if err != nil {
			panic(err)
		}
	}
	conf.Load(*configPtr)

	err = ReadServiceUsers()
	if err != nil {
		fmt.Println("Cannot read service users")
		panic(err)
	}
	fmt.Println("Connecting to database")

	err = StartDatabase()
	if err != nil {
		fmt.Println("Cannot connect to Database")
		panic(err)
	}
	fmt.Println("Starting HTTP")

	StartHTTP()
	fmt.Println("Starting Gumble")

	StartGumble()
	StartUser()
	StartSound()
	fmt.Println("Starting modules")

	StartupInliner()
	StartWebsocket()
	StartSoundboard()
	StartRecorder()
	StartUpload()

	StartWebInterface()
	StartTextInterface()

	fmt.Println("Started")
	<-keepAlive
}
