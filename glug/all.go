// Copyright 2017 Yumeko Contributors (as named in CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the LICENSE file

package glug

/*
func (l *Logger) \1(args ...interface{}) {
	l.Log(Level\1 ,args...)
}

func (l *Logger) \1f(fmt string, args ...interface{}) {
	l.Logf(Level\1 ,fmt,args...)
}
*/

func (l *Logger) Info(args ...interface{}) {
	l.Log(LevelInfo, args...)
}

func (l *Logger) Infof(fmt string, args ...interface{}) {
	l.Logf(LevelInfo, fmt, args...)
}

func (l *Logger) Weird(args ...interface{}) {
	l.Log(LevelWeird, args...)
}

func (l *Logger) Weirdf(fmt string, args ...interface{}) {
	l.Logf(LevelWeird, fmt, args...)
}

func (l *Logger) Error(args ...interface{}) {
	l.Log(LevelError, args...)
}

func (l *Logger) Errorf(fmt string, args ...interface{}) {
	l.Logf(LevelError, fmt, args...)
}
