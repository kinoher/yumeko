// Copyright 2017 Yumeko Contributors (as named in CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the LICENSE file

// package glug provides a logging framework for modular applications
//
// Construct a `Logger` by calling `MakeLogger` once per package, then
// use that logger throughout that package. You can set individual logging
// levels per package by calling `SetLoggerLevel("PackageName",LevelDebug)`.
// To enable `Debug` and `Trace` levels you must compile with `-tags debug`
// or the calls are empty.
package glug

import (
	"bytes"
	"fmt"
	"os"
)

type KV map[string]interface{}

const (
	// Inside loops
	LevelTrace = -8
	// Packets
	LevelDebug = 0
	// Connections, disconnects, normal operation
	LevelInfo = 8
	// Interesting config values
	LevelWeird = 16
	// Unreachable, Unable to open config
	LevelError = 24
)

type Logger struct {
	Name  string
	Level int
}

var loggers = map[string]*Logger{}

func NewLogger(name string) *Logger {
	log, ok := loggers[name]
	if ok {
		return log
	}
	log = &Logger{
		Name:  name,
		Level: LevelInfo,
	}
	return log
}
func SetLoggerLevel(name string, level int) bool {
	log, ok := loggers[name]
	if ok {
		log.Level = level
	}
	return ok
}

func (l *Logger) logs(level int) *bytes.Buffer {
	b := &bytes.Buffer{}
	str := "ERROR/"
	switch true {
	case level <= LevelTrace:
		str = "TRACE/"
	case level <= LevelDebug:
		str = "DEBUG/"
	case level <= LevelInfo:
		str = "INFO /"
	case level <= LevelWeird:
		str = "WEIRD/"
	}
	b.WriteString(str)
	b.WriteString(l.Name)
	b.WriteString("> ")
	return b
}

func (l *Logger) Log(level int, args ...interface{}) {
	if l.Level > level {
		return
	}
	b := l.logs(level)
	for _, arg := range args {
		if arg == nil {
			b.WriteString("'<nil>' ")
		} else {
			switch arg := arg.(type) {
			case KV:
				for k, v := range arg {
					fmt.Fprint(b, k)
					b.WriteString("='")
					fmt.Fprint(b, v)
					b.WriteString("' ")
				}
			case string:
				b.WriteString("'")
				b.WriteString(arg)
				b.WriteString("' ")
			default:
				b.WriteString("'")
				fmt.Fprint(b, arg)
				b.WriteString("' ")
			}
		}
	}
	b.WriteString("\n")
	b.WriteTo(os.Stderr)
}

func (l *Logger) Logf(level int, format string, args ...interface{}) {
	if l.Level > level {
		return
	}
	b := l.logs(level)
	fmt.Fprintf(b, format, args...)
	b.WriteString("\n")
	b.WriteTo(os.Stderr)
}
