// Copyright 2017 Yumeko Contributors (as named in CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the LICENSE file

package glug

import (
	"bytes"
	"fmt"
	"strings"
)

type MultiError []error

func Combine(errs []error) error {
	if len(errs) == 0 {
		return nil
	}
	return MultiError(errs)
}

func (m MultiError) Error() string {
	b := bytes.Buffer{}
	b.WriteString(fmt.Sprintf("%v errors:\n", len(m)))
	for i, err := range m {
		lines := strings.Split(err.Error(), "\n")
		if len(lines) == 0 {
			continue
		}
		b.WriteString(fmt.Sprintf(" %v: %v\n", i+1, lines[0]))
		if len(lines) > 1 {
			for _, line := range lines[1:] {
				b.WriteString("    ")
				b.WriteString(line)
				b.WriteString("\n")
			}
		}
	}
	return b.String()
}

func (m MultiError) String() string {
	return m.Error()
}
