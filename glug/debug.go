// Copyright 2017 Yumeko Contributors (as named in CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the LICENSE file

// +build debug

package glug

func (l *Logger) Trace(args ...interface{}) {
	l.Log(LevelTrace, args...)
}

func (l *Logger) Tracef(fmt string, args ...interface{}) {
	l.Logf(LevelTrace, fmt, args...)
}

func (l *Logger) Debug(args ...interface{}) {
	l.Log(LevelDebug, args...)
}

func (l *Logger) Debugf(fmt string, args ...interface{}) {
	l.Logf(LevelDebug, fmt, args...)
}
