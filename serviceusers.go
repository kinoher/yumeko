// Copyright 2017 Yumeko Contributors (as named in CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the LICENSE file

package main

import (
	"strings"

	"bitbucket.org/abex/yumeko/conf"
	"bitbucket.org/abex/yumeko/glug"

	"github.com/gorilla/websocket"
)

func ReadServiceUsers() error {
	log := glug.NewLogger("service-users")
	return conf.OpenFileDefault("service.json", func(sks []ServiceUser, err error) {
		if err != nil {
			log.Errorf("Erorr opening service.json: %v", err)
		}
		for _, sk := range sks {
			var p Permission
			for _, pstr := range sk.Permissions {
				b, ok := PermissionLookup[strings.ToLower(pstr)]
				if !ok {
					log.Errorf("Cannot find permission: %v", pstr)
					continue
				}
				p |= b
			}
			if !sk.Disabled {
				ServiceUsers[sk.Key] = &User{
					Permissions: p,
					Name:        sk.Name,
					Hash:        sk.Key,
					Websockets:  map[*websocket.Conn]*WSData{},
					Service:     true,
				}
			}
		}
	}, `[
	{
		"Name":"Icecast",
		"Key":"changeme",
		"Permissions":[],
		"Disabled":true
	}
]`)
}

//ServiceUser is the on disk form of a non existant user.
//This is desined for external services that interact with the HTTP API only,
//so they lack a mumble user alias.
type ServiceUser struct {
	Name        string
	Permissions []string
	Key         string
	Disabled    bool
}
