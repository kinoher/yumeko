// Copyright 2017 Yumeko Contributors (as named in CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the LICENSE file

package main

import (
	"bytes"
	"fmt"
	"html"
	"strings"

	"bitbucket.org/abex/yumeko/commonConfig"
	"bitbucket.org/abex/yumeko/gumble/gumble"
	"bitbucket.org/abex/yumeko/gumble/gumbleutil"
	"bitbucket.org/abex/yumeko/versioning"
)

var inlineLimit = NewLimiterConf("inliner", "5000", "15", "30")

//StartTextInterface starts the text interface
func StartTextInterface() {
	var lastChannel uint32
	Mumble.Attach(gumbleutil.Listener{
		TextMessage: func(e *gumble.TextMessageEvent) {
			if e.Sender == nil {
				return
			}
			user := Load(e.Sender.UserID, false)
			if user == nil {
				return
			}
			private := e.Channels == nil || len(e.Channels) == 0
			switch strings.ToLower(e.Message) {
			case "help":
				buf := &bytes.Buffer{}
				buf.WriteString(fmt.Sprintf(`Yumeko %v.%v.%v<br/>`, versioning.Major, versioning.Minor, versioning.Patch))
				buf.WriteString(html.EscapeString(versioning.Commit))
				if versioning.Date != "" {
					buf.WriteString(`<br/>Built on `)
					buf.WriteString(html.EscapeString(versioning.Date))
				}
				buf.WriteString("<br/>")
				version, err := TryInlining("thumblink version")
				if err == nil {
					buf.WriteString(version)
					buf.WriteString("<br/>")
				}
				buf.WriteString(`<br/>Help:<br/>
stop: Stop the currently playing clip.<br/>
login: Lets you login to the webui.<br/>
login sharex: Creates a custom uploader for <a href="https://getsharex.com">ShareX</a>.<br/>
login reset: Gives you a new login token, invalidating the old one.<br/>
moveall: Moves all users to you.<br/>`)
				help, err := TryInlining("thumblink help")
				if err == nil {
					buf.WriteString(help)
					buf.WriteString("<br/>")
				}
				e.Sender.Send(buf.String())
			case "stop":
				err := user.Deduct(1)
				if err != nil {
					e.Sender.Send(err.Error())
					return
				}
				SoundStop(SoundChannelMain, false)
			case "login reset":
				err := user.ResetSalt()
				if err != nil {
					e.Sender.Send(err.Error())
					return
				}
				e.Sender.Send("Your login token is now different")
				fallthrough
			case "login sharex":
				tok := Load(e.Sender.UserID, false).GetWebLogin()
				e.Sender.Send(`Add this as a Custom Uploader in ShareX. This contains your login token, so keep it safe.<pre>
{
  "Name": "Yumeko",
  "DestinationType": "ImageUploader, TextUploader, FileUploader",
  "RequestType": "POST",
  "RequestURL": "` + commonConfig.ExternalHostname + `api/upload/upload",
  "FileFormName": "upload",
  "Arguments": {
   "inline": "1",
   "key": "` + tok + `"
  },
  "URL": "$json:path$"
}`)
			case "login":
				tok := Load(e.Sender.UserID, false).GetWebLogin()
				loginurl := commonConfig.ExternalHostname + "#," + tok
				qrcode, err := MakeQRCode(loginurl)
				if err != nil {
					fmt.Println(err)
				}
				e.Sender.Send(
					`<table>
	<tr>
		<th width="210">Web</th>
		<th width="1"></th>
		<th width="210">Mobile</th>
	</tr>
	<tr>
		<td>
			Your weblogin ID is: <tt>` + tok + `</tt><br/>
			You can login at <a href="` + loginurl + `">` + commonConfig.ExternalHostname + `</a>.<br/>
			Keep this key safe as it linked to your certificate.<br/>
			To change this key message me:<br/><tt>login reset</tt>.<br/>
		</td><td>
			<img src="data:image/jpeg;base64,/9j/2wCEAAIBAQEBAQIBAQECAgICAgQDAgICAgUEBAMEBgUGBgYFBgYGBwkIBgcJBwYGCAsICQoKCgoKBggLDAsKDAkKCgoBAgICAgICBQMDBQoHBgcKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCv/AABEIANgAAQMBIgACEQEDEQH/xAGiAAABBQEBAQEBAQAAAAAAAAAAAQIDBAUGBwgJCgsQAAIBAwMCBAMFBQQEAAABfQECAwAEEQUSITFBBhNRYQcicRQygZGhCCNCscEVUtHwJDNicoIJChYXGBkaJSYnKCkqNDU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6g4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY2drh4uPk5ebn6Onq8fLz9PX29/j5+gEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoLEQACAQIEBAMEBwUEBAABAncAAQIDEQQFITEGEkFRB2FxEyIygQgUQpGhscEJIzNS8BVictEKFiQ04SXxFxgZGiYnKCkqNTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqCg4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY2dri4+Tl5ufo6ery8/T19vf4+fr/2gAMAwEAAhEDEQA/AP3Z/wCEdi/550f8I7F/zzra8yH0FHmQ+goA5z/hJY/74/Oj/hJY/wC+Pzryj/hOV/57/rR/wnK/89/1oA8U/wCE9/6a/rR/wnv/AE1/WvG/+EzP/PZvzo/4TM/89m/OgDzL/hJX/wCeh/Oj/hJX/wCeh/OuN/tQ/wB8/nR/ah/vn86AMT7V70faveqe/wD2v1o3/wC1+tAEW9fWjevrTKKACiiigAooooAKKlwfQ0YPoaAJvJajyWrS+xH+6fzo+xH+6fzoA6H+xX/uH8qP7Ff+4fyruv8AhFf+mNH/AAiv/TGgD1f/AIQc/wDPA/lR/wAIOf8Angfyr2n/AIQEf88D+VH/AAgI/wCeB/KgD2n/AIQlP+eI/wC+aP8AhCU/54j/AL5r0f8AsSP+4Pzo/sSP+4PzoA0PtMX94fnR9pi/vD86p0UAf//Z"/>
		</td><td style="float:right;">
			` + qrcode + `
		</td>
	</tr>
</table>`)
			case "moveall":
				err := MoveAllTo(user)
				if err != nil {
					e.Sender.Send("Error: " + err.Error())
					return
				}
			default:
				shouldTryInlining := user != nil && (user.Has(PermissionInlineAlways) || (private && user.Has(PermissionInlineExplicit)))
				if shouldTryInlining {
					err := user.Limit(inlineLimit)
					if err != nil {
						e.Sender.Send("Error:" + err.Error())
						return
					}
					go func() {
						newMsg, err := TryInlining(e.Message)
						if private {
							if newMsg == "" {
								newMsg = e.Message
							}
						} else {
							if newMsg == "" {
								return
							}
						}
						if err != nil {
							e.Sender.Send("Error:<pre>" + html.EscapeString(err.Error()) + "</pre>")
						}
						final := "<a href='clientid://" + e.Sender.Hash + "' class='log-user log-source'>" + e.Sender.Name + "</a>:" + newMsg
						e.Sender.Channel.Send(final, false)
					}()
				}
			}
		},
		Connect: func(e *gumble.ConnectEvent) {
			lastChannel = Mumble.Self.Channel.ID
		},
		UserChange: func(e *gumble.UserChangeEvent) {
			if e.Type == gumble.UserChangeChannel && e.User != nil && e.User.Session == Mumble.Self.Session {
				if e.Actor != nil && e.Actor.Channel != nil && e.Actor.Channel.ID == lastChannel && e.Actor.Session != Mumble.Self.Session {
					user := Load(e.Actor.UserID, false)
					if user == nil || !user.Has(PermissionUserMoveAll) {
						return
					}
					c, ok := Mumble.Channels[lastChannel]
					if !ok {
						return
					}
					for _, u := range c.Users {
						u.Move(Mumble.Self.Channel)
					}
				}
				lastChannel = Mumble.Self.Channel.ID
			}
		},
	})
}
