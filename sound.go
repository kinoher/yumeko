// Copyright 2017 Yumeko Contributors (as named in CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the LICENSE file

package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"os/exec"
	"strings"
	"time"

	"bitbucket.org/abex/sliceconv/sliceconv"
	"bitbucket.org/abex/yumeko/conf"
	"bitbucket.org/abex/yumeko/opus"
)

//SoundChannel specs the importance of the audio
const (
	SoundChannelMain int = iota
	SoundChannelBackground
	SoundChannelOverlay
)

var soundEncoderMode = make(chan bool, 3)

//Sound holds data for playing sounds on a default player
type Sound struct {
	//Opt: Name of string
	Name string
	//Opt: Who played name
	Owner string
	//Opt: Soundboard id
	ID string
	//Opt: Duration (sec)
	Duration uint32
	//Req: Input passed to ffmpeg
	File string
	//Req for main: Points to inturrupt
	Inturrupt uint
	//Req SoundChannel to play on
	Channel int
	//Opt: (input) Channel to use 0 <= = mux
	ChannelMode uint8
	//Max play time samples @ 48khz
	TimeLimit int
	//Mode to play
	EncoderMode uint8
	//Raw volume of clip
	Vol      float32
	Stopping bool
	d        chan []float32
}

//Start starts the sound
func (s *Sound) Start() error {
	args := []string{"-i", s.File}
	if s.ChannelMode > 0 {
		args = append(args, "-af", fmt.Sprintf("pan=1c|c0=c%v", s.ChannelMode-1))
	}
	soundEncoderMode <- s.EncoderMode > 0
	args = append(args, strings.Split("-ac 1 -ar 48000 -f f32le -", " ")...)
	ffdecode := exec.Command("ffmpeg", args...)
	ffdecodeOut, err := ffdecode.StdoutPipe()
	if err != nil {
		return err
	}
	err = ffdecode.Start()
	if err != nil {
		return err
	}
	go func() {
		hasLimit := s.TimeLimit > 0
		floatBuf := make([]float32, soundConfig.SamplesPerPacket)
		byteBuf := sliceconv.Float32ToByte(floatBuf)
		for !s.Stopping {
			read, err := ffdecodeOut.Read(byteBuf)
			if err != io.EOF && err != nil {
				fmt.Print(err)
				continue
			}
			newFloatBuf := make([]float32, read/4)
			copy(newFloatBuf, floatBuf)
			if hasLimit {
				s.TimeLimit -= len(newFloatBuf)
				if s.TimeLimit <= 0 {
					s.Stopping = true
				}
			}
		FOR:
			for {
				select {
				case <-time.After(time.Second):
					if s.Stopping {
						break FOR
					}
				case s.d <- newFloatBuf:
					break FOR
				}
			}
			if err == io.EOF {
				s.Stopping = true
			}
		}
		done := make(chan struct{}, 1)
		go func() {
			ffdecode.Wait()
			done <- struct{}{}
		}()
		select {
		case <-time.After(time.Second * 3):
			ffdecode.Process.Kill()
			<-done
		case <-done:
		}
	}()

	body, err := json.Marshal(JSON{
		"Name":     s.Name,
		"Wager":    s.Inturrupt,
		"Duration": s.Duration,
		"User":     s.Owner,
		"Channel":  s.Channel,
	})

	if err == nil {
		WSGlobalDispatch(&WSResponse{
			URI:    "/api/soundboard/play",
			Status: 200,
			Body:   string(body),
		})
	}

	body, err = json.Marshal(JSON{
		"Volume":  s.Amplitude(),
		"Channel": s.Channel,
	})
	if err == nil {
		WSGlobalDispatch(&WSResponse{
			URI:    "/api/soundboard/volume",
			Status: 200,
			Body:   string(body),
		})
	}
	return nil
}

//Data implements AudioSource
func (s *Sound) Data() chan []float32 {
	return s.d
}

//Stop implements AudioSource
func (s *Sound) Stop(force bool) bool {
	if s.Inturrupt > 0 && !force {
		s.Inturrupt--
		return false
	}
	s.Stopping = true
	return true
}

//IsPlaying implements AudioSource
func (s *Sound) IsPlaying() bool {
	return !s.Stopping
}

//Play plays the sound
func (s *Sound) Play() error {
	s.d = make(chan []float32, 1)
	err := SoundStart(s.Channel, s)
	if err != nil {
		return err
	}
	err = s.Start()
	return err
}

//Amplitude implements AudioSource
func (s *Sound) Amplitude() float32 {
	return s.Vol
}

//SetAmplitude implements AudioSource
func (s *Sound) SetAmplitude(v float32) {
	s.Vol = v
}

//SetAmplitude sets the volume of the playing clip
func SetAmplitude(channel int, v float32) {
	c, ok := currentSounds[channel]
	if ok {
		c.SetAmplitude(v)
	}
}

//Amplitude returns the amplification
func Amplitude(channel int) float32 {
	c, ok := currentSounds[channel]
	if ok {
		return c.Amplitude()
	}
	return -1
}

//ChannelIsPlaying returns true if the channel exists and is playing
func ChannelIsPlaying(channel int) bool {
	c, ok := currentSounds[channel]
	if !ok {
		return false
	}
	return c.IsPlaying()
}

//AudioSource specs a source to be played. Output 48khz float32 pcm data
type AudioSource interface {
	IsPlaying() bool
	Data() chan []float32
	Stop(force bool) bool
	Amplitude() float32
	SetAmplitude(float32)
}

func init() {
	currentSounds = make(map[int]AudioSource)
}

var currentSounds map[int]AudioSource

//SoundStop stops the sound playing on a channel
func SoundStop(channel int, force bool) bool {
	sound, ok := currentSounds[channel]
	if !ok {
		return true
	}
	ok = sound.Stop(force)
	return ok
}

//SoundStart starts a sound playing on a channel
func SoundStart(channel int, newSound AudioSource) error {
	if !SoundStop(channel, false) {
		return errors.New("Cannot stop playing sound.")
	}
	currentSounds[channel] = newSound
	return nil
}
func soundGetSample(id int, buf []float32) (float32, []float32) {
	src, ok := currentSounds[id]
	if !ok {
		return 0, buf
	}
	if len(buf) == 0 {
		select {
		case buf = <-src.Data():
		default:
			if !src.IsPlaying() {
				delete(currentSounds, id)
				go func() {
					str, _ := json.Marshal(JSON{
						"success": true,
						"Channel": id,
					})
					WSGlobalDispatch(&WSResponse{
						URI:    "/api/soundboard/stop",
						Status: 200,
						Body:   (string)(str),
					})
				}()
				return 0, buf
			}
			buf = make([]float32, 240) //50ms of samples
		}
		return soundGetSample(id, buf)
	}
	d := buf[0]
	buf = buf[1:]
	return d, buf
}

var soundConfig soundConfigT

type soundConfigT struct {
	SamplesPerSec    int
	TargetBitsPerSec int
	MSPerPacket      int

	TargetCodecBitrate int
	MaxBytesPerPacket  int
	SamplesPerPacket   int
}

func soundUpdateConfig() {
	//See mumble\AudioConfigDialog.cpp
	// 20 IP, 20 TCP, 4 crypt, 1 type/target, 2 (varint: sequence), 2 (varint, max0x1FFF, length)
	OverheadPerPacket := 20 + 20 + 4 + 1 + 2 + 2
	OverheadPerSecond := (OverheadPerPacket * 1000) / soundConfig.MSPerPacket
	soundConfig.TargetCodecBitrate = int(float64(soundConfig.TargetBitsPerSec-(8*OverheadPerSecond)) * .93)
	TargetBitsPerPacket := int(float64(soundConfig.TargetBitsPerSec) / (1000.0 / float64(soundConfig.MSPerPacket)))
	soundConfig.MaxBytesPerPacket = ((TargetBitsPerPacket / 8) - OverheadPerPacket)
	soundConfig.SamplesPerPacket = int(float64(soundConfig.SamplesPerSec) / (1000.0 / float64(soundConfig.MSPerPacket)))
}

var soundBufferCount int
var soundBitrate int
var soundBandwidth string

func init() {
	conf.Config(&soundBufferCount, "sound", `
# How many buffers of audio to keep. Increase if audio studders under load
buffer-count = 5
`)
	conf.Config(&soundBitrate, "sound", `
# Bitrate of audio to send. This should look like murmur's config
bitrate = 70_000
`)
	conf.Config(&soundBandwidth, "sound", `
# Bandwidth of the Opus encoder.
# Can be Full, Medium, Narrow, SuperWide, Wide, and Auto -bands
bandwidth = "fullband"
`)
}

//StartSound starts the sound module
func StartSound() {
	soundConfig = soundConfigT{
		SamplesPerSec:    48000,
		TargetBitsPerSec: soundBitrate,
		MSPerPacket:      60,
	}
	soundUpdateConfig()
	buf := make(chan []byte, soundBufferCount)
	// mux + encode
	go func() {
		encoder, err := opus.NewEncoder(soundConfig.SamplesPerSec, 1, opus.ApplicationVoip)
		if err != nil {
			panic(err)
		}
		var bandwidth opus.Bandwidth
		switch strings.ToLower(soundBandwidth) {
		case "fullband":
			bandwidth = opus.BandwidthFullband
		case "mediumband":
			bandwidth = opus.BandwidthMediumband
		case "narrowband":
			bandwidth = opus.BandwidthNarrowband
		case "superwideband":
			bandwidth = opus.BandwidthSuperWideband
		case "wideband":
			bandwidth = opus.BandwidthWideband
		case "autoband":
			bandwidth = opus.BandwidthAutoband
		default:
			fmt.Println("Unknown bandwidth; defaulting to auto")
			bandwidth = opus.BandwidthAutoband
		}
		encoder.SetBandwidth(bandwidth)
		encoder.SetBitrate(soundConfig.TargetCodecBitrate)
		muxedAudio := make([]float32, soundConfig.SamplesPerPacket)
		for {
			var mainBuf, backBuf, overBuf []float32
			var main, over, back float32
			allZero := true
			mainVol := Amplitude(SoundChannelMain)
			backVol := Amplitude(SoundChannelBackground)
			overVol := Amplitude(SoundChannelOverlay)
			for i := range muxedAudio {
				main, mainBuf = soundGetSample(SoundChannelMain, mainBuf)
				back, backBuf = soundGetSample(SoundChannelBackground, backBuf)
				over, overBuf = soundGetSample(SoundChannelOverlay, overBuf)
				if over != 0 {
					main /= 2
					back /= 2
				}
				if main != 0 {
					back /= 2
				}
				if allZero && (main != 0 || over != 0 || back != 0) {
					allZero = false
				}
				v := (main * mainVol) + (over * overVol) + (back * backVol)
				/*
					if v < -1 {
						v = -1
					}
					if v > 1 {
						v = 1
					}*/
				muxedAudio[i] = v
			}
			select {
			case useMusic := <-soundEncoderMode:
				if useMusic {
					encoder.SetApplication(opus.ApplicationAudio)
				} else {
					encoder.SetApplication(opus.ApplicationVoip)
				}
			default:
			}
			if allZero {
				buf <- nil
				time.Sleep(time.Duration(soundConfig.MSPerPacket) * time.Millisecond)
				continue
			}
			muxEncRaw, err := encoder.EncodeFloat32(muxedAudio, len(muxedAudio), soundConfig.MaxBytesPerPacket)
			if err != nil {
				fmt.Println(err)
			} else {
				buf <- muxEncRaw
			}
		}
	}()
	// send over network
	go func() {
		seq := 0
		tick := time.NewTicker(time.Duration(soundConfig.MSPerPacket) * time.Millisecond)
		for {
			var data []byte
			for data == nil {
				data = <-buf
			}
			Mumble.Conn.WriteAudio(4, 0, seq, data, nil, nil, nil)
			seq += (soundConfig.MSPerPacket / 10)
			<-tick.C
		}
	}()
}
