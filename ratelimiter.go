// Copyright 2017 Yumeko Contributors (as named in CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the LICENSE file

package main

import (
	"time"

	"bitbucket.org/abex/yumeko/conf"
)

//Limiter is a user based token bucket limiter
type Limiter struct {
	l     map[uint32]int
	burst int
	anti  int
}

//A Response is a limiter Response
type Response uint

const (
	//ResponseAllow signifies to allow the request
	ResponseAllow Response = iota
	//ResponseDeny signifies to disallow the request
	ResponseDeny
	//ResponseBan signifies to disallow the request and possibly take further action
	ResponseBan
)

//NewLimiterConf is a wrapper for NewLimiter, setup for config files
func NewLimiterConf(name, regenRate, burst, antiburst string) *Limiter {
	l := &Limiter{
		l:     make(map[uint32]int),
		burst: 0,
		anti:  0,
	}
	var iregenRate int
	conf.Config(&iregenRate, "ratelimiter."+name, `
# How often should a token be added to the bucket in milliseconds
regen-rate=`+regenRate+"\n")
	conf.Config(&l.burst, "ratelimiter."+name, `
# How many tokens should the bucket hold
burst = `+burst+"\n")
	conf.Config(&l.anti, "ratelimiter."+name, `
# How much 'in debt' can a limiter go before forcing action
# This typically means banning the user
antiburst=`+antiburst+"\n")
	go func() {
		for {
			l.Regen()
			time.Sleep(time.Duration(iregenRate) * time.Millisecond)
		}
	}()
	return l
}

//NewLimiter constructs a new limiter
func NewLimiter(regenRate time.Duration, burst int, antiburst int) *Limiter {
	l := &Limiter{
		l:     make(map[uint32]int),
		burst: burst,
		anti:  antiburst,
	}
	go func() {
		for {
			l.Regen()
			time.Sleep(regenRate)
		}
	}()
	return l
}

//Regen adds a token to all user's buckets
func (l *Limiter) Regen() {
	for i := range l.l {
		if l.l[i] >= l.burst {
			delete(l.l, i)
		} else {
			l.l[i]++
		}
	}
}

//Request returns if a user has a token in their bucket
func (l *Limiter) Request(user uint32) (r Response) {
	v, exists := l.l[user]
	if !exists {
		v = l.burst
	}
	if v <= -l.anti {
		return ResponseBan
	}
	v--
	if v <= 0 {
		r = ResponseDeny
	} else {
		r = ResponseAllow
	}
	l.l[user] = v
	return
}

//Return gives the user a token in their bucket, if you preemptivly took a token, but the operation failed
func (l *Limiter) Return(user uint32) {
	v, exists := l.l[user]
	if exists {
		l.l[user] = v + 1
	}
}
