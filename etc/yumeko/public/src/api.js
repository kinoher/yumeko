API = {
  WS:undefined,
  Call:function(url,data){
    API.DCall({
      url:url,
      data:data,
    })
  },
  DCall:function(obj){
    if(obj.ForceHTTP||!API.WS){
      obj.data=obj.data||{};
      if(window.Login&&Login.Hash){
        obj.data.key=Login.Hash;
      }
      pl.ajax({
        url:"/api/"+obj.url,
        type:"POST",
        data:obj.data,
        dataType:"json",
        error:function(err){
          if(obj.error)obj.error(err);
          Log.HTTPError(err);
        },
        success:function(data){
          if(obj.callback)obj.callback(data);
          if(API.Cbs[obj.url])API.Cbs[obj.url](data);
        }
      });
    }else{
      var np={};
      Object.keys(obj.data||{}).forEach(function(k){
        np[k]=[obj.data[k]];
      });
      var pack={
        URI:"/api/"+obj.url,
        params:np,
      };
      Log.WSEvent(pack);
      API.WS.send(
        JSON.stringify(pack));
    }
  },
  SetupWS:function(){
    if(!Login.Hash||Login.Hash.length<9) return;
    var uri = window.location.origin.replace("http","ws")+"/api/ws?key="+Login.Hash
    if(API.WS&&API.WS.readyState<=1&&API.WS.url==uri) return;
    API.CloseWS();
    var ws=new WebSocket(uri);
    API.WS=ws;
    ws.addEventListener("close",function(e){
      Log.WSClose(e);
      if(ws.readyState==3)setTimeout(API.SetupWS,5000);
    });
    ws.addEventListener("open",function(e){
      Log.WSOpen(e);
      Login.IsLoggedIn=true;
    });
    ws.addEventListener("error",function(e){
      Log.WSError(e);
    });
    ws.addEventListener("message",function(e){
      try {
        var res = JSON.parse(e.data);
        var uri = /\/api\/(.*)/.exec(res.URI)[1];
        var data = JSON.parse(res.Body);
        Log.WSEvent(e);
        if(API.Cbs[uri]){
          API.Cbs[uri](data);
        }else{
          Log.WSError("Unused api message: "+uri);
        }
      }catch(ex){
        Log.WSError(e.data);
        Log.WSError(ex);
      }
    })
  },
  CloseWS:function(){
    if(API.WS){
      API.WS.close();
      API.WS=undefined;
    }
  },
  Cbs:{},
};
window.addEventListener("beforeunload",function(){
  API.CloseWS();
});
