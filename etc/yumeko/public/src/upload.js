var Upload = {
  Upload:function(k,file,uri,success,progressCb,args){
    var fd = new FormData();
    fd.append(k,file,file.name);
    fd.append("key",Login.Hash);
    Object.keys(args||{}).forEach(function(e){
      fd.append(e,args[e]);
    });
    var req = new XMLHttpRequest();
    req.onreadystatechange=function(){
      if(req.readyState==4)success(req.responseText,req.status);
    };
    req.upload.addEventListener("progress",progressCb);
    req.open("POST","/api/"+uri);
    req.send(fd);
  },
  Event:function(fn,el){
    return function(ev,dt){
      dt=ev.dataTransfer||dt;
      ev.stopPropagation();
      ev.preventDefault();
      var plt = pl(el);
      if(ev.type=="dragover"){
        dt.dropEffect="copy";
        if(!plt.hasClass("dropLit")){
          plt.addClass("dropLit");
        }
        if(el.__Timeout)clearTimeout(el.__Timeout);
        el.__Timeout = setTimeout(function(){
          plt.removeClass("dropLit");
        },200);
      }else if(ev.type="drop"){
        plt.removeClass("dropLit");
        fn(dt.files[0]);
      }
    };
  },
  SizeFmt:function(v){
    var suf = ["B","KB","MB","GB"];
    var i=0;
    while(v>=1024){
      v/=1024;
      i++;
    }
    if(i>=suf.length) return "Too Much";
    return v.toLocaleString(undefined,{maximumFractionDigits:2})+suf[i];
  },
};
xtag.register("x-drop",{
  lifecycle:{
    created:function(){
      var that=this;
      this._eventListener=function(e){
        //console.log(e.dataTransfer);
        Upload.__GARBAGE=e.dataTransfer
        //if(that._disabled) return;
        if(e.target.tagName=="INPUT"&&e.target.type=="file") return;
        var fn = eval(that._bindData||"");
        if((typeof fn)!="function")return console.log("Invalid bind ", this);
        fn(e,e.dataTransfer);
      };
      this.addEventListener("dragover",this._eventListener);
      this.addEventListener("drop",this._eventListener);
    },
    removed:function(){
      this.removeEventListener("dragover",this._eventListener);
      this.removeEventListener("drop",this._eventListener);
    }
  },
  accessors:{
    bind:{
      attribute:{},
      get:function(){
        return this._bindData||"";
      },
      set:function(v){
        this._bindData=v;
      },
    },
    disabled:{
      attribute:{},
      get:function(){
        return this._disabled;
      },
      set:function(v){
        this._disabled=v;
        return v;
      }
    }
  },
});
