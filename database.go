// Copyright 2017 Yumeko Contributors (as named in CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the LICENSE file

package main

import (
	"fmt"

	"bitbucket.org/abex/yumeko/conf"

	mgo "gopkg.in/mgo.v2"
)

//MGOLogger implements mgo's logger
type MGOLogger uint8

//Output prints text to stdout
func (_ MGOLogger) Output(_ int, s string) error {
	fmt.Printf("[MGO] %s\n", s)
	return nil
}

//The global connection the mongod
var DbSession *mgo.Session

//The global Database
var Db *mgo.Database

var dbconnstr string

func init() {
	conf.Config(&dbconnstr, "database", `
# The address of the MongoDB server to conenct to
# This should include the usernname, password, and database to use
url = "mongodb://yumeko:yumekos_password@localhost:27017/yumeko"
`)
}

// StartDatabase Connects to the database
func StartDatabase() error {
	//mgo.SetLogger(MGOLogger(0))
	//mgo.SetDebug(true)
	DbSession, err := mgo.DialWithTimeout(dbconnstr, 0)
	if err != nil {
		return err
	}
	Db = DbSession.DB("yumeko")
	return nil
}
