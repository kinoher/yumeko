// Copyright 2017 Yumeko Contributors (as named in CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the LICENSE file

package main

import (
	"crypto/tls"
	"encoding/json"
	"fmt"
	"strings"
	"time"

	"bitbucket.org/abex/yumeko/conf"
	"bitbucket.org/abex/yumeko/glug"
	"bitbucket.org/abex/yumeko/gumble/gumble"
	"bitbucket.org/abex/yumeko/gumble/gumbleutil"
	_ "bitbucket.org/abex/yumeko/opus"
)

var mumbleUsername string
var mumbleAddress string
var mumbleTokens []string
var mumbleConnectAttempts uint
var mumbleCommentFile string
var mumbleCert string

func init() {
	conf.Config(&mumbleUsername, "mumble", `
# Username to request from the mumble server
username = "夢子"
`)
	conf.Config(func(us string, err error) {
		if err != nil {
			panic(err)
		}
		mumbleTokens = strings.Split(us, ",")
	}, "mumble", `
# Tokens to use to connect to mumble
tokens = "yumeko,othertoken"
`)
	conf.Config(&mumbleCert, "mumble", `
# Certificate used to authenticate yumeko
cert = "yumeko.pem"
`)
	conf.Config(&mumbleAddress, "mumble", `
# Address of the mumble server
address = "localhost:64738"
`)
	conf.Config(&mumbleConnectAttempts, "mumble", `
# How many times to retry connecting to the mumble server before failing
connect-attempts = 15
`)
}

var mumblelog = glug.NewLogger("mumble")

// The Mumble client singleton
var Mumble *gumble.Client

// FindUser returns a user by user id. Must be regestered and connected to the server
func FindUser(userID uint32) *gumble.User {
	if userID == gumble.UserUnregisteredID {
		return nil
	}
	if Mumble.State != gumble.StateSynced {
		return nil
	}
	for _, u := range Mumble.Users {
		if u.UserID == userID {
			return u
		}
	}
	return nil
}

//FindChannel finds a channel by name
func FindChannel(name string) *gumble.Channel {
	for _, cha := range Mumble.Channels {
		if cha.Name == name {
			return cha
		}
	}
	return nil
}

var commentstr = ""

func setComment() {
	if Mumble != nil && Mumble.Self != nil {
		Mumble.Self.SetComment(commentstr)
	}
}

var connectionAttempts uint

//ConnectToMumble connects to mumble. Can timeout
func ConnectToMumble() {
	if err := Mumble.Connect(); err != nil {
		fmt.Println("MumbleConnect: " + err.Error())
		connectionAttempts++
		if connectionAttempts > mumbleConnectAttempts {
			panic("Cannot reconnect")
		}
		time.Sleep(time.Second * 5)
		ConnectToMumble()
	} else {
		connectionAttempts = 0
	}
}

// StartGumble starts the mumble client. Must have conf
func StartGumble() {
	conf.OpenFileDefault("comment.txt", func(v string, err error) {
		if err != nil {
			mumblelog.Errorf("Error loading comment.txt: %v", err)
			return
		}
		commentstr = v
		setComment()
	}, `You can configure this in comment.txt<br>
It uses a limited subset of html.`)
	MumConf := gumble.Config{
		Username: mumbleUsername,
		Address:  mumbleAddress,
		Tokens:   mumbleTokens,
	}
	Mumble = gumble.NewClient(&MumConf)

	//TLS
	MumConf.TLSConfig.InsecureSkipVerify = true
	certPath := conf.Path(mumbleCert)
	cert, err := tls.LoadX509KeyPair(certPath, certPath)
	if err != nil {
		panic(err)
	}
	MumConf.TLSConfig.Certificates = append(MumConf.TLSConfig.Certificates, cert)

	//Mumble.Attach(gumbleutil.AutoBitrate)

	Mumble.Attach(gumbleutil.Listener{
		Connect: func(e *gumble.ConnectEvent) {
			setComment()
			if !Mumble.Self.IsRegistered() {
				Mumble.Self.Register()
			}
		},
		Disconnect: func(e *gumble.DisconnectEvent) {
			if e.Type.Has(gumble.DisconnectBanned) {

			} // superuser
			ConnectToMumble()
		},
		UserChange: func(e *gumble.UserChangeEvent) {
			if (e.User.UserID != Mumble.Self.UserID) && ((e.Type & (gumble.UserChangeConnected |
				gumble.UserChangeKicked |
				gumble.UserChangeBanned |
				gumble.UserChangeRegistered |
				gumble.UserChangeUnregistered |
				gumble.UserChangeDisconnected |
				gumble.UserChangeName)) != 0) {
				user := Load(e.User.UserID, false)
				if user != nil {
					user.SendFullUpdate(e.Type.Has(gumble.UserChangeDisconnected | gumble.UserChangeKicked | gumble.UserChangeBanned))
				}
			}
			if e.Type.Has(gumble.UserChangeName) && e.User.UserID == Mumble.Self.UserID {
				body, err := json.Marshal(JSON{
					"botname": e.User.Name,
				})
				if err == nil {
					WSGlobalDispatch(&WSResponse{
						URI:    "/api/config",
						Status: 200,
						Body:   string(body),
					})
				}
			}
		},
		PermissionDenied: func(e *gumble.PermissionDeniedEvent) {
			mumblelog.Weirdf("Server: Permission Denied: %#v", e)
		},
	})
	ConnectToMumble()
}
