// Copyright 2017 Yumeko Contributors (as named in CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the LICENSE file

package main

import (
	"fmt"
	"io"
	"net/http"
	"net/url"
	"time"

	"github.com/gorilla/websocket"
)

//WSRequest is a request sent over websocket
type WSRequest struct {
	URI    string
	Params map[string][]interface{}
}

//WSResponse is a response sent over websocket
type WSResponse struct {
	URI    string
	Body   string
	Status int
}

// WSUpgrader is the upgrader for websocket
var WSUpgrader = websocket.Upgrader{
	ReadBufferSize:  1500,
	WriteBufferSize: 1500,
}

//WSData holds data about each connection
type WSData struct {
	resSender chan *WSResponse
}

//WSSockets contans all of the currently open websockets mapped to their user
var WSSockets = map[*websocket.Conn]*WSData{}

//WSGlobalDispatch dispatches a message to all clients
func WSGlobalDispatch(res *WSResponse) {
	for _, c := range WSSockets {
		c.resSender <- res
	}
}

//SendWS sends a websocket message to a single user
func (u *User) SendWS(res *WSResponse) {
	for _, c := range u.Websockets {
		c.resSender <- res
	}
}

//WSResponseWriter implements http.ResponseWriter
type WSResponseWriter struct {
	status *int
	data   *[]byte
}

//WriteHeader sends the status code
func (r WSResponseWriter) WriteHeader(status int) {
	*r.status = status
}

//Write writes data
func (r WSResponseWriter) Write(d []byte) (int, error) {
	*r.data = append(*r.data, d...)
	return len(d), nil
}

//Header returns a header map
func (r WSResponseWriter) Header() http.Header {
	return http.Header(map[string][]string{})
}

//StartWebsocket starts the websocket module
func StartWebsocket() {
	Handle("/api/ws", func(w *Request) {
		user := w.GetUser()
		if user != nil {
			conn, err := WSUpgrader.Upgrade(w.W, w.R, nil)
			if err != nil {
				fmt.Println(err)
				return
			}
			icChan := make(chan *WSResponse, 3)
			wsd := &WSData{
				resSender: icChan,
			}
			WSSockets[conn] = wsd
			user.Websockets[conn] = wsd
			defer func() {
				close(WSSockets[conn].resSender)
				delete(WSSockets, conn)
				delete(user.Websockets, conn)
			}()
			go func() {
				defer func() {
					recover()
				}()
				for {
					var req WSRequest
					err := conn.ReadJSON(&req)
					if err != nil {
						if _, ok := err.(*websocket.CloseError); ok {
							break
						}
						fmt.Println(err)
						break
					}
					urlp, err := url.Parse(req.URI)
					if err != nil {
						break
					}
					if req.Params == nil {
						req.Params = map[string][]interface{}{}
					}
					req.Params["key"] = []interface{}{user.GetWebLogin()}
					params := make(map[string][]string, len(req.Params))
					for k, v := range req.Params {
						params[k] = make([]string, len(v))
						for k2, v2 := range v {
							params[k][k2] = fmt.Sprintf("%v", v2)
						}
					}
					status := 200
					var data []byte
					Mux.ServeHTTP(WSResponseWriter{
						status: &status,
						data:   &data,
					}, &http.Request{
						Method:     "WS",
						URL:        urlp,
						Proto:      "WS/1.0",
						ProtoMajor: 1,
						ProtoMinor: 0,
						Form:       url.Values(params),
						Host:       w.R.Host,
						RemoteAddr: w.RemoteAddr(),
					})
					msg := WSResponse{
						URI:    req.URI,
						Body:   string(data),
						Status: status,
					}
					icChan <- &msg
				}
			}()
			var tick = time.NewTicker(time.Second * 10)
			defer tick.Stop()
			for {
				select {
				case msg := <-icChan:
					err = conn.WriteJSON(msg)
				case <-tick.C:
					err = conn.WriteMessage(websocket.PingMessage, []byte{})
				}
				if err != nil {
					if err == websocket.ErrCloseSent || err == io.EOF {
						break
					}
					fmt.Println(err)
					break
				}
			}
		}
	})
}
