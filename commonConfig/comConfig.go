package commonConfig

import "bitbucket.org/abex/yumeko/conf"

var ExternalHostname string

func init() {
	conf.Config(&ExternalHostname, "HTTP", `
# The URI that can be used to connect to this service remotely
# This must have a trailing slash
external-address = "https://localhost/"
`)
}
