#!/bin/bash
set -e -x
[ -e "$INSTALL_DIR/lib/libpixman-1.a" ]&&exit 0
if ! [ -e pixman ]; then
	[ -e pixman.tar.gz ] || wget -Opixman.tar.gz https://www.cairographics.org/releases/pixman-0.34.0.tar.gz
	mkdir pixman
	tar -xf pixman.tar.gz -C pixman
	rm pixman.tar.gz
fi
pushd pixman/*
./configure "--prefix=$INSTALL_DIR" --disable-shared --disable-openmp
make $MAKEFLAGS
make install $MAKEFLAGS
popd
rm -rf pixman