#!/bin/bash
set -e -x
[ -e "$INSTALL_DIR/lib/libbz2.a" ]&&exit 0
if ! [ -e bzip2 ]; then
	[ -e bzip2.tar.gz ] || wget -Obzip2.tar.gz http://www.bzip.org/1.0.6/bzip2-1.0.6.tar.gz
	mkdir bzip2
	tar -xf bzip2.tar.gz -C bzip2
	rm bzip2.tar.gz
fi
pushd bzip2/*
make $MAKEFLAGS
make install $MAKEFLAGS "PREFIX=$INSTALL_DIR"
popd
rm -rf bzip2