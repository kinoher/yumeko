#!/bin/bash
set -e -x
[ -e "$INSTALL_DIR/lib/libfribidi.a" ]&&exit 0
[ -e fribidi ]||git clone --depth 1 https://github.com/fribidi/fribidi.git
pushd fribidi
./autogen.sh "--prefix=$INSTALL_DIR" --enable-static --disable-shared --disable-docs
#build is broken when parallel (a831b5a) (11/30/17)
make -j1
make install $MAKEFLAGS
popd
rm -rf fribidi