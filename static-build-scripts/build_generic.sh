#!/bin/bash
set -e -x
[ -e "$INSTALL_DIR/??" ]&&exit 0
[ -e ?? ]||git clone --branch ?? --depth 1 ??
pushd ??
./autogen.sh
./configure "--prefix=$INSTALL_DIR"
make $MAKEFLAGS
make install $MAKEFLAGS
popd
rm -rf ??