#!/bin/bash
set -e -x
[ -e "$INSTALL_DIR/lib/libopus.a" ]&&exit 0
git clone --branch v1.2.1 --depth 1 https://github.com/xiph/opus.git
pushd opus
./autogen.sh
./configure "--prefix=$INSTALL_DIR" --disable-shared --enable-static --disable-doc --disable-extra-programs
make $MAKEFLAGS
make install $MAKEFLAGS
popd
rm -rf opus