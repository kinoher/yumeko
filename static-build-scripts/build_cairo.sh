#!/bin/bash
set -e -x
[ -e "$INSTALL_DIR/lib/libcairo.a" ]&&exit 0
if ! [ -e cairo ]; then
	[ -e cairo.tar.gz ] || wget -Ocairo.tar.gz http://cairographics.org/snapshots/cairo-1.15.8.tar.xz
	mkdir cairo
	tar -xf cairo.tar.gz -C cairo
	rm cairo.tar.gz
fi
pushd cairo/*
./configure "--prefix=$INSTALL_DIR" --disable-shared --disable-xlib --disable-xcb --disable-qt --disable-quartz --disable-win32 --disable-skia --disable-os2 --disable-beos --disable-drm --disable-gallium --disable-gl --disable-glesv2 --disable-cogl --disable-directfb --disable-vg --disable-egl --disable-glx --disable-wgl --disable-ft --disable-fc --disable-pdf --disable-svg --disable-gobject --disable-trace --disable-interpreter
make $MAKEFLAGS
make install $MAKEFLAGS
popd
rm -rf cairo