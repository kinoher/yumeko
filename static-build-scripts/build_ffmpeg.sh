#!/bin/bash
set -e -x
[ -e "$INSTALL_DIR/bin/ffmpeg" ]&&exit 0
if ! [ -e FFmpeg ]; then
  git clone --branch release/3.4 --depth 1 https://github.com/FFmpeg/FFmpeg.git
fi
pushd FFmpeg
./configure \
  "--prefix=$INSTALL_DIR" \
  --pkg-config-flags="--static" \
  --extra-cflags="-I$INSTALL_DIR/include" \
  --extra-ldflags="-L$INSTALL_DIR/lib" \
  --extra-libs="-lpthread -lm" \
  --enable-gpl \
  --enable-libfdk_aac \
  --enable-libfreetype \
  --enable-libmp3lame \
  --enable-libopus \
  --enable-libvorbis \
  --enable-libvpx \
  --enable-libx264 \
  --enable-libx265 \
  --enable-nonfree \
  --enable-libass \
  --enable-libgme
make $MAKEFLAGS
make install $MAKEFLAGS
popd
rm -rf FFmpeg