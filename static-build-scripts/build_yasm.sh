#!/bin/bash
set -e -x
[ -e "$INSTALL_DIR/bin/yasm" ]&&exit 0
[ -e yasm ]||git clone --branch v1.3.0 --depth 1 https://github.com/yasm/yasm.git
pushd yasm
./autogen.sh
./configure "--prefix=$INSTALL_DIR"
make $MAKEFLAGS
make install $MAKEFLAGS
popd
rm -rf yasm