#!/bin/bash
set -e -x
[ -e "$INSTALL_DIR/lib/libgme.a" ]&&exit 0
[ -e "libgme" ]||git clone --depth 1 https://Abex@bitbucket.org/mpyne/game-music-emu.git libgme
pushd libgme
cmake -G "Unix Makefiles" "-DCMAKE_INSTALL_PREFIX=$INSTALL_DIR" -DBUILD_SHARED_LIBS=OFF -DENABLE_UBSAN=OFF
make $MAKEFLAGS
make install $MAKEFLAGS
popd
rm -rf libgme