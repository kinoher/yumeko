#!/bin/bash
set -e -x
export INSTALL_DIR=$(pwd)/deps
export MAKEFLAGS=-j8
mkdir $INSTALL_DIR||true
export PATH="$INSTALL_DIR/bin:$PATH"
export PKG_CONFIG_PATH="$INSTALL_DIR/lib/pkgconfig"

./build_nasm.sh
./build_yasm.sh
./build_ragel.sh
hash -r

./build_zlib.sh
./build_bzip2.sh

./build_opus.sh
./build_libjpeg_turbo.sh

./build_png.sh
./build_pixman.sh
./build_fribidi.sh
./build_cairo.sh
./build_freetype.sh
# harfbuzz is build in build_freebuzz.sh due to circular dependancies
./build_libass.sh
./build_libogg.sh
./build_vorbis.sh
./build_x264.sh
./build_x265.sh
./build_libfdk_aac.sh
./build_libmp3lame.sh
./build_libvpx.sh
./build_libgme.sh
./build_ffmpeg.sh

# Pure yumeko deps
source ./get_go.sh

# Build yumeko

if [ -z "$GOPATH" ] && ! [ -e "$GOPATH" ]; then
	GOPATH=$(pwd)/gopath
	mkdir $GOPATH||true
fi
if ! [ -e "$GOPATH/src/bitbucket.org/abex/yumeko" ]; then
	go get -d  bitbucket.org/abex/yumeko
fi
rm -rf bin||true
mkdir bin
go build -i -o $INSTALL_DIR/bin/yumeko-version-arguments -v -x bitbucket.org/abex/yumeko/versioning/yumeko-version-arguments
gobuild(){
	go install -v -x -ldflags "$(yumeko-version-arguments $2)" $2
	cp $GOPATH/bin/$1 bin/$1
}
CGO_LDFLAGS="-lopus -lm" gobuild yumeko bitbucket.org/abex/yumeko
gobuild thumblink bitbucket.org/abex/yumeko/thumblink/thumblink
gobuild ymup bitbucket.org/abex/yumeko/ymup
go install -v -x bitbucket.org/abex/yumeko/subhub
cp $GOPATH/bin/subhub bin/subhub
cp "$INSTALL_DIR/bin/ffmpeg" bin
rm -rf gopath||true
echo "Build complete. thumblink and ffmpeg are nonfree and unredistributable."