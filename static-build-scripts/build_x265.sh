#!/bin/bash
set -e -x
[ -e "$INSTALL_DIR/lib/libx265.a" ]&&exit 0
[ -e x265 ]||hg clone https://bitbucket.org/multicoreware/x265
pushd x265/build/linux
cmake -G "Unix Makefiles" "-DCMAKE_INSTALL_PREFIX=$INSTALL_DIR" -DENABLE_SHARED:bool=off ../../source
make $MAKEFLAGS
make install $MAKEFLAGS
popd
rm -rf x265