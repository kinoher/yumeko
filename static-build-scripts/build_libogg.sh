#!/bin/bash
set -e -x
[ -e "$INSTALL_DIR/lib/libogg.a" ]&&exit 0
[ -e ogg ]||git clone --depth 1 https://github.com/xiph/ogg.git
pushd ogg
./autogen.sh
./configure "--prefix=$INSTALL_DIR" --disable-shared
make $MAKEFLAGS
make install $MAKEFLAGS
popd
rm -rf ogg