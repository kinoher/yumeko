#!/bin/bash
set -e -x
[ -e "$INSTALL_DIR/lib/libjpeg.a" ]&&exit 0
[ -e libjpeg-turbo ]||git clone --branch 1.5.2 --depth 1 https://github.com/libjpeg-turbo/libjpeg-turbo.git
pushd libjpeg-turbo
autoreconf -fiv
./configure "--prefix=$INSTALL_DIR" --disable-shared
make $MAKEFLAGS
make install $MAKEFLAGS
popd
rm -rf libjpeg-turbo