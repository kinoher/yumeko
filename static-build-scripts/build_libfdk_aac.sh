#!/bin/bash
set -e -x
[ -e "$INSTALL_DIR/lib/libfdk-aac.a" ]&&exit 0
[ -e fdk-aac ]||git clone --depth 1 https://github.com/mstorsjo/fdk-aac
pushd fdk-aac
autoreconf -fiv
./configure "--prefix=$INSTALL_DIR" --disable-shared
make $MAKEFLAGS
make install $MAKEFLAGS
popd
rm -rf fdk-aac