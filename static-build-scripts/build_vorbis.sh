#!/bin/bash
set -e -x
[ -e "$INSTALL_DIR/lib/libvorbis.a" ]&&exit 0
[ -e vorbis ]||git clone --branch v1.3.5 --depth 1 https://github.com/xiph/vorbis.git
pushd vorbis
./autogen.sh
./configure "--prefix=$INSTALL_DIR" --with-ogg="$INSTALL_DIR" --disable-shared
make $MAKEFLAGS
make install $MAKEFLAGS
popd
rm -rf vorbis