#!/bin/bash
set -e -x
[ -e "$INSTALL_DIR/lib/libz.a" ]&&exit 0
if ! [ -e zlib ]; then
	[ -e zlib.tar.gz ] || wget -Ozlib.tar.gz https://zlib.net/zlib-1.2.11.tar.gz
	mkdir zlib
	tar -xf zlib.tar.gz -C zlib
	rm zlib.tar.gz
fi
pushd zlib/*
./configure "--prefix=$INSTALL_DIR" --static
make $MAKEFLAGS
make install $MAKEFLAGS
popd
rm -rf zlib