#!/bin/bash
set -e -x
[ -e "$INSTALL_DIR/lib/libpng.a" ]&&exit 0
if ! [ -e png ]; then
	[ -e png.tar.gz ] || wget -Opng.tar.gz ftp://ftp-osl.osuosl.org/pub/libpng/src/libpng16/libpng-1.6.34.tar.gz
	mkdir png
	tar -xf png.tar.gz -C png
	rm png.tar.gz
fi
pushd png/*
./configure "--prefix=$INSTALL_DIR" --disable-shared
make $MAKEFLAGS
make install $MAKEFLAGS
popd
rm -rf png