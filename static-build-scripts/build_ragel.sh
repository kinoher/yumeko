#!/bin/bash
set -e -x
[ -e "$INSTALL_DIR/bin/ragel" ]&&exit 0
if ! [ -e ragel ]; then
	[ -e ragel.tar.gz ] || wget -Oragel.tar.gz http://www.colm.net/files/ragel/ragel-6.10.tar.gz
	mkdir ragel
	tar -xf ragel.tar.gz -C ragel
	rm ragel.tar.gz
fi
pushd ragel/*
./configure "--prefix=$INSTALL_DIR"
make $MAKEFLAGS
make install $MAKEFLAGS "PREFIX=$INSTALL_DIR"
popd
rm -rf ragel