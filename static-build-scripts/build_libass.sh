#!/bin/bash
set -e -x
[ -e "$INSTALL_DIR/lib/libass.a" ]&&exit 0
[ -e libass ]||git clone --branch 0.14.0 --depth 1 https://github.com/libass/libass.git
pushd libass
./autogen.sh
./configure "--prefix=$INSTALL_DIR" --disable-shared --disable-coretext --disable-directwrite --disable-fontconfig --disable-require-system-font-provider
make $MAKEFLAGS
make install $MAKEFLAGS
popd
rm -rf libass