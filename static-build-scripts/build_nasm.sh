#!/bin/bash
set -e -x
[ -e "$INSTALL_DIR/bin/nasm" ]&&exit 0
[ -e nasm ]||git clone --branch nasm-2.13.02 --depth 1 http://repo.or.cz/nasm.git
pushd nasm
./autogen.sh
./configure "--prefix=$INSTALL_DIR"
make $MAKEFLAGS
#make fails on (non disable-able) documentation
make install $MAKEFLAGS || nasm -v
popd
rm -rf nasm