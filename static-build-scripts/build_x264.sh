#!/bin/bash
set -e -x
[ -e "$INSTALL_DIR/lib/libx264.a" ]&&exit 0
[ -e x264 ]||git clone --depth 1 http://git.videolan.org/git/x264
pushd x264
./configure "--prefix=$INSTALL_DIR" --enable-static
make $MAKEFLAGS
make install $MAKEFLAGS
popd
rm -rf x264