#!/bin/bash
set -e -x
[ -e "$INSTALL_DIR/lib/libharfbuzz.a" ]&&exit 0
if ! [ -e harfbuzz ]; then
	[ -e harfbuzz.tar.gz ] || wget -Oharfbuzz.tar.gz https://www.freedesktop.org/software/harfbuzz/release/harfbuzz-1.7.1.tar.bz2
	mkdir harfbuzz
	tar -xf harfbuzz.tar.gz -C harfbuzz
	rm harfbuzz.tar.gz
fi
pushd harfbuzz/*
./configure "--prefix=$INSTALL_DIR" --disable-shared --enable-static --without-gobject --without-fontconfig --without-icu --without-graphite2 --with-freetype --without-uniscribe --without-directwrite
make $MAKEFLAGS
make install $MAKEFLAGS "PREFIX=$INSTALL_DIR"
popd
rm -rf harfbuzz