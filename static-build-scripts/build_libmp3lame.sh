#!/bin/bash
set -e -x
[ -e "$INSTALL_DIR/lib/libmp3lame.a" ]&&exit 0
if ! [ -e lame ]; then
	[ -e lame.tar.gz ] || wget -Olame.tar.gz http://downloads.sourceforge.net/project/lame/lame/3.100/lame-3.100.tar.gz
	mkdir lame
	tar -xf lame.tar.gz -C lame
	rm lame.tar.gz
fi
pushd lame/*
./configure "--prefix=$INSTALL_DIR" --disable-shared --enable-nasm
make $MAKEFLAGS
make install $MAKEFLAGS
popd
rm -rf lame