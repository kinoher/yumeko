#!/bin/bash
set -e -x
[ -e "$INSTALL_DIR/lib/libvpx.a" ]&&exit 0
[ -e libvpx ]||git clone --depth 1 https://chromium.googlesource.com/webm/libvpx.git
pushd libvpx
./configure "--prefix=$INSTALL_DIR" --disable-examples --disable-unit-tests --enable-vp9-highbitdepth --as=yasm
make $MAKEFLAGS
make install $MAKEFLAGS
popd
rm -rf libvpx