#!/bin/bash
set -e -x
HARFBUZZ_CHECK="$INSTALL_DIR/lib/freetype_has_harfbuzz"
[ -e "$HARFBUZZ_CHECK" ]&&[ -e "$INSTALL_DIR/lib/libfreetype.a" ]&&exit 0
if ! [ -e freetype ]; then
	[ -e freetype.tar.gz ] || wget -Ofreetype.tar.gz https://download.savannah.gnu.org/releases/freetype/freetype-2.8.1.tar.gz
	mkdir freetype
	tar -xf freetype.tar.gz -C freetype
	rm freetype.tar.gz
fi
if ! [ -e "$INSTALL_DIR/lib/libfreetype.a" ]; then
	pushd freetype/*
	./configure "--prefix=$INSTALL_DIR" --disable-shared --with-zlib=yes --with-bzip2=no --with-png=yes --with-harfbuzz=no
	make $MAKEFLAGS
	make install $MAKEFLAGS
	popd
fi
./build_harfbuzz.sh
pushd freetype/*
./configure "--prefix=$INSTALL_DIR" --disable-shared --with-zlib=yes --with-bzip2=no --with-png=yes --with-harfbuzz=yes
make $MAKEFLAGS
make install $MAKEFLAGS
popd
touch "$HARFBUZZ_CHECK"
rm -rf freetype