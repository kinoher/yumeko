// Copyright 2017 Yumeko Contributors (as named in CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the LICENSE file

package main

import (
	"bufio"
	"encoding/binary"
	"errors"
	"fmt"
	"os/exec"
	"strconv"
	"strings"
	"time"

	"bitbucket.org/abex/yumeko/conf"
	"bitbucket.org/abex/yumeko/gumble/gumble"
	"bitbucket.org/abex/yumeko/gumble/gumbleutil"
	"bitbucket.org/abex/yumeko/ogg"
	"bitbucket.org/abex/yumeko/opus"
)

//RecorderAudio holds a packet of user audio
type RecorderAudio struct {
	Time   time.Time
	Buffer []byte
	Older  *RecorderAudio
	Newer  *RecorderAudio
}

//RecorderAudioBuffer holds a amount of user audio
type RecorderAudioBuffer struct {
	Newest *RecorderAudio
	Oldest *RecorderAudio
}

//Push a new audio onto the buffer
func (r *RecorderAudioBuffer) Push(a RecorderAudio) {
	a.Older = r.Newest
	if a.Older != nil {
		a.Older.Newer = &a
	}
	r.Newest = &a
	if r.Oldest == nil {
		r.Oldest = &a
		return
	}
}

//PopAfter pops all records after time t
func (r *RecorderAudioBuffer) PopAfter(t time.Time) {
	oldest := r.Oldest
	for oldest.Time.Before(t) {
		if oldest.Newer == nil {
			break
		}
		oldest.Older = nil //help:gc
		oldest = oldest.Newer
		oldest.Older.Newer = nil //help:gc
		oldest.Older = nil
	}
	r.Oldest = oldest
}

//RecorderAudios holds all of the user audio
var RecorderAudios map[uint32]*RecorderAudioBuffer

var recorderKeep int
var recorderDelOnDC bool

func init() {
	conf.Config(&recorderKeep, "recorder", `
# How many seconds of user audio to keep per user.
timeout = 45
`)
	conf.Config(&recorderDelOnDC, "recorder", `
# Delete recorded audio when the user disconnects
delete-on-disconnect = true
`)
}

//StartRecorder starts the Temporary recorder
func StartRecorder() {
	Mumble.AttachRawAudio(func(p *gumble.RawAudioPacket) {
		b, ok := RecorderAudios[p.Sender.UserID]
		if !ok {
			b = &RecorderAudioBuffer{}
			RecorderAudios[p.Sender.UserID] = b
		}
		buf := make([]byte, len(p.Buffer))
		copy(buf, p.Buffer)
		b.Push(RecorderAudio{
			Time:   time.Now(),
			Buffer: buf,
		})
		b.PopAfter(time.Now().Add(-time.Duration(recorderKeep) * time.Second))
	})

	Mumble.Attach(gumbleutil.Listener{
		UserChange: func(e *gumble.UserChangeEvent) {
			if recorderDelOnDC && e.Type.Has(gumble.UserChangeDisconnected) {
				delete(RecorderAudios, e.User.UserID)
			}
		},
	})
	Handle("/api/downloadUserAudio", func(w *Request) {
		if w.IsWS() {
			return
		}
		user := w.GetUser()
		if user != nil {
			err := user.Limit(ServeRateLimit)
			if err != nil {
				w.Error(429, err)
			}
			if !user.Has(PermissionDownloadUserAudio) {
				w.Error(401, errors.New("Missing Permission"))
				return
			}
			targets := w.R.FormValue("target")
			targeti64, err := strconv.ParseUint(targets, 10, 32)
			if err != nil {
				w.Error(400, err)
				return
			}
			targeti := uint32(targeti64)
			cb, ok := RecorderAudios[targeti]
			if !ok || cb == nil {
				w.Error(400, errors.New("User does not have audio"))
				return
			}
			target := Load(targeti, false)
			if target == nil {
				w.Error(400, errors.New("User cannot be found"))
				return
			}
			makeOpus := ParseBool(w.R.FormValue("opus"), false)
			datestr := time.Now().Format("Jan-02-2006__15-04-05")
			extension := ".mp3"
			if makeOpus {
				extension = ".opus"
			}
			filename := "AudioClip-" + strings.Replace(strings.Replace(target.Name, "\\", "", -1), "\"", "", -1) + "-at-" + datestr + extension
			w.W.Header().Set("Content-Disposition", "attachment; filename=\""+filename+"\"")
			if makeOpus {
				w.W.Header().Set("Content-Type", "audio/ogg; codecs=opus")
				//Opus headers
				os := ogg.NewOggStream(w.W)
				err := os.WritePacket(false, 0, []byte{
					'O', 'p', 'u', 's',
					'H', 'e', 'a', 'd',
					1, 1, 0, 0, //version 1; 1 channel; no pre-skip
					0x80, 0xBB, 0, 0, //48000 Hz
					0, 0, 0, // 0dB Gain, 1 stream
				})
				if err != nil {
					fmt.Println(err)
					return
				}
				vendorString := "Yumeko+libopus"
				tags := make([]byte, 8+4+len(vendorString)+4)
				copy(tags, []byte{
					'O', 'p', 'u', 's',
					'T', 'a', 'g', 's',
				})
				binary.LittleEndian.PutUint32(tags[8:], uint32(len(vendorString)))
				copy(tags[12:], ([]byte)(vendorString))
				//remaining 4 is tag length, always 0 in our case
				err = os.WritePacket(false, 0, tags)
				if err != nil {
					fmt.Println(err)
					return
				}
				c := cb.Oldest
				//get samples/packet. Assume its the same for all packets
				var spp uint64
				{
					dec, err := opus.NewDecoder(48000, 1)
					if err != nil {
						w.Error(500, err)
						return
					}
					decoded, err := dec.DecodeInt16(c.Buffer, 2880, false)
					if err != nil {
						w.Error(500, err)
						return
					}
					spp = uint64(len(decoded))
				}
				i := uint64(1)
				for c != nil {
					buf := c.Buffer
					c = c.Newer
					err = os.WritePacket(c == nil, i*spp, buf)
					i++
				}
			} else {
				w.W.Header().Set("Content-Type", "audio/mp3")
				cmd := exec.Command("ffmpeg", strings.Split("-ar 48000 -ac 1 -f s16le -i pipe: -f mp3 pipe:", " ")...)
				inputPipe, _ := cmd.StdinPipe()
				outputPipe, _ := cmd.StdoutPipe()
				go func() {
					c := cb.Oldest
					dec, err := opus.NewDecoder(48000, 1)
					if err != nil {
						w.Error(500, err)
						return
					}
					for c != nil {
						b, err := dec.DecodeInt16(c.Buffer, 2880, false)
						if err != nil {
							fmt.Println(err)
						}
						binary.Write(inputPipe, binary.LittleEndian, b)
						c = c.Newer
					}
					inputPipe.Close()
				}()
				go cmd.Run()
				bufio.NewReader(outputPipe).WriteTo(w.W)
				outputPipe.Close()
			}
		}
	})
}

func init() {
	RecorderAudios = make(map[uint32]*RecorderAudioBuffer)
}
