package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
	"time"
)

var arg = `-X "bitbucket.org/abex/yumeko/versioning.%v=%v" `
var matcher = regexp.MustCompile(`(.*) ([0-9]+)\.([0-9]+)\.([0-9]+)`)

func main() {
	if len(os.Args) != 2 {
		panic("Usage: yumeko-version-arguments [Package import]")
	}
	base := filepath.Join(os.Getenv("GOPATH"), "src", os.Args[1])
	vus, err := ioutil.ReadFile(filepath.Join(base, "VERSION"))
	if err != nil {
		panic(err)
	}
	cmd := exec.Command("git", "rev-parse", "HEAD")
	cmd.Dir = base
	commit, err := cmd.Output()
	if err != nil {
		panic(err)
	}
	if commit[len(commit)-1] == '\n' {
		commit = commit[:len(commit)-1]
	}
	if commit[len(commit)-1] == '\r' {
		commit = commit[:len(commit)-1]
	}
	match := matcher.FindStringSubmatch(string(vus))
	fmt.Printf(arg, "PackageName", match[1])
	fmt.Printf(arg, "sMajor", match[2])
	fmt.Printf(arg, "sMinor", match[3])
	fmt.Printf(arg, "sPatch", match[4])
	fmt.Printf(arg, "Date", time.Now().Format(time.RFC1123))
	fmt.Printf(arg, "Commit", string(commit))
}
