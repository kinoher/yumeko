package versioning

import (
	"fmt"
	"net/http"
	"strconv"
)

func mustAtoi(v string) int {
	i, _ := strconv.Atoi(v)
	return i
}

var PackageName = "Package"
var sMajor, sMinor, sPatch string
var Major = mustAtoi(sMajor)
var Minor = mustAtoi(sMinor)
var Patch = mustAtoi(sPatch)
var Commit = "Development"
var Date string

func ShortVersion() string {
	return fmt.Sprintf("%v %v.%v.%v", PackageName, Major, Minor, Patch)
}

func LongVersion() string {
	return fmt.Sprintf("%v %v.%v.%v (%v) @ %v", PackageName, Major, Minor, Patch, Commit, Date)
}

func UserAgent() string {
	return fmt.Sprintf("%v/%v.%v (+https://bitbucket.org/abex/yumeko)", PackageName, Major, Minor)
}

func SetUserAgent(req *http.Request) {
	req.Header.Set("User-Agent", UserAgent())
}
