// Copyright 2017 Yumeko Contributors (as named in CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the LICENSE file

package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"hash/crc32"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
	"strings"

	"bitbucket.org/abex/yumeko/conf"
	"bitbucket.org/abex/yumeko/glug"
	//"net/http/pprof"
)

// Global HTTP mountpoint
var Mux *http.ServeMux

func init() {
	Mux = http.NewServeMux()
}

//The Global Ratelimiter. Limit for all incoming http requests
var HTTPLimiter = NewLimiterConf("HTTP", "100", "200", "500")

var httpHostname string
var httpRealIP string
var httpPort int
var httpsPort int
var httpsCert string
var httpsKey string
var grecaptchaSecret string

func init() {
	conf.Config(&httpHostname, "HTTP", `
# Address to listen on for HTTP and HTTPS
# A empty value means all addresses
address = ""
`)
	conf.Config(&httpPort, "HTTP", `
# Port to listen on for HTTP trafic
port = 80
`)
	conf.Config(&httpRealIP, "HTTP", `
# Header to look for for the client's actual IP Address
# Set to "" to use the connection's ip
real-ip = ""
`)
	conf.Config(&httpsPort, "HTTP.secure", `
# Port to listen on for HTTPS traffic
# If set cert and key must be valid aswell
port = 0
`)
	conf.Config(&httpsCert, "HTTP.secure", `
# Certificate to use for incoming HTTPS connections
cert = ""
`)
	conf.Config(&httpsKey, "HTTP.secure", `
# Key to use for incoming HTTPS connections
key = ""
`)
	conf.Config(&grecaptchaSecret, "keys.grecaptcha", `
# Google ReCAPTCHA api secret
secret = ""
`)
}

var httplog = glug.NewLogger("HTTP")

// StartHTTP starts listening on http and https. Configs must be loaded
func StartHTTP() {

	//HTTP/1.x is easy, Just hook the L&S to Mux
	go func() {
		err := http.ListenAndServe(fmt.Sprintf("%v:%v", httpHostname, httpPort), Mux)
		if err != nil {
			httplog.Error("Error starting HTTP server", err)
		}
	}()

	if httpsPort != 0 {
		go func() {
			err := http.ListenAndServeTLS(fmt.Sprintf("%v:%v", httpHostname, httpsPort), conf.Path(httpsCert), conf.Path(httpsKey), Mux)
			if err != nil {
				httplog.Error("Error starting HTTPS server", err)
			}
		}()
	}

	/*
		Mux.Handle("/debug/pprof/", http.HandlerFunc(pprof.Index))
		Mux.Handle("/debug/pprof/cmdline", http.HandlerFunc(pprof.Cmdline))
		Mux.Handle("/debug/pprof/profile", http.HandlerFunc(pprof.Profile))
		Mux.Handle("/debug/pprof/symbol", http.HandlerFunc(pprof.Symbol))
	*/
}

// ParseBool trys to parse s as a boolean
func ParseBool(s string, defalt bool) bool {
	s = strings.ToLower(s)
	switch s {
	case "1", "t", "tr", "tru", "true", "on", "yes", "ok":
		return true
	case "0", "f", "fa", "fal", "fals", "false", "off", "of", "no":
		return false
	default:
		return defalt
	}
}

//A RequestHandler represents HTTP Request Callback
type RequestHandler func(*Request)

//A Request represents HTTP Request data
type Request struct {
	W http.ResponseWriter
	R *http.Request
}

//Handle enrolls a HandleFunc into the primary Mux
func Handle(path string, h RequestHandler) {
	Mux.HandleFunc(path, func(ow http.ResponseWriter, or *http.Request) {
		wnp := Request{
			W: ow,
			R: or,
		}
		w := &wnp
		if w.Limit(HTTPLimiter) {
			return
		}
		h(w)
	})
}

//A JSON object
type JSON map[string]interface{}

//GetUser returns the user who send the request. Sends error on failure

func (w *Request) Error(status int, err error) {
	w.WriteJSON(status, JSON{
		"error": err.Error(),
	})
}

//IsWS returns true if the connection is from websocket
func (w *Request) IsWS() bool {
	return w.R.Proto == "WS/1.0"
}

//DispatchJSON dispatches a WS callback for all suers and writes the http response
func (w *Request) DispatchJSON(status int, uri string, d interface{}) error {
	w.W.Header().Set("content-type", "application/json")
	str, err := json.Marshal(d)
	if err != nil {
		fmt.Println(err)
		w.Error(500, err)
		return err
	}
	go func() {
		wres := &WSResponse{
			URI:    uri,
			Body:   (string)(str),
			Status: status,
		}
		for _, sock := range WSSockets {
			sock.resSender <- wres
		}
	}()
	w.W.WriteHeader(status)
	w.W.Write(str)
	return nil
}

//RemoteAddr returns the remote ip, taking into account reverse proxying
func (w *Request) RemoteAddr() string {
	if !w.IsWS() && httpRealIP != "" {
		nr := w.R.Header.Get(httpRealIP)
		if nr != "" {
			return nr
		}
	}
	return w.R.RemoteAddr
}

//HostToUint32 converts a host to a uint32 by crc32 (for ipv6)
func HostToUint32(host string) (uint32, error) {
	h := crc32.NewIEEE()
	h.Write([]byte(host))
	return h.Sum32(), nil
}

//Limit requests a token from the limiter and applys it to the Host
func (w *Request) Limit(l *Limiter) bool {
	i, err := HostToUint32(w.RemoteAddr())
	if err != nil {
		return true
	}
	r := l.Request(i)
	if r == ResponseAllow {
		return false
	}
	if r == ResponseBan {
		w.W.WriteHeader(429)
		return true
	}
	w.Error(429, errors.New("Rate limit exceeded"))
	return true
}

//GetUserByKey ruturns a user, or nil from a login token
func GetUserByKey(key string) (*User, error) {
	su, ok := ServiceUsers[key]
	if ok {
		return su, nil
	}
	if len(key) < 9 {
		return nil, errors.New("Bad Key")
	}
	runes := []rune(key)
	runeUserID := runes[0 : len(runes)-8]
	userID64, err := strconv.ParseUint(string(runeUserID), 16, 32)
	userID := uint32(userID64)
	if err != nil {
		return nil, errors.New("Bad Key")
	}
	user := Load(userID, false)
	if user == nil {
		return nil, errors.New("Bad Key")
	}
	if user.Token() == strings.ToUpper(string(runes[len(runes)-8:len(runes)])) {
		return user, nil
	}
	return nil, errors.New("Bad Key")
}

//GetUser gets a user from a Request
func (w *Request) GetUser() *User {
	w.R.ParseForm()
	key := w.R.Form.Get("key")
	user, err := GetUserByKey(key)
	if err != nil {
		w.Error(401, err)
		return nil
	}
	return user
}

//Write writes data to the underlying ResponseWriter
func (w *Request) Write(str string) (int, error) {
	return w.W.Write([]byte(str))
}

//WriteJSON writes with JSON. Sends error on failure
func (w *Request) WriteJSON(status int, d interface{}) error {
	w.W.Header().Set("content-type", "application/json")
	str, err := json.Marshal(d)
	if err != nil {
		fmt.Println(err)
		w.Error(500, err)
		return err
	}
	w.W.WriteHeader(status)
	w.W.Write(str)
	return nil
}

// R Returns a value from a JSON object. Fails silently
func R(rv interface{}, key string) (ret interface{}) {
	v, ok := (rv.(map[string]interface{}))
	if !ok {
		return
	}
	val, exists := v[key]
	if !exists {
		return
	}
	ret = val
	return
}

// RI Returns a value from a JSON array. Fails silently
func RI(rv interface{}, key int) (ret interface{}) {
	v, ok := (rv.([]interface{}))
	if !ok {
		return
	}
	if len(v) <= key {
		return
	}
	ret = v[key]
	return
}

//SanetizeFile Removes any ..s in a file
func SanetizeFile(file string) string {
	return strings.Replace(file, "..", "", -1)
}

// ValidateCaptcha validates a captcha token
func ValidateCaptcha(token string, remoteIP string) bool {
	v := url.Values{}
	v.Set("secret", grecaptchaSecret)
	v.Set("response", token)
	if remoteIP != "" {
		v.Set("remoteip", remoteIP)
	}
	r, err := http.PostForm("https://www.google.com/recaptcha/api/siteverify", v)
	if err != nil {
		fmt.Println(err)
		return false
	}
	defer r.Body.Close()
	bodystr, err := ioutil.ReadAll(r.Body)
	if err != nil {
		fmt.Println(err)
		return false
	}
	var d map[string]interface{}
	json.Unmarshal(bodystr, &d)
	success, ok := d["success"].(bool)
	if !ok {
		success = false
	}
	return success
}
