rm -rf out
mkdir out
mkdir out\themes
rm -rf tmp
mkdir tmp
FOR /F %%F IN ('dir /b "src\themes\*.css"') DO (
 cleancss --source-map -o out/themes/%%F src/themes/%%F | cat
)
cp src/favicon.ico out/favicon.ico
cp src/*.png out
cp -r src out\src
cat src/index.html | sed "s/<!--|/ /g" | sed "s/|-->/ /g" > tmp/index.html
html-minifier tmp/index.html --remove-comments --collapse-whitespace --collapse-boolean-attributes -o out/index.html | cat
cp src/scheme.html out
cleancss --source-map -o out/all.min.css src/admin.css src/normalize.css src/soundboard.css src/tempupload.css src/main.css src/perms.css src/tab.css src/util.css | cat
java -jar closure/compiler.jar --language_in ECMASCRIPT5 --strict_mode_input=false --js src/x-tag-core.min.js --js src/prevel-min.js --js src/util.js --js src/log.js --js src/api.js --js src/tab.js --js src/config.js --js src/login.js --js src/perms.js --js src/soundboard.js --js src/upload.js --js src/theme.js --js src/tempupload.js --js src/admin.js --js_output_file out/all.min.js --create_source_map out/all.min.js.map
echo //# sourceMappingURL=all.min.js.map >> out/all.min.js
rm -rf ../etc/yumeko/public
cp -r out ../etc/yumeko/public
pause