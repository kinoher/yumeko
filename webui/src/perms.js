var Permissions = {
  List:[
"SBUpdateName",
"SBUpdateVolume",
"SBUpdateCost",
"SBDelete",
"SBUpload",
"SBPlay",
"UserChangePerms",
"UserMoveAll",
"FileUpload",
"DownloadUserAudio",
"InlineExplicit",
"InlineAlways",
"SBChangePlayingVolume",
"FileUploadForever",
"FileUploadDelete",
"SBCleanup",
  ],
  Mine:{},
  ChangeMine:function(bits){
    Permissions.List.forEach(function(k,i){
      Permissions.Mine[k]=!!(bits&(1 << i));
    });
    var ev = new Event('permchange');
    document.documentElement.dispatchEvent(ev);
  },
  Mutate:function(muts){
    muts.forEach(function(mut){
      if(mut.type=="attributes"&&mut.target.hasAttribute("perms")){
        return Permissions.PermMutate(mut.target);
      }
      if(mut.addedNodes){
        for(var i=0;i<mut.addedNodes.length;i++){
          var e=mut.addedNodes[i];
          if(e && e.hasAttribute && e.hasAttribute("perms")) Permissions.PermMutate(e);
          if(e && e.querySelectorAll)Array.prototype.forEach.call(e.querySelectorAll("*[perms]"),Permissions.PermMutate);
        }
      }
      mut=undefined;
    });
    muts=undefined;
  },
  PermMutate:function(e){
    document.documentElement.addEventListener("permchange",Permissions.PermListener.bind(e));
    Permissions.PermListener.call(e);
  },
  PermListener:function(e){
    var perms=this.getAttribute("perms");
    var pP = perms.split(";")
    var evil =pP[1].replace(/\|+/g,"||").replace(/\&+/g,"&&");
    var legal=Permissions.doEval(evil);
    if(pP[0]=="disable"){
      pl(this).addClass("permActive");
      this.disabled=legal?"":"disabled";
    }else if(pP[0]=="cdisable"){
      pl(this).addClass("permActive");
      this.children[0].disabled=legal;
    }else{
      if(!legal){
        pl(this).removeClass("permActive");
      }else{
        pl(this).addClass("permActive");
      }
    }
  },
  doEval:function(evil){
    with(Permissions.Mine){
      return eval(evil)||false;
    };
  },
};

(function(){
  var MutationObserver = window.MutationObserver || window.WebKitMutationObserver;
  var observer = new MutationObserver(Permissions.Mutate);
  observer.observe(document.documentElement,{
    attributes:true,
    childList:true,
    subtree:true,
    attributeFilter:["perms"],
  });
})();
Permissions.ChangeMine(0);
AddSetter(Login,"Permissions",function(v){
  Permissions.ChangeMine(v);
  return v;
});
