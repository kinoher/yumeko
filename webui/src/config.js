var Config = {
  Update:function(){
    API.Call("config");
  },
  CAPTCHASiteKey:"",
  Botname:"",
  MaxUploadTime:0,
};
pl(function(){
  AddSetter(Config,"Botname",function(v){
    document.title=v;
  });
});
Config.Update();
API.Cbs["config"]=function(d){
  Config.Botname=d.botname;
  Config.MaxUploadTime=d.maxUploadTime;
  Config.DefaultUploadTime=d.defaultUploadTime;
  Config.CAPTCHASiteKey=d.captchaSiteKey;
};
