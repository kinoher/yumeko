var HashDecode = (/^#([^,]*)(?:,([0-9a-f]*))?$/i).exec(window.location.hash);
var Tab={
  ReadHash:function(){
    var data = HashDecode;
    var tTab;
    if(data!=null && data[1]) {
      tTab=data[1];
    }
    return tTab||"soundboard";
  },
};
Tab = {
  ReadHash:Tab.ReadHash,
  Tab:{
    v:Tab.ReadHash(),
    setters:[
      function(v){
        v=v.toLowerCase();
        if(v=="logout"){
          Login.Logout();
          return Tab.ReadHash();
        }else{
          window.location.hash=v;
        }
        return v;
      },
    ],
  },
  TabList:{},
  TabHeaderList:{},
};
MakeProp(Tab,"Tab");
pl(function(){
  AddSetter(Tab,"Tab",function(v){
    Object.keys(Tab.TabList).forEach(function(k){
      pl(Tab.TabList[k]).removeClass("active");
      pl(Tab.TabHeaderList[k]).removeClass("active");
    });
    if(Tab.TabList[v]) {
      pl(Tab.TabList[v]).addClass("active");
      pl(Tab.TabHeaderList[v]).addClass("active");
    }else{
      Log.TabError("Cannot select tab: "+v);
    }
    return v;
  });
  document.body.addEventListener("paste",function(e){
    var bind=Tab.TabHeaderList[Tab.Tab].paste;
    if(!bind) return;
    if(e.clipboardData.files.length<1) return;
    Upload.__GARBAGE=e.clipboardData;
    var fn = eval(bind||"");
    if((typeof fn)!="function")return console.log("Invalid bind ", this);
    fn(e,e.clipboardData);
  });
});
xtag.register("x-tab",{
  lifecycle:{
    created:function(){
      var el = document.createElement("x-click");
      el.className="nostyle";
      el.innerHTML=this.innerHTML;
      this.replaceChild(el,this.childNodes[0]);
    },
  },
  accessors:{
    name:{
      attribute:{},
      set:function(v){
        this._v=v;
        this.children[0].bind="Tab.Tab='"+v+"';";
        Tab.TabHeaderList[v]=this;
      },
      get:function(){
        return this._v;
      },
    },
    paste:{
      attribute:{},
      get:function(){
        return this._paste||"";
      },
      set:function(v){
        this._paste=v;
      },
    },
  }
});

xtag.register("x-tabcontent",{
  lifecycle:{},
  accessors:{
    name:{
      attribute:{},
      set:function(v){
        this._name=v;
        Tab.TabList[v]=this;
      },
      get:function(){
        return this._name;
      }
    }
  },
});
