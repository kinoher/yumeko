var Soundboard = {
  ProgressValue:0,
  CurrentTime:0,
  TimeFmt:function(s){
    var s=Math.floor(s);
    var m=Math.floor(s/60);
    s=s%60;
    var h=Math.floor(m/60);
    m=m%60;
    var r="";
    if(h){
      r=h+":";
      if(m<10){
        m="0"+m;
      }
    }
    if(s<10){
      s="0"+s;
    }
    r+=m+":"+s;
    return r
  },
  DoProgress:function(){
    if(!Soundboard.CurrentLength){
      Soundboard.CurrentStartTime=0;
      Soundboard.CurrentTime=0;
      Soundboard.CurrentLength=0;
      Soundboard.ProgressValue=0;
      return;
    }
    Soundboard.CurrentTime=((Date.now()-Soundboard.CurrentStartTime)/1000);
    Soundboard.ProgressValue = Soundboard.CurrentTime/Soundboard.CurrentLength;
    if(Soundboard.ProgressValue>1) return;
    setTimeout(Soundboard.DoProgress,100/Soundboard.CurrentLength);
  },
  CurrentLength:0,
  CurrentStartTime:0,
  Stop:function(){
    API.Call("soundboard/stop");
  },
  RecentPlays:{},
  Points:0,
  Clip:{
    List:{},
    AddItem:function(d){
      var Sb_Clip=Soundboard.Clip;
      Sb_Clip.List[d.ID]=d;
      var path = Sb_Clip.GetPath(d.Name);
      var name = Sb_Clip.GetName(d.Name);
      var clip = document.createElement("sb-clip");
      clip.innerHTML=Sb_Clip.Fmter(d.ID);
      clip.CID = d.ID;
      clip.title = d.Name;
      clip.name = name;
      Sb_Clip.List[d.ID].El=clip;
      var folder = Sb_Clip.GetElByPath(path);
      SortInsert(folder,clip,function(a,b){
        if(a.tagName!="SB-CLIP")return -1;
        if(b.tagName!="SB-CLIP")return 1;
        return StrCmp(a.name,b.name);
      },1);
    },
    GetPath:function(name){
      var i = name.lastIndexOf("/");
      if(i==-1) return "";
      return name.substr(0,i);
    },
    GetName:function(name){
      var i = name.lastIndexOf("/");
      return name.substr(i+1);
    },
    Main:0,
    Play:function(id,wager,overlay){
      var d = {id:id};
      if (wager){
        d.wager = Soundboard.Clip.Wager
      }
      if(overlay){
        d.overlay=1
      }
      API.Call("soundboard/play",d);
    },
    GetFolderByPath:function(path){
      var t = Soundboard.Clip.Paths;
      var parts = path.split("/");
      if(path!="") for(var i=0;i<parts.length;i++){
        var tt = t["_"+parts[i]];
        if(tt===undefined){//create folder
          var folder = document.createElement("sb-folder");
          var path = parts.slice(0,i+1).join("/");
          folder.path=path;
          var clickEl = document.createElement("x-click");
          clickEl.className="nostyle";
          clickEl.bind="Soundboard.Clip.FolderToggle(this)";
          clickEl.el=path;
          clickEl.appendChild(document.createTextNode(parts[i]));
          folder.appendChild(clickEl);
          var preEl=t.r.children[0];
          var preName;
          var cmpStr = "_"+parts[i];
          var compare = StrCmp;
          Object.keys(t).forEach(function(k){
            if(k=="r")return;
            if(k=="p")return;
            if(compare(k,cmpStr)<0){
              if(preName===undefined || compare(k,preName)>0){
                preName=k;
                preEl=t[k].r;
              }
            }
          });
          t.r.insertBefore(folder,preEl.nextSibling);
          tt=t[cmpStr]={
            r:folder,
          };
          if(Soundboard.Clip.SavedHides[path]||Soundboard.Clip.Hides[clickEl.el]){
            Soundboard.Clip.FolderSet(clickEl,true);
          }
        }
        t=tt;
      }
      return t;
    },
    GetElByPath:function(path){
      return Soundboard.Clip.GetFolderByPath(path).r;
    },
    Paths:{},
    Hides:{},
    SavedHides:JSON.parse(localStorage["sbopenfolders"]||"{}"),
    SaveHides:function(){
      var keys = Object.keys(Soundboard.Clip.Hides)
      if(!keys.length)return;
      localStorage["sbopenfolders"]=JSON.stringify(Soundboard.Clip.Hides);
    },
    FolderToggle:function(el){
      var Sb_Clip=Soundboard.Clip;
      Sb_Clip.Hides[el.el]=Sb_Clip.Hides[el.el]^1;
      Sb_Clip.SaveHides();
      Sb_Clip.FolderUpdate(el);
    },
    FolderSet:function(el,v){
      var Sb_Clip=Soundboard.Clip;
      Sb_Clip.Hides[el.el]=v;
      Sb_Clip.SaveHides();
      Sb_Clip.FolderUpdate(el);
    },
    FolderUpdate:function(el){
      if(Soundboard.Clip.Hides[el.el]){
        pl(el.parentElement).addClass("active");
      }else{
        pl(el.parentElement).removeClass("active");
      }
    },
    CheckRemFolder:function(fname){
      if(fname=="") return;
      var folder = Soundboard.Clip.GetFolderByPath(fname);
      console.log(folder);
      if(folder.r.children.length<=1){
        folder.r.parentElement.removeChild(folder.r);
        var ps = fname.split("/");
        var i = ps.pop();
        var np = ps.join("/");
        var cont = Soundboard.Clip.GetFolderByPath(np);
        delete cont["_"+i];
        delete Soundboard.Clip.Hides[fname];
        delete Soundboard.Clip.Hides["_b"+fname];
        Soundboard.Clip.CheckRemFolder(np);
      }
    },
    RemItem:function(id){
      var e = Soundboard.Clip.List[id].El;
      e.parentElement.removeChild(e);
      Soundboard.Clip.CheckRemFolder(Soundboard.Clip.GetPath(Soundboard.Clip.List[id].Name));
      delete Soundboard.Clip.List[id];
    },
    NameFmter:function(v){
      v=Soundboard.Clip.GetName(v);
      var maxL=20;
      if(v.length>maxL) return v.substr(0,maxL-2)+"...";
      return v;
    },
    Fmter:function(id){
      var v="Soundboard.Clip.List['"+id+"']";
      var r = [
  '<x-click nostyle bind="Soundboard.Clip.Play(\''+id+'\')" ctrl="Soundboard.Clip.Play(\''+id+'\',0,true)" alt="Soundboard.Clip.Favourite(\''+id+'\')">',
    '<x-out bind="'+v+'.Name" fmt="Soundboard.Clip.NameFmter"></x-out>',
  '</x-click>',
  '<x-click nostyle class="fa fa-pencil" bind="Soundboard.Clip.Edit(\''+id+'\')"></x-click>',
].join('\n');
      return r;
    },
    FavList:{},
    Favourite:function(id,loading){
      var Sb_Clip = Soundboard.Clip;
      var elemHolder = document.getElementById("sbFavs");
      if(Sb_Clip.FavList.hasOwnProperty(id)){
        Sb_Clip.FavList[id].parentElement.removeChild(Sb_Clip.FavList[id]);
        delete Sb_Clip.FavList[id];
        if(!Object.keys(Sb_Clip.FavList).length){
          pl(elemHolder).removeClass("active");
        }
      }else{
        var d = Sb_Clip.List[id];
        if(!d)return;
        var path = Sb_Clip.GetPath(d.Name);
        var name = Sb_Clip.GetName(d.Name);
        var clip = document.createElement("sb-clip");
        clip.innerHTML=Sb_Clip.Fmter(d.ID);
        clip.CID = d.ID;
        clip.title = d.Name;
        clip.name = name;
        SortInsert(elemHolder,clip,function(a,b){
          return StrCmp(a.name,b.name);
        },0);
        if(!Object.keys(Sb_Clip.FavList).length){
          pl(elemHolder).addClass("active");
        }
        Sb_Clip.FavList[id]=clip;
      }
      if(!loading){
        localStorage["favourites"]=JSON.stringify(Object.keys(Sb_Clip.FavList));
      }
    },
    Edit:function(v){
      if(v==Soundboard.Clip.CurrentEdit){
        Soundboard.Clip.CurrentEdit = "";
      }else{
        Soundboard.Clip.CurrentEdit = v||"";
      }
    },
    CurrentEditE:{
      Cost:0,
      Duration:0,
      Name:"",
      ID:"",
      Volume:0,
      UploadDate:"",
      ChannelMode:0,
      UseMusicEncoder:0,
    },
    VolToAmp:function(vol){
      return Math.pow(vol,2);
    },
    AmpToVol:function(amp){
      return Math.sqrt(amp);
    },
    CurrentEdit:{
      v:"",
      setters:[
        function(v){
          var e =Soundboard.Clip.List[v];
          if(e){
            document.getElementById("sbEditDL").href="/api/soundboard/download?key="+Login.Hash+"&id="+v;
            ["Cost","Duration","Name","ID","Volume","UploadDate","ChannelMode","UseMusicEncoder"].forEach(function(i){
              Soundboard.Clip.CurrentEditE[i]=e[i];
            });
            return v;
          }
          return "";
        },
      ],
    },
    Wager:8,
    SaveEdit:function(){
      var Sb_Clip=Soundboard.Clip;
      var e =Sb_Clip.List[Sb_Clip.CurrentEdit];
      var data={
        id:Sb_Clip.CurrentEdit,
      };
      ["Cost","Name","ChannelMode"].forEach(function(i){
        if(Sb_Clip.CurrentEditE[i]!=e[i]){
          data[i.toLowerCase()]=Sb_Clip.CurrentEditE[i];
        }
      });
      if (Sb_Clip.CurrentEditE.UseMusicEncoder!=e.UseMusicEncoder){
        data["encoder"]=Sb_Clip.CurrentEditE.UseMusicEncoder?1:0;
      }
      if(Sb_Clip.CurrentEditE["Volume"]!=e["Volume"]){
        data["volume"]=Soundboard.Clip.VolToAmp(Sb_Clip.CurrentEditE["Volume"]);
      }
      API.Call("soundboard/update",data);
    },
    DeleteEdit:function(){
      if(prompt("Type `delete` to delete the clip.")=="delete"){
        API.Call("soundboard/delete",{id:Soundboard.Clip.CurrentEdit});
      }
    },
    CleanEdit:function(){
      Soundboard.Clean(Soundboard.Clip.CurrentEdit,prompt("Type anything to run a extended clean."))
    },
    LSearchStr:"",
    LSearchRes:[],
    SearchRes:[],
    SearchStr:{
      v:"",
      setters:[
        function(v){
          if(v==Soundboard.Clip.LSearchStr)return v;
          Soundboard.Clip.LSearchStr=v;
          Soundboard.Clip.LSearchRes=Soundboard.Clip.SearchRes;
          if(!v){
            Soundboard.Clip.SearchRes=[];
            pl(document.getElementById("sbMain")).removeClass("search")
          }else{
            pl(document.getElementById("sbMain")).addClass("search")
            Soundboard.Clip.SearchRes=Soundboard.Clip.Search(v);
          }
          var diff=function(a,b) {
            return a.filter(function(i) {return  b.indexOf(i) < 0;});
          };
          var ins=diff(Soundboard.Clip.SearchRes,Soundboard.Clip.LSearchRes);
          var del=diff(Soundboard.Clip.LSearchRes,Soundboard.Clip.SearchRes);
          ins.forEach(function(e){
            pl(Soundboard.Clip.List[e].El).addClass("searchTarget");
          });
          del.forEach(function(e){
            pl(Soundboard.Clip.List[e].El).removeClass("searchTarget");
          });
          var els = document.getElementsByTagName("sb-folder");
          for(var i=0;i<els.length;i++){
            var el=els[i];
            if(el.getElementsByClassName("searchTarget").length){
              pl(el).addClass("searchTarget");
            }else{
              pl(el).removeClass("searchTarget");
            }
          }
          return v;
        },
      ],
    },
    Search:function(needle,minReturns){
      minReturns=minReturns||10
      var pool=[];
      var SB_C_List=Soundboard.Clip.List;
      pathNeedle=Soundboard.Clip.GetPath(needle);
      nameNeedle=needle;
      if(pathNeedle){
        nameNeedle=Soundboard.Clip.GetName(needle);
        pathNeedle+="/";
        Object.keys(SB_C_List).forEach(function(k){
          if(SB_C_List[k].Name.indexOf(pathNeedle)!=-1){
            var name = Soundboard.Clip.GetName(SB_C_List[k].Name);
            if(name.length-3>=nameNeedle.length){
              pool[pool.length]={
                ID:SB_C_List[k].ID,
                Name:name.toLowerCase(),
              };
            }
          }
        });
      }
      if(pool.length<minReturns){
        var minLen=nameNeedle.length-4;
        Object.keys(SB_C_List).forEach(function(k){
          var name = Soundboard.Clip.GetName(SB_C_List[k].Name);
          if(name.length>=minLen){
            pool[pool.length]={
              ID:SB_C_List[k].ID,
              Name:name.toLowerCase(),
            };
          }
        });
      }
      nameNeedle=nameNeedle.toLowerCase();
      var distPools=[[]];
      var maxDist=10;
      console.log(pool.length);
      pool.forEach(function(e){
        var hay = e.Name;
        var needle = nameNeedle;
        var v1=new Array(hay.length+1);
        var v2=new Array(hay.length+1);
        for(var i=0;i<v1.length;i++)v1[i]=0;
        for(var i=0;i<needle.length;i++){
          v2[0]=i+1;
          for(var j=0;j<hay.length;j++){
            var cost=needle[i]!=hay[j];
            v2[j+1]=Math.min(v1[j+1]+1,v2[j]+1,v1[j]+cost);
          }
          {
            var vt=v1;
            v1=v2;
            v2=vt;
          }
        }
        var min=v1[0];
        v1.forEach(function(e){
          min=Math.min(min,e);
        });
        var len = min;
        if(!distPools[len])distPools[len]=[];
        distPools[len][distPools[len].length]=e.ID;
        var totLen=0
        for(var i=0;i<distPools.length-1;i++){
          if(distPools[i]){
            totLen+=distPools[i].length;
            if(totLen>=minReturns || totLen/((i+1)/2)<distPools[i].length){
              maxDist=i+1;
              break;
            }
          }
        }
        distPools.splice(maxDist,99);
      });
      var retarr=[];
      distPools.forEach(function(e){
        retarr=retarr.concat(e||[]);
      });
      return retarr;
    },
  },
  DateFmt:function(v){
    return (new Date(v)).toDateString();
  },
  Setup:function(){
    API.DCall({
      url:"soundboard/list",
      ForceHTTP:true,
    });
    API.Call("points");
  },
  Upload:{
    MainLit:undefined,
    MainEvent:function(ev){
      ev.stopPropagation();
      ev.preventDefault();
      var t = ev.target;
      for(;;){
        if(t.tagName=="SB-FOLDER"||t.id=="sbMain"||t.id=="sbQuickbar")break;
        t=t.parentElement
        if(!t)return;
      }
      if(ev.type=="dragover"){
        ev.dataTransfer.dropEffect="copy";
        var plt = pl(t);
        if(!plt.hasClass("dropLit")){
          plt.addClass("dropLit");
          if(Soundboard.Upload.MainLit)pl(Soundboard.Upload.MainLit).removeClass("dropLit");
          Soundboard.Upload.MainLit=t;
        }
        if(Soundboard.Upload.Timeout)clearTimeout(Soundboard.Upload.Timeout);
        Soundboard.Upload.Timeout = setTimeout(function(){
          if(Soundboard.Upload.MainLit)pl(Soundboard.Upload.MainLit).removeClass("dropLit");
        },200);
      }else if(ev.type=="drop"){
        if(Soundboard.Upload.MainLit)pl(Soundboard.Upload.MainLit).removeClass("dropLit");
        console.log(ev);
        if(ev.dataTransfer.files&&ev.dataTransfer.files.length>0){
          Soundboard.Upload.Upload(ev.dataTransfer.files[0],t.path);
        }else{//move a clip
          var id = ev.dataTransfer.getData("ID");
          if(t.id=="sbQuickbar"){
            //TODO: imp
          }else{
            var name = Soundboard.Clip.List[id].Name;
            var path = t.path||"";
            if(path)path+="/";
            name = path+Soundboard.Clip.GetName(name);
            API.Call("soundboard/update",{
              id:id,
              name:name,
            });
          }
        }
      }
    },
    Upload:function(fi,pathPrefix){
      pathPrefix=pathPrefix||"";
      if(pathPrefix)pathPrefix+="/";
      Soundboard.Upload.State=2;
      Upload.Upload("audio",fi,"soundboard/upload",function(d){
        Soundboard.Upload.State=3;
        var rd = JSON.parse(d);
        Soundboard.Upload.Name=pathPrefix+rd.Name;
        Soundboard.Upload.ID=rd.ID;
      },
      function(e){
        Soundboard.Upload.Progress=e.loaded/e.total;
        Soundboard.Upload.ProgText=Upload.SizeFmt(e.loaded)+"/"+Upload.SizeFmt(e.total);
      });
    },
    UploadByInput:function(){
      Soundboard.Upload.Upload(document.getElementById("sbUploadBox").files[0])
    },
    Finalize:function(){
      API.Call("soundboard/uploadFinish",{
        id:Soundboard.Upload.ID,
        name:Soundboard.Upload.Name,
      });
      Soundboard.Upload.State=0;
    },
    Show:function(){
      if(Soundboard.Upload.State==1){
        Soundboard.Upload.State=0;
      }else if(Soundboard.Upload.State==0){
        Soundboard.Upload.State=1;
      }
    },
    State:0,
    ID:"",
    Progress:0,
    ProgText:"",
    Name:""
  },
  ShowAddPoints:function(){
    Soundboard.AddPointsVis^=1;
  },
  AddPointsVis:0,
  OnCaptchaLoad:function(){
    AddSetter(Config,"CAPTCHASiteKey",function(v){
      if(v)
        AddSetter(Soundboard,"CaptchaTheme",function(vt){
          Soundboard.CapID = grecaptcha.render(document.getElementById("sbCaptcha"),{
            sitekey:Config.CAPTCHASiteKey,
            theme:vt,
            callback:Soundboard.OnCaptcha,
            size:"compact",
          });
        return vt;
      });
      return v;
    });
  },
  CaptchaTheme:"light",
  CapID:0,
  OnCaptcha:function(d){
    API.Call("pointsPlus",{
      capTok:d,
    });
    grecaptcha.reset(Soundboard.CapID);
    Soundboard.AddPointsVis=0;
  },
  CurrentVolume:{},
  CurrentVolumeH:[],
  Clean:function(id,ext){
    var d = {
      id:id,
    };
    if(ext){
      d.extended=ext;
    }
    API.Call("soundboard/cleanup",d);
  },
};
for(var i=0;i<10;i++){
  Soundboard.RecentPlays[i]="";
}
function grecaptchaLoaded(){
  Soundboard.OnCaptchaLoad();
}
pl(function(){
  Soundboard.Clip.Paths.r=document.getElementById("sbMain");
  AddSetter(Soundboard.Clip,"CurrentEdit",function(v){
    var e = document.getElementById("sbEdit");
    if(v==""){
      pl(e).removeClass("active");
    }else{
      pl(e).addClass("active");
    }
    return v;
  });
  AddSetter(Soundboard.Upload,"State",function(v){
    var e = document.getElementById("sbUpload");
    var p = e.children;
    for(var i=0;i<p.length;i++){
      p[i].style.display="none";
    }
    if(!v){
      pl(e).removeClass("active")
    } else {
      pl(e).addClass("active")
      p[v-1].style.display="block";
    }
    return v;
  });
  AddSetter(Soundboard,"AddPointsVis",function(v){
    el = pl(document.getElementById("sbCaptcha"));
    if(v){
      el.addClass("active");
    }else{
      el.removeClass("active");
    }
    return v;
  });
  document.getElementById("sbSearchInp").addEventListener("keydown",function(e){
    if(e.keyCode==27){
      Soundboard.Clip.SearchStr="";
      e.preventDefault();
      e.stopPropagation();
    }
  });
  window.addEventListener("keydown",function(e){
    if((e.keyCode==114 || (e.ctrlKey && e.keyCode==70)) && Tab.Tab=="soundboard"){
      e.preventDefault();
      e.stopPropagation();
      document.getElementById("sbSearchInp").children[0].focus();
    }
  });
  for(var i=0;i<3;i++){
    var e=document.getElementById("sbVolumeHolder"+i);
    Soundboard.CurrentVolumeH[i]=pl(e);
    var t=i;
    (function(e,t){
      e.addEventListener("mouseup",function(v){
        API.Call("soundboard/volume",{
          channel:t,
          volume:Soundboard.Clip.VolToAmp(Soundboard.CurrentVolume[t]),
        });
      });
    })(e,t);
  }
});
API.Cbs["soundboard/volume"]=function(d){
  if(d.Volume!==undefined){
    Soundboard.CurrentVolume[d.Channel]=Soundboard.Clip.AmpToVol(d.Volume);
  }
};
API.Cbs["soundboard/play"]=function(data){
  if(data.eapi)return;
  Soundboard.CurrentVolumeH[data.Channel].removeClass("hidden");
  if(data.hasOwnProperty("success")) return;
  for(var i=9;i>=0;i--){
    Soundboard.RecentPlays[i+1]=Soundboard.RecentPlays[i];
  }
  Soundboard.RecentPlays[0]=data.Name+(data.User?" - "+data.User:"");
  if(data.Wager>0){
    Soundboard.RecentPlays[0]+=" ("+data.Wager+")"
  }
  if(data.Channel==2){
    Soundboard.RecentPlays[0]+=" (+)";
  } else {
    Soundboard.CurrentLength=data.Duration;
    Soundboard.CurrentStartTime=Date.now();
    Soundboard.DoProgress();
  }
};
API.Cbs["soundboard/stop"]=function(data){
  if(data.success){
    Soundboard.CurrentVolumeH[data.Channel].addClass("hidden");
  }
  if(data.success && data.Channel==0){
    Soundboard.CurrentLength=0;
  }
};
API.Cbs["soundboard/list"]=function(d){
  var Sb_Clip=Soundboard.Clip;
  var el = document.createElement("div");
  el.id="sbMain";
  el.appendChild(document.createElement("span"));
  Sb_Clip.Paths={r:el};
  Sb_Clip.List={};
  ForEach(d,Sb_Clip.AddItem,function(){
    var oel = document.getElementById("sbMain");
    oel.parentElement.replaceChild(el,oel);

    var favs=[];
    if(localStorage["favourites"]){
       favs=JSON.parse(localStorage["favourites"]);
    };
    favs.forEach(function(k){
      if(k=="")return;
      Soundboard.Clip.Favourite(k,true)
    });
  });
};
API.Cbs["soundboard/cbUpload"]=function(d){
  Soundboard.Clip.AddItem(d);
};
API.Cbs["soundboard/delete"]=function(d){
  Soundboard.Clip.RemItem(d.ID);
  if(d.ID==Soundboard.Clip.CurrentEdit){
    Soundboard.Clip.Edit();
  }
};
API.Cbs["soundboard/update"]=function(d){
  var Sb_Clip=Soundboard.Clip;
  if(d.Name && Sb_Clip.List[d.ID].Name!=d.Name){
    var e = Sb_Clip.List[d.ID];
    Sb_Clip.RemItem(d.ID);
    e.Name=d.Name;
    Sb_Clip.AddItem(e);
  }
  if(d.Volume)Sb_Clip.List[d.ID].Volume=Soundboard.Clip.AmpToVol(d.Volume);
  if(d.CostOverride)Sb_Clip.List[d.ID].Cost=d.CostOverride;
  if(d.UseMusicEncoder)Sb_Clip.List[d.ID].UseMusicEncoder=d.UseMusicEncoder;
  if(d.ChannelMode)Sb_Clip.List[d.ID].ChannelMode=d.ChannelMode;
};
API.Cbs["points"]=function(d){
  Soundboard.Points=d.points;
};
xtag.register("sb-clip",{
  lifecycle:{
    created:function(){
      this.draggable=true;
      this.__dragEvent = function(e){
        if(!e.shiftKey)return;
        e.dataTransfer.dropEffect="move";
        e.dataTransfer.setData("ID",this.CID);
      };
      this.addEventListener("dragstart",this.__dragEvent);
    },
    removed:function(){
      this.removeEventListener("dragstart",this.__dragEvent);
    },
  },
});
xtag.register("sb-folder",{});

AddSetter(Login,"IsLoggedIn",
function(v){
  if(v){
    Soundboard.Setup();
  }else{

  }
  return v;
});

xtag.register("sb-user",{
  lifecycle:{
    created:function(){
    },
    removed:function(){
    }
  },
  accessors:{
    bind:{
      attribute:{},
      get:function(){
        return this._bindData.text||"";
      },
      set:function(v){
        this.innerHTML=[
"<a href='/api/downloadUserAudio?key="+Login.Hash+"&target="+v+"' download='' target='_blank'>",
  "<x-click class='fa fa-download'></x-click>",
"</a>",
"<x-out bind='Users.List[\""+v+"\"].Username'></x-out>",
        ].join("");
        this._bindData={
          text:v,
        };
      },
    }
  }
});
