var Admin = {
  MoveAllToMe:function(){
    API.Call("moveAll");
  },
  SetupHeader:function(){
    var header = document.createElement("l-r");
    header.id="amPTHeader";
    header.appendChild(document.createElement("l-i"));
    var add = function(e){
      var a=document.createElement("l-i");
      var b=document.createElement("div");
      var c=document.createElement("div");
      var d=document.createElement("div");
      d.appendChild(document.createTextNode(e.Name))
      c.appendChild(d);
      b.appendChild(c);
      a.appendChild(b);
      header.appendChild(a);
    };
    Admin.SortedList.forEach(add);
    add({Name:"\xa0"});
    var el = document.getElementById("amPermTable");
    el.insertBefore(header,el.children[0]);
  },
  SortedList:function(){
    var inlist = Permissions.List;
    var l=[];
    inlist.forEach(function(k,i){
      l[l.length]={
        Name:k,
        Bit:1 << i,
      };
    });
    l.sort(function(a,b){
      return StrCmp(a.Name,b.Name);
    });
    return l;
  }(),
  AddUser:function(d){
    var el = document.createElement("am-puser");
    el.bind=d.ID;
    Users.List[d.ID].AMElem=el;
    SortInsert(document.getElementById("amPermTable"),el,function(a,b){
      return a.bind-b.bind;
    },1);
  },
  RemUser:function(d){
    if(Users.List[d].AMElem)Users.List[d].AMElem.parentElement.removeChild(Users.List[d].AMElem);
  },
  PermDelta:{},
  PermSave:function(){
    Object.keys(Admin.PermDelta).forEach(function(id){
      API.Call("updateUserPerms",{
        target:id,
        perm:Admin.PermDelta[id],
      });
    });
    Admin.PermDelta={};
  },
  PermReset:function(){
    API.Call("user");
    Admin.PermDelta={};
  }
};

pl(function(){
  Admin.SetupHeader();

  document.getElementById("amPermTable").addEventListener("click",function(ev){
    if(ev.target.tagName!="L-I") return;
    var bit = ev.target.attributes.bit.value;
    if(!bit) return;
    var user = ev.target.parentElement.bind;
    if(!user) return;
    if(Admin.PermDelta[user]===undefined)Admin.PermDelta[user]=Users.List[user].Perms;
    Admin.PermDelta[user]^=bit;
    Users.List[user].Perms=Admin.PermDelta[user];
  });
});

xtag.register("am-puser",{
  lifecycle:{
    created:function(){},
    destroyed:function(){
      if(this._bindData){
        RemoveSetter(this._bindData.t[0],this._bindData.t[1],this._bindData.id);
      }
    },
  },
  accessors:{
    bind:{
      attribute:{},
      get:function(){
        return this._bindData.text||"";
      },
      set:function(v){
        if(this._bindData){
          RemoveSetter(this._bindData.t[0],this._bindData.t[1],this._bindData.id);
        }
        var that=this;
        var t = [Users.List[v],"Perms"];
        var id=AddSetter(t[0],t[1],function(perm){
          var arr=['<l-i><x-out bind="Users.List[\''+v+'\'].Username"></x-out></l-i>'];
          Admin.SortedList.forEach(function(e){
            arr[arr.length]='<l-i class="abPTBox'+((perm&e.Bit)?" valid":"")+'" title="'+e.Name+'" bit="'+e.Bit+'"></l-i>'
          });
          that.innerHTML=arr.join("");
          return perm;
        });
        this._bindData={
          text:v,
          t:t,
          id:id,
        };
      }
    },
  },
});
