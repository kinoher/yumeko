var Login = {
  Hash:"",
  ShowAnimate:0,
  Username:"",
  Permissions:0,
  Points:0,
  ID:-1,
  Logout:function(){
    Login.Hash="";
  },
  IsLoggedIn:{
    v:false,
    setters:[],
  },
};
MakeProp(Login,"Hash");
AddSetter(Login,"Hash",function(hash){
	var ev = document.createEvent("CustomEvent");
	ev.initCustomEvent("yumeko_settoken",false, false, hash);
	window.dispatchEvent(ev);
	return hash;
});
MakeProp(Login,"IsLoggedIn");
Login.Hash = function(){
  var data = HashDecode;
  var tHash;
  if(data!=null && data[2]) {
    tHash=data[2];
  }
  var v =  tHash||localStorage.token||"";
  if(v==""){
    Login.ShowAnimate=1;
  }
  return v;
}();
MakeProp(Login,"Permissions");
(function(){
  var lastLogin;
  AddSetter(Login,"Hash",function(v){
    if(v===lastLogin&&v!==undefined) return v;
    lastLogin=v;
    if(v.length>=9){
      API.DCall({
        url:"user",
        ForceHTTP:true,
      });
    }else{
      Login.Permissions=0;
      API.CloseWS();
    }
    localStorage.token=v
    return v;
  });
})();
pl(function(){
  AddSetter(Login,"Permissions",function(v){
    document.getElementById("mainContainer").style.top=((v==0)?"0%":"-100%");
    return v;
  });
  AddSetter(Login,"ShowAnimate",function(v){
    if(v) document.getElementById("mainContainer").style.transition="top .5s cubic-bezier(0.86, 0, 0.07, 1)";
    return v;
  })
});

var Users={
  List:{},
  AddUser:function(d){
    if(Users.List[d.ID])Users.RemUser(d.ID);
    Users.List[d.ID]=d;
    var la;
    AddSetter(Users.List[d.ID],"Connected",function(v){
      if(la!=undefined && !(la^v))return v;
      la=v;
      if(v){
        Users.Login(d);
      }else{
        Users.Logout(d)
      }
      return v;
    });
    Admin.AddUser(d);
  },
  RemUser:function(id){
    var d=Users.List[id];
    Admin.RemUser(id);
    if(d.Connected)Users.Logout(d);
    delete Users.List[id];
  },
  Login:function(d){
    var e = document.createElement("sb-user");
    var users = document.getElementById("sbUsers");
    e.bind=d.ID;
    e.name=d.Username;
    SortInsert(users,e,function(a,b){
      return StrCmp(a.name,b.name);
    },1);
    Users.List[d.ID].SBElem = e;
  },
  Logout:function(d){
    if(!Users.List[d.ID].SBElem)return;
    Users.List[d.ID].SBElem.parentElement.removeChild(Users.List[d.ID].SBElem);
  },
};
API.Cbs["user"]=function(data){
  Login.ID=data.ID;
  Object.keys(Users.List).forEach(function(k){
    Users.RemUser(k);
  });
  Object.keys(data.Users).forEach(function(k){
    Users.AddUser(data.Users[k]);
  });
  BindTo(Users.List[Login.ID],"Username",Login,"Username");
  BindTo(Users.List[Login.ID],"Perms",Login,"Permissions");
  API.SetupWS();
  xtag.skipFrame(function(){
    Login.ShowAnimate=1;
  });
};
API.Cbs["cbUserUpdate"]=function(d){
  if(Users.List[d.ID]===undefined){
    return Users.AddUser(d);
  }
  ["Connected","Username"].forEach(function(k){
    if(d.hasOwnProperty(k)) Users.List[d.ID][k]=d[k];
  });
  if(d.hasOwnProperty("Perms")&&Admin.PermDelta[d.ID]===undefined)Users.List[d.ID].Perms=d.Perms
};
