var TempUpload = {
  Upload:function(fi){
    Upload.Upload("upload",fi,"upload/upload",function(d){
      TempUpload.Progress=0;
      TempUpload.ProgText="Done";
    },
    function(e){
      TempUpload.Progress=e.loaded/e.total;
      TempUpload.ProgText=Upload.SizeFmt(e.loaded)+"/"+Upload.SizeFmt(e.total);
    },{
      expire:TempUpload.Expire,
      inline:TempUpload.Inline?1:"",
    });
  },
  UploadByInput:function(){
    TempUpload.Upload(document.getElementById("tuFileBox").children[0].files[0])
  },
  Delete:function(id){
    if(prompt("Type `delete` to delete the file.")=="delete"){
      API.Call("upload/delete",{
        id:id,
      });
    }
  },
  UpdateExpireTime:function(){
    var v=Config.MaxUploadTime;
    if(Permissions.FileUploadForever)v="";
    var el=document.getElementById("tuExpire");
    if(el.max!=v)el.max=v;
  },
  Inline:true,
  Expire:0,
  Progress:0,
  ProgText:"",
  List:{},
};
AddSetter(Config,"DefaultUploadTime",function(v){
  TempUpload.Expire=v;
  return v;
});
pl(function(){
  AddSetter(Config,"MaxUploadTime",function(v){
    TempUpload.UpdateExpireTime();
    return v;
  });
  AddSetter(Permissions.Mine,"FileUploadForever",function(v){
    TempUpload.UpdateExpireTime();
    return v;
  });
});
API.Cbs["upload/cbDelete"]=function(d){
  var e=TempUpload.List[d.ID];
  e.El.parentElement.removeChild(e.El);
  delete TempUpload.List[d.ID];
};
API.Cbs["upload/cbUpload"]=function(d){
  TempUpload.List[d.ID]=d;
  var el=document.createElement("tu-item");
  el.bind=d.ID;
  TempUpload.List[d.ID].El=el;
  document.getElementById("tuList").appendChild(el);
};
API.Cbs["upload/list"]=function(d){
  document.getElementById("tuList").innerHTML="";
  (d||[]).forEach(API.Cbs["upload/cbUpload"]);
}
AddSetter(Login,"IsLoggedIn",function(v){
  if(v){
    API.Call("upload/list");
  }else{

  }
  return v;
});
xtag.register("tu-item",{
  accessors:{
    bind:{
      attribute:{},
      set:function(id){
        this._bind=id;
        this.innerHTML=[
'<l-i><a href="/api/upload/download?id='+id+'" target="_blank"><x-out bind="TempUpload.List[\''+id+'\'].Name"><x-out></a></l-i>',
'<l-i><x-out bind="Users.List[\''+TempUpload.List[id].Uploader+'\'].Username"></x-out></l-i>',
'<l-i perms=";FileUploadDelete"><x-click bind="TempUpload.Delete(\''+id+'\')" class="fa fa-trash-o"></x-click></l-i>',
        ].join("");
      },
      get:function(v){
        return this._bind;
      }
    },
  },
});
