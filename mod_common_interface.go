// Copyright 2017 Yumeko Contributors (as named in CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the LICENSE file

package main

import "errors"

//MoveAllTo moves all the user in the server to the caller's location
func MoveAllTo(user *User) error {
	if !user.Has(PermissionUserMoveAll) {
		return errors.New("Missing Permission")
	}
	muser := user.M()
	if muser == nil {
		return errors.New("Not Connected")
	}
	for _, u := range Mumble.Users {
		if u.Channel.ID != muser.Channel.ID {
			u.Move(muser.Channel)
		}
	}
	return nil
}
