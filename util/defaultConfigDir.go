package util

import "runtime"

func DefaultConfigDir() string {
	if runtime.GOOS == "windows" {
		return "c:\\yumeko\\"
	} else {
		return "/etc/yumeko.d/"
	}
}
