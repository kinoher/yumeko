// Copyright 2017 Yumeko Contributors (as named in CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the LICENSE file

package util

import (
	"net/http"
	"time"
)

var Client = &http.Client{
	Transport: &http.Transport{
		TLSHandshakeTimeout:   time.Second * 5,
		MaxIdleConnsPerHost:   1,
		ResponseHeaderTimeout: time.Second * 5,
	},
	Timeout: time.Second * 15,
}

var NoRedirectClient *http.Client

func init() {
	c := *Client
	NoRedirectClient = &c
	NoRedirectClient.CheckRedirect = func(req *http.Request, via []*http.Request) error {
		return http.ErrUseLastResponse
	}
}
