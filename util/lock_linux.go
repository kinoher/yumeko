// +build linux

package util

import (
	"os"

	"golang.org/x/sys/unix"
)

func Lock(file *os.File) error {
	return unix.Flock(int(file.Fd()), unix.LOCK_EX)
}
