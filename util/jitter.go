// Copyright 2017 Yumeko Contributors (as named in CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the LICENSE file

package util

import (
	"math/rand"
	"time"
)

var jitrand = rand.New(rand.NewSource(time.Now().Unix()))

//Adds up to +-5 seconds of deviaton to the input time
func Jitter(in time.Duration) time.Duration {
	max := time.Second * 10
	if max > in {
		max = in
	}
	dif := time.Duration(jitrand.Int63n(int64(max))) - (max / 2)
	return in + dif
}
