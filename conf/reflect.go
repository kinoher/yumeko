// Copyright 2017 Yumeko Contributors (as named in CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the LICENSE file

package conf

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"reflect"
	"strconv"
	"strings"
)

// unwrap returns T in cases chan T, *T, func(T, error)
func unwrap(out reflect.Value) (reflect.Type, error) {
	switch out.Kind() {
	case reflect.Chan:
		return out.Type().Elem(), nil
	case reflect.Ptr:
		return out.Type().Elem(), nil
	case reflect.Func:
		t := out.Type()
		if t.NumIn() != 2 {
			return nil, fmt.Errorf("Expected function to take 2 arguments, takes %v", t.NumIn())
		}
		if t.NumOut() != 0 {
			return nil, fmt.Errorf("Expected function to return 0 arguments, returns %v", t.NumOut())
		}
		if !t.In(1).AssignableTo(reflect.TypeOf((*error)(nil)).Elem()) {
			return nil, fmt.Errorf("Expected function's second argument to be error but is %v", t.In(1))
		}
		return t.In(0), nil
	default:
		return nil, fmt.Errorf("%v is not a pointer, channel, or func", out.Type())
	}
}

// write writes in to out in cases chan T, *T, func(T, error).
// If out is a function err is passed to it and nil is returned,
// otherwise err is returned
func write(out, in reflect.Value, err error) error {
	if in.Kind() == reflect.Invalid {
		t, err := unwrap(out)
		if err != nil {
			panic(err)
		}
		in = reflect.Zero(t)
	}
	switch out.Kind() {
	case reflect.Chan:
		out.Send(in)
		return err
	case reflect.Ptr:
		out.Elem().Set(in)
		return err
	case reflect.Func:
		rerr := reflect.ValueOf(err)
		if err == nil {
			rerr = reflect.Zero(reflect.TypeOf((*error)(nil)).Elem())
		}
		out.Call([]reflect.Value{in, rerr})
		return nil
	}
	panic("Unsupported type")
}

var descore = strings.NewReplacer(
	" ", "",
	"_", "",
	",", "",
)

func reint(src string) (str string, mul int, err error) {
	mul = 1
	str = src
	lastnum := strings.LastIndexAny(str, "0123456789")
	sfx := ""
	if lastnum > 0 {
		sfx = strings.ToLower(str[lastnum+1:])
		str = str[:lastnum+1]
	}
	if len(sfx) > 0 {
		imul := 0
		if (len(sfx) == 3 && sfx[1:] == "ib") || len(sfx) == 1 {
			imul = 1000
		} else if len(sfx) == 2 && sfx[1] == 'b' {
			imul = 1024
		}
		switch sfx[0] {
		case 'p':
			imul *= imul
			fallthrough
		case 't':
			imul *= imul
			fallthrough
		case 'g':
			imul *= imul
			fallthrough
		case 'm':
			imul *= imul
			fallthrough
		case 'k':
		default:
			imul = 0
		}
		if imul == 0 {
			err = fmt.Errorf("'%v' is not a valid suffix (/[ptgmk](b|ib|)/i)", sfx)
		} else {
			mul = imul
		}
	}
	return
}
func pint(val reflect.Value, src string, bit int) error {
	src, mul, err := reint(descore.Replace(src))
	if err != nil {
		return err
	}
	i, err := strconv.ParseInt(src, 0, bit)
	if err != nil {
		return err
	}
	val.SetInt(i * int64(mul))
	return nil
}
func puint(val reflect.Value, src string, bit int) error {
	src, mul, err := reint(descore.Replace(src))
	if err != nil {
		return err
	}
	i, err := strconv.ParseUint(src, 0, bit)
	if err != nil {
		return err
	}
	val.SetUint(i * uint64(mul))
	return nil
}
func pfloat(val reflect.Value, src string, bit int) error {
	src, mul, err := reint(descore.Replace(src))
	if err != nil {
		return err
	}
	i, err := strconv.ParseFloat(src, bit)
	if err != nil {
		return err
	}
	val.SetFloat(i * float64(mul))
	return nil
}

func setSend(out reflect.Value, src string) error {
	outType, err := unwrap(out)
	if err != nil {
		return err
	}
	new := reflect.New(outType).Elem()
	var oerr error
	if src == `""` {
		src = ``
	}
	switch outType.Kind() {
	case reflect.Bool:
		if src == "true" {
			new.SetBool(true)
		} else if src == "false" {
			new.SetBool(false)
		} else {
			oerr = fmt.Errorf("'%v' is not 'true' or 'false'", src)
		}
	case reflect.Int:
		oerr = pint(new, src, 0)
	case reflect.Int8:
		oerr = pint(new, src, 8)
	case reflect.Int16:
		oerr = pint(new, src, 16)
	case reflect.Int32:
		oerr = pint(new, src, 32)
	case reflect.Int64:
		oerr = pint(new, src, 64)
	case reflect.Uint:
		oerr = puint(new, src, 0)
	case reflect.Uint8:
		oerr = puint(new, src, 8)
	case reflect.Uint16:
		oerr = puint(new, src, 16)
	case reflect.Uint32:
		oerr = puint(new, src, 32)
	case reflect.Uint64:
		oerr = puint(new, src, 64)
	case reflect.Float32:
		oerr = pfloat(new, src, 32)
	case reflect.Float64:
		oerr = pfloat(new, src, 64)
	case reflect.String:
		new.SetString(src)
	default:
		oerr = fmt.Errorf("%v is a unsupported type", outType)
	}
	return write(out, new, oerr)
}

//Handles updating out when name is changed
// - struct{}
// - *os.File
// - string
// - []byte
// - interface{} which will be JSON decoded
func fileUpdate(name string, out reflect.Value) error {
	log.Infof("Reloading %v", name)
	outType, err := unwrap(out)
	switch outType.Kind() {
	case reflect.Struct:
		if outType.NumField() == 0 {
			return write(out, reflect.ValueOf(struct{}{}), nil)
		}
		goto json
	case reflect.Ptr:
		if outType.AssignableTo(reflect.TypeOf(&os.File{})) {
			fi, err := os.Open(name)
			return write(out, reflect.ValueOf(fi), err)
		}
		goto json
	case reflect.String:
		b, err := ioutil.ReadFile(name)
		return write(out, reflect.ValueOf(string(b)), err)
	case reflect.Slice:
		if outType.Elem().Kind() != reflect.Uint8 {
			goto json
		}
		b, err := ioutil.ReadFile(name)
		return write(out, reflect.ValueOf(b), err)
	case reflect.Interface:
		goto json
	default:
		return fmt.Errorf("unsupported type: %v", out.Type())
	}
json:
	ov := reflect.New(outType)
	fi, err := os.Open(name)
	if err != nil {
		write(out, ov.Elem(), err)
	}
	defer fi.Close()
	err = json.NewDecoder(fi).Decode(ov.Interface())
	return write(out, ov.Elem(), err)
}
